package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class ProfileResponse(

    @SerializedName("answer")
    var answer: Answer? = Answer(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: String? = ""
) {
    data class Answer(
        @SerializedName("email")
        var email: String? = "",
        @SerializedName("firstname")
        var firstname: String? = "",
        @SerializedName("lastname")
        var lastname: String? = "",
        @SerializedName("status")
        var status: String? = "",
        @SerializedName("telephone")
        var telephone: String? = "",


        @SerializedName("bonus")
        var bonus: String? = "",

        @SerializedName("vogak")
        var vogak: String? = "",
        @SerializedName("vogak_id")
        var vogakId: String? = ""
    )
}
