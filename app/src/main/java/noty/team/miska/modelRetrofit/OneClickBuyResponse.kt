package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class OneClickBuyResponse(
    @SerializedName("answer")
    var answer: Answer? = Answer(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: String? = ""
) {
    data class Answer(
        @SerializedName("data")
        var `data`: Any? = Any(),
        @SerializedName("status")
        var status: Boolean? = false
    )
}