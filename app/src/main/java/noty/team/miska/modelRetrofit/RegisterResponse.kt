package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class RegisterResponse(
    @SerializedName("answer")
    var answer: Answer? = Answer(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: Any? = Any()
) {

    data class Answer(
        @SerializedName("token")
        var token: String? = "",
        @SerializedName("loc")
        var loc: Int? = 0
    )
}