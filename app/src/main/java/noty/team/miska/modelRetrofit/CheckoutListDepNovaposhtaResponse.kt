package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class CheckoutListDepNovaposhtaResponse(
    @SerializedName("answer")
    var answer: ArrayList<AnswerDep>? = arrayListOf(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: Any? = Any()
)

data class AnswerDep(
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("Ref")
    var ref: String? = ""

) {
    override fun toString(): String {
        return name.toString()
    }
}
