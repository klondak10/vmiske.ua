package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class CheckoutOrderInfoResponse(


    @SerializedName("answer")
    var answer: List<Answer?>? = listOf(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: Any? = Any()
) {
    data class Answer(
        @SerializedName("address")
        var address: String? = "",
        @SerializedName("ambar")
        var ambar: Int? = 0,
        @SerializedName("ambars")
        var ambars: ArrayList<Ambar>? = arrayListOf(),
        @SerializedName("subinfo")
        var subinfo: ArrayList<Subinfo>? = arrayListOf(),
        @SerializedName("date")
        var date: String? = "",
        @SerializedName("delivery")
        var delivery: String? = "",
        @SerializedName("ordId")
        var ordId: Int? = 0,
        @SerializedName("paytype")
        var paytype: String? = "",
        @SerializedName("products")
        var products: ArrayList<ProductOrderInfo>? = arrayListOf(),
        @SerializedName("status")
        var status: String? = "",
        @SerializedName("total")
        var total: String? = ""
    )

    override fun toString(): String {
        return "CheckoutOrderInfoResponse(answer=$answer, error=$error, message=$message)"
    }

}

data class Subinfo(
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("value")
    var value: Int? = 0
)
data class Ambar(
    @SerializedName("info")
    var info: InfoAmbar? = InfoAmbar(),
    @SerializedName("products")
    var products: ArrayList<ProductAmbar>? = arrayListOf()
)

data class InfoAmbar(
    @SerializedName("ambar_id")
    var ambarId: Int? = 0,
    @SerializedName("date")
    var date: String? = "",
    var dateLong: Long = 0,
    @SerializedName("error_sent")
    var errorSent: Int? = 0,
    @SerializedName("order_id")
    var orderId: Int? = 0,
    @SerializedName("sent")
    var sent: Int? = 0,
    @SerializedName("status")
    var status: String? = "",
    @SerializedName("txt_for_admin")
    var txtForAdmin: Any? = Any(),
    @SerializedName("txt_for_customer")
    var txtForCustomer: String? = ""
)

data class ProductAmbar(
    var selected: Boolean = false,
    var enabled: Boolean = false,
    @SerializedName("image")
    var image: String? = "",
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("price")
    var price: Int? = 0,
    @SerializedName("product_id")
    var productId: String? = "",
    @SerializedName("quantity")
    var quantity: Int = 0,


    //order_product_id
    @SerializedName("order_product_id")
    var orderProductId: String = "",
    var inAmbar: Int? = 0,
    var inOrder: Int? = 0
)

data class ProductOrderInfo(
    var selected: Boolean = false,
    @SerializedName("image")
    var image: String? = "",
    @SerializedName("inAmbar")
    var inAmbar: Int? = 0,
    @SerializedName("model")
    var model: String? = "",
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("order_id")
    var orderId: Int? = 0,
    @SerializedName("order_product_id")
    var orderProductId: Int? = 0,
    @SerializedName("price")
    var price: String? = "",
    @SerializedName("product_id")
    var productId: Int? = 0,
    @SerializedName("quantity")
    var quantity: Int = 0,
    @SerializedName("reward")
    var reward: Int? = 0,
    @SerializedName("tax")
    var tax: String? = "",
    @SerializedName("total")
    var total: String? = ""
)