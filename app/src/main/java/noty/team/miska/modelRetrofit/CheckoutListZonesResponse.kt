package noty.team.miska.modelRetrofit
import com.google.gson.annotations.SerializedName


data class CheckoutListZonesResponse(
    @SerializedName("answer")
    var answer: ArrayList<AnswerZone>? = arrayListOf<AnswerZone>(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: Any? = Any()
)

     data class AnswerZone(
        @SerializedName("area")
        var area: String? = "",
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("zone_id")
        var zoneId: String? = ""

    ) {
        override fun toString(): String {
            return name.toString()
        }
    }
