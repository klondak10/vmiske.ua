package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName

data class BasketDeleteResponse (
    @SerializedName("answer")
    var answer: Boolean?,
    @SerializedName("error")
    var error: Boolean?,
    @SerializedName("message")
    var message: Any?
)