package noty.team.miska.modelRetrofit
import com.google.gson.annotations.SerializedName


data class RestorePasswordResponse(
    @SerializedName("success")
    var success: Success? = Success(),
    @SerializedName("error")
    var error: Error? = Error()

) {
    data class Success(
        @SerializedName("title")
        var title: String? = ""
    )

    data class Error(
        @SerializedName("email")
        var email: String? = ""
    )
}
