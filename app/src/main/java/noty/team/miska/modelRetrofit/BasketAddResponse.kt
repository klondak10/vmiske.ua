package noty.team.miska.modelRetrofit
import com.google.gson.annotations.SerializedName


data class BasketAddResponse(
    @SerializedName("answer")
    var answer: Boolean? = false,
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: String? = ""
)