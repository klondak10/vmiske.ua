package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class CheckoutListPickupResponse(
    @SerializedName("answer")
    var answer: AnswerPickup? = AnswerPickup(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: String? = ""
)

data class AnswerPickup(
    @SerializedName("address")
    var address: String? = ""
) {
    override fun toString(): String {
        return address.toString()
    }
}
