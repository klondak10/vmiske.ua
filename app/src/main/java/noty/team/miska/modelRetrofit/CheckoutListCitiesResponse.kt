package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class CheckoutListCitiesResponse(
    @SerializedName("answer")
    var answer: ArrayList<AnswerCity>? = arrayListOf(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: Any? = Any()
)

data class AnswerCity(
    @SerializedName("city_id")
    var cityId: String? = "",
    @SerializedName("Ref")
    var Ref: String? = "",
    @SerializedName("name")
    var name: String? = ""
) {
    override fun toString(): String {
        return name.toString()
    }
}
