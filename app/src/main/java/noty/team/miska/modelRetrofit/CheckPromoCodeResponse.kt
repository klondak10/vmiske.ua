package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class CheckPromoCodeResponse(
    @SerializedName("answer")
    var answer: AnswerPromo? = AnswerPromo(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: String? = ""
)

data class AnswerPromo(
    @SerializedName("minsumm")
    var minsumm: Int? = 0,
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("summ")
    var summ: Int? = 0,
    @SerializedName("type")
    var type: String? = ""
)
