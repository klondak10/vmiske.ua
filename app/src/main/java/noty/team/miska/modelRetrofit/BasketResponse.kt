package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class BasketResponse(
    @SerializedName("answer")
    var answer: List<Answer?>? = listOf(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: String? = ""
) {
    data class Answer(
        @SerializedName("artikul")
        var artikul: String? = "",
        @SerializedName("barcode")
        var barcode: String? = "",
        @SerializedName("baskId")
        var baskId: Int? = 0,
        @SerializedName("count")
        var count: Int? = 0,
        @SerializedName("discount")
        var discount: Discount? = Discount(),
        @SerializedName("image")
        var image: String? = "",
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("opt")
        var opt: Opt?= Opt(),
        @SerializedName("price")
        var price: Int? = 0,
        @SerializedName("product_id")
        var productId: Int? = 0,
        @SerializedName("quantity")
        var quantity: Int? = 0,
        @SerializedName("status")
        var status: Any? = Any(),
        @SerializedName("stock_status_id")
        var stockStatusId: Int? = 0
    )
    data class Opt(
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("price")
        var price: Int? = 0,
        @SerializedName("value")
        var value: String? = ""
    )
}

data class Discount(
    @SerializedName("price")
    var price: String? = "",
    @SerializedName("date_end")
    var dateEnd: String? = "",
    @SerializedName("date_start")
    var dateStart: String? = ""
)
