package noty.team.miska.modelRetrofit

import android.graphics.Bitmap
import com.google.gson.annotations.SerializedName

data class ProductInfoResponse(
    @SerializedName("answer")
    var answer: Answer? = Answer(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: Any? = Any()
) {

    data class Answer(
        @SerializedName("info")
        var info: Info? = Info(),
        @SerializedName("reviews")
        var reviews: List<Review?>? = listOf(),
        @SerializedName("spec")
        var spec: List<Spec?>? = listOf(),
        @SerializedName("link")
        var link: String? = ""
    )

    data class Review(
        @SerializedName("rating")
        var rating: String? = "",
        @SerializedName("author")
        var author: String? = "",
        @SerializedName("answer")
        var answer: AnswerX? = AnswerX(),
        @SerializedName("text")
        var text: String? = "",
        @SerializedName("email")
        var email: String? = "",
        @SerializedName("date")
        var date: String? = ""
    )

    class AnswerX(
        @SerializedName("rating")
        var rating: String? = "",
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("text")
        var text: String? = "",
        @SerializedName("email")
        var email: String? = "",
        @SerializedName("date")
        var date: String? = ""


    )

    data class Spec(
        @SerializedName("text")
        var text: String? = "",
        @SerializedName("name")
        var name: String? = ""
    )

    data class Info(
        @SerializedName("product_id")
        var productId: Int? = 0,
        @SerializedName("price")
        var price: Int? = 0,
        @SerializedName("image")
        var image: ArrayList<String?>? = arrayListOf(),
        var imageBitmap: ArrayList<Bitmap> = arrayListOf(),
        @SerializedName("viewed")
        var viewed: Int? = 0,
        @SerializedName("status")
        var status: String? = "",
        @SerializedName("stock_status_id")
        var stockStatusId: Int? = 0,
        @SerializedName("quantity")
        var quantity: Int? = 0,

        @SerializedName("barcode")
        var barcode: String? = "",
        @SerializedName("artikul")
        var artikul: String? = "",

        @SerializedName("name")
        var name: String? = "",
        @SerializedName("description")
        var description: String? = "",
        @SerializedName("discount")
        var discount: Discount? = Discount(),
        @SerializedName("subprice")
        var subprice: ArrayList<Subprice?>? = arrayListOf(),
        @SerializedName("rew")
        var rew: Rew? = Rew()
    )

    data class Discount(
        @SerializedName("price")
        var price: Int? = 0,
        @SerializedName("date_end")
        var dateEnd: String? = "",
        @SerializedName("date_start")
        var dateStart: String? = ""
    )

    data class Rew(
        @SerializedName("rating")
        var rating: Float? = 0.0f,
        @SerializedName("count")
        var count: Int? = 0
    )

    data class Subprice(
        @SerializedName("price")
        var price: Int? = 0,
        @SerializedName("quantity")
        var quantity: Int? = 0,
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("ovId")
        var ovId: Int? = 0,
        @SerializedName("povId")
        var povId: Int? = 0,
        @SerializedName("poId")
        var poId: Int? = 0,
        @SerializedName("value")
        var value: String = "",


        @SerializedName("artikul")
        var artikul: String? = "",
        @SerializedName("barcode")
        var barcode: String? = ""
    )
}
