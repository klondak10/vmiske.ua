package noty.team.miska.modelRetrofit
import com.google.gson.annotations.SerializedName
data class OrderMakeResponse(
    @SerializedName("answer")
    var answer: Answer? = Answer(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: Any? = Any()
) {
    data class Answer(
        @SerializedName("data")
        var data: Data? = Data(),
        @SerializedName("status")
        var status: Boolean? = false
    ) {
        data class Data(
            @SerializedName("amount")
            var amount: Int? = 0,
            @SerializedName("currency")
            var currency: String? = "",
            @SerializedName("default_phone")
            var defaultPhone: String? = "",
            @SerializedName("description")
            var description: String? = "",
            @SerializedName("order_id")
            var orderId: Int? = 0,
            @SerializedName("pay_way")
            var payWay: String? = ""
        )
    }
}