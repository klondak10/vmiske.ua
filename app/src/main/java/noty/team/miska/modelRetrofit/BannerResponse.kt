package noty.team.miska.modelRetrofit

import android.graphics.Bitmap
import com.google.gson.annotations.SerializedName

data class BannerResponse(
    @SerializedName("answer")
    var answer: List<BannerAnswer?>? = listOf(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: Any? = Any(),
    var serverError: String? = null
)

data class BannerAnswer(
    @SerializedName("title")
    var title: String? = "",
    @SerializedName("image")
    var image: String? = "",
    @SerializedName("description")
    var description: String? = "",
    @SerializedName("link")
    var link: String? = "",
    var bitmap: Bitmap? = null
)