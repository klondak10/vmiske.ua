package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class NewReviewResponse(
    @SerializedName("answer")
    var answer: List<Answer?>? = listOf(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: String? = ""
) {

    data class Answer(
        @SerializedName("rating")
        var rating: String? = "",
        @SerializedName("name")
        var author: String? = "",
        @SerializedName("answer")
        var answer: AnswerX? = AnswerX(),
        @SerializedName("text")
        var text: String? = "",
        @SerializedName("email")
        var email: String? = "",
        @SerializedName("date")
        var date: String? = ""
    )

    class AnswerX(
        @SerializedName("rating")
        var rating: String? = "",
        @SerializedName("name")
        var author: String? = "",
        @SerializedName("text")
        var text: String? = "",
        @SerializedName("email")
        var email: String? = "",
        @SerializedName("date")
        var date: String? = ""

    )
}