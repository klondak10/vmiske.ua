package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName

data class CatalogResponse(
    @SerializedName("answer")
    var answer: List<Answer?>? = listOf(),
    @SerializedName("error")
    var error: Boolean = false,
    @SerializedName("message")
    var message: String? = "",
    var serverError: String? = null
)

data class Answer(
    @SerializedName("cId")
    var cId: Int? = 0,
    @SerializedName("img")
    var img: String? = "",
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("pcount")
    var pcount: Int? = 0,
    @SerializedName("prodcount")
    var prodcount: Int? = 0
)