package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class LoginGoogleResponse(
    @SerializedName("answer")
    var answer: Answer?,
    @SerializedName("error")
    var error: Boolean?,
    @SerializedName("message")
    var message: String?
) {
    data class Answer(
        @SerializedName("token")
        var token: String?
    )
}