package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName

data class ListProductResponse(
    @SerializedName("answer")
    var answer: Answer? = Answer(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: Any? = Any()
) {

    data class Answer(
        @SerializedName("products")
        var products: ArrayList<Product?>? = arrayListOf(),
        @SerializedName("filters")
        var filters: ArrayList<Filter?>? = arrayListOf(),
        @SerializedName("page")
        var page: Page? = Page()
    )

    data class Product(
        @SerializedName("product_id")
        var productId: Int? = 0,
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("image")
        var image: String? = "",
        @SerializedName("price")
        var price: Int? = 0,
        @SerializedName("viewed")
        var viewed: Int? = 0,
        @SerializedName("status")
        var status: String? = "",
        @SerializedName("stock_status_id")
        var stockStatusId: Int? = 0,
        @SerializedName("quantity")
        var quantity: Int? = 0,
        @SerializedName("discount")
        var discount: Discount? = Discount(),
        @SerializedName("subprice")
        var subprice: ArrayList<Subprice>? = arrayListOf(),
        @SerializedName("rew")
        var rew: Float? = 0.0f
    )

    data class Discount(
        @SerializedName("price")
        var price: String? = "",
        @SerializedName("date_end")
        var dateEnd: String? = "",
        @SerializedName("date_start")
        var dateStart: String? = ""
    )

    data class Filter(
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("value")
        var value: ArrayList<Value?>? = arrayListOf()
    )

    data class Value(
        @SerializedName("id")
        var id: Int? = 0,
        @SerializedName("subname")
        var subname: String? = ""
    )


    data class Page(
        @SerializedName("count")
        var count: Int? = 0,
        @SerializedName("limit")
        var limit: Int? = 0,
        @SerializedName("offset")
        var offset: Int? = 0,
        @SerializedName("sort")
        var sort: String? = ""
    )
}

data class Subprice(
    @SerializedName("price")
    var price: Int? = 0,
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("ovId")
    var ovId: Int? = 0,
    @SerializedName("quantity")
    var quantity: Int? = 0
)

