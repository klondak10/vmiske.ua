package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class CheckoutOrdersResponse(
    @SerializedName("answer")
    var answer: ArrayList<AnswerOrders>? = arrayListOf(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: String? = ""
)
    data class AnswerOrders(
        @SerializedName("ambar")
        var ambar: Int? = 0,
        @SerializedName("date")
        var date: String? = "",
        @SerializedName("ordId")
        var ordId: Int? = 0,
        @SerializedName("status")
        var status: String? = "",
        @SerializedName("total")
        var total: String? = "",

        @SerializedName("order_status")
        var orderStatus: Int? = 0,
        @SerializedName("payment_code")
        var payment_code: String? = ""



    )
