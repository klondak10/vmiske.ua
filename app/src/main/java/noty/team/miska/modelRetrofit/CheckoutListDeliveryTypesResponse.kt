package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class CheckoutListDeliveryTypesResponse(
    @SerializedName("answer")
    var answer: ArrayList<AnswerDeliveryType>? = arrayListOf(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: String? = ""
)

    data class AnswerDeliveryType(
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("type")
        var type: String? = ""
    )
