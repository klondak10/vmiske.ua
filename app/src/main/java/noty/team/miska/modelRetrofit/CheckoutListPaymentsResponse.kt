package noty.team.miska.modelRetrofit

import com.google.gson.annotations.SerializedName


data class CheckoutListPaymentsResponse(
    @SerializedName("answer")
    var answer: ArrayList<AnswerPayment>? = arrayListOf(),
    @SerializedName("error")
    var error: Boolean? = false,
    @SerializedName("message")
    var message: String? = ""
)

data class AnswerPayment(
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("type")
    var type: String? = ""
)
