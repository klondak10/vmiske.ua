package noty.team.miska.product

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_product_property_item.view.*
import noty.team.miska.R
import noty.team.miska.model.ItemProductProperty


class ProductPropertyAdapter(
    private val mValues: List<ItemProductProperty>

) : RecyclerView.Adapter<ProductPropertyAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener


    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ItemProductProperty
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view: View? = null
        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_product_property_item, parent, false)

        return ViewHolder(view!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        holder.mName.text = item.name + ": "
        holder.mText.text = item.text

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {

        val mName: TextView = mView.product_property_name
        val mText: TextView = mView.product_property_data

    }
}
