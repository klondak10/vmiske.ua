package noty.team.miska.product


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_buy_now.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.checkout.CheckoutErrorFragment
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.model.ItemProductCatalog
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel


const val BUY_NOW_SEND_FRAGMENT = "BUY_NOW_SEND_FRAGMENT"

class BuyNowFragment(var product: ItemProductCatalog) : Fragment() {
    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_buy_now, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buy_now_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        buy_now_button.setOnClickListener {
            if (buy_now_phone_number.text.toString().trim().isEmpty()) {
                context?.toast(getString(R.string.input_phone_number))
                return@setOnClickListener
            }



            if (buy_now_phone_number.text.toString().length < 10 &&
                buy_now_phone_number.text.toString().length > 0
            ) {
                context?.toast(getString(R.string.message_input_10_numbers))
                return@setOnClickListener
            }

            var number = buy_now_phone_number.text.toString()

            product.oneClickPhone = "+38(" + number.substring(0, 3) + ")" + number.substring(
                3,
                6
            ) + "-" + number.substring(6, 8) + "-" + number.substring(8, 10)

            var token = Paper.book().read(PAPER_TOKEN, "")

            if (product.basketItemInfo == null)
                viewModel.apiOneClickBuyProductId(
                    token, product.oneClickPhone, (product.quantity).toString(),
                    product.productId.toString()
                ).observe(this, Observer { response ->
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data?.error == false) {
                            if (data.answer?.status == true) {
                                context?.toast(getString(R.string.purchase_done))
                                showSuccessInfo()
                            }
                        } else
                            context?.toast(data?.message.toString())
                    } else
                        context?.toast(response?.message().toString())
                })
            else {

                viewModel.apiOneClickBuy(
                    token,
                    product.oneClickPhone,
                    (product.quantity).toString(),
                    product.productId.toString(),
                    product.basketItemInfo?.optionValue,
                    product.basketItemInfo?.optionDescription,
                    product.basketItemInfo?.optionPrice.toString(),
                    product.basketItemInfo?.povId.toString(),
                    product.basketItemInfo?.poId.toString()
                ).observe(this, Observer { response ->
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data?.error == false) {
                            if (data.answer?.status == true) {
                                context?.toast(getString(R.string.purchase_done))
                                showSuccessInfo()
                            }
                        } else
                            context?.toast(data?.message.toString())
                    } else
                        context?.toast(response?.message().toString())
                })

            }

        }
    }

    fun showSuccessInfo() {
        (activity as MainActivity).showCustomAppBar("", true, false)
        (activity as MainActivity).replaceFragment(
            BuyNowSendFragment(product),
            BUY_NOW_SEND_FRAGMENT,
            false,
            ""
        )
    }

    fun showFailInfo() {
        (activity as MainActivity).showCustomAppBar("", true, false)
        (activity as MainActivity).replaceFragment(
            CheckoutErrorFragment(),
            BUY_NOW_SEND_FRAGMENT,
            false,
            ""
        )
    }

}
