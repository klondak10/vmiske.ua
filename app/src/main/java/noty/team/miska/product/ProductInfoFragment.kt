package noty.team.miska.product

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.asksira.loopingviewpager.LoopingViewPager
import com.google.android.material.chip.Chip
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_product_info.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.catalog.CURRENCY
import noty.team.miska.catalog.ProductImageInfiniteAdapter
import noty.team.miska.catalog.formatNumberWithSpaces
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.model.BasketItemInfo
import noty.team.miska.model.ItemProductCatalog
import noty.team.miska.modelRetrofit.ProductInfoResponse
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


const val BUY_NOW_FRAGMENT = "BUY_NOW_FRAGMENT"

class ProductInfoFragment(private val itemInfo: ProductInfoResponse, val paperLang: String) : Fragment() {

    var bitmapList: ArrayList<Bitmap> = arrayListOf()
    var adapter: ProductImageInfiniteAdapter? = null
    var progressBar: ProgressBar? = null

    //10 kg
    var optionValue = ""
    //Weight
    var optionDescription = ""
    //1250
    var mainPrice = 0
    var optionPrice = 0

    var basketItemInfo: BasketItemInfo? = null

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_product_info, container, false)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser)
            activity?.let {
                (it as MainActivity).fabShow()
            }
    }

    fun setResourcesByLocale() {
        val configuration = Configuration(resources.configuration)
        configuration.setLocale(Locale("ru"))
        resources.updateConfiguration(configuration, resources.displayMetrics)
    }


    override fun onResume() {
        super.onResume()
        progressBar?.visibility = View.GONE
        if (pauseFlag) {
            for (index in 0 until chipsArray.size)
                if (selectedChip == index) {

                    chipsArray[index].performClick()
                    chipsArray[index].isChecked = true
                }
            pauseFlag = false
        }
    }


    var pauseFlag = false
    override fun onPause() {
        super.onPause()
        pauseFlag = true
    }

    var chipsArray: ArrayList<Chip> = arrayListOf()
    var selectedChip = 0

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // setResourcesByLocale()

        itemInfo.answer?.info?.let { info ->

            var newName = info.name?.replace("&quot;", "")
            product_info_text.text = newName?.replace("amp;", "")

            val newInfo = info.description?.replace("&quot;", "")
            val newInfo2 = newInfo?.replace("&nbsp;", "")
            product_info_description.text = newInfo2?.replace("amp;", "")

            val countStr = info.rew?.count ?: "".toInt()


            var countPlural: String? = ""
            if (countStr == 0) {
                countPlural = getString(R.string.zero_review)
                product_info_review_count.text = "0 $countPlural"
            } else {
                countPlural = context?.resources?.getQuantityString(
                    R.plurals.plurals_review,
                    countStr, countStr
                )
                product_info_review_count.text = countPlural.toString()
            }


            product_info_rating_bar.rating = (info.rew?.rating ?: 0.0f)

            var messageCode = getString(R.string.code_stuff)
            product_info_code.text = messageCode + info.artikul.toString()

            var newPrice = formatNumberWithSpaces(info.price ?: 0) + CURRENCY
            product_info_price.text = newPrice
            mainPrice = info.price ?: 0

            product_info_no_count.visibility = View.GONE

            if (info.discount!=null ){
                product_info_price_discount.visibility = View.VISIBLE
                product_info_price.text = formatNumberWithSpaces(info.discount!!.price!!) + CURRENCY
                product_info_price_discount.text = formatNumberWithSpaces(info.price ?: 0) + CURRENCY
                product_info_price_discount.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                if (convertDateToLong(info.discount!!.dateEnd!!)- System.currentTimeMillis()<0){
                    promo_chip.visibility = View.GONE
                }else {
                    promo_chip.visibility = View.VISIBLE
                    promo_chip.text =
                        activity!!.resources.getString(R.string.label_discount) + convertSecondsToHMmSs(
                            convertDateToLong(info.discount!!.dateEnd!!) - System.currentTimeMillis()
                        )
                }

                product_info_price.text = newPrice
            }
            if (info.status!!.contains("2-3")){
                sales_chip_label.visibility = View.VISIBLE
                sales_chip_label.text = getString(R.string.label)
            }
            else{

                product_info_price_discount.visibility = View.GONE
                var newPrice = formatNumberWithSpaces(info.price ?: 0) + CURRENCY
            }

            val sizeSubprice = info.subprice?.size ?: 0

            if (sizeSubprice > 0) {

                info.subprice?.let { priceList ->

                    chipsArray.clear()
                    // add chips dynamic
                    for (item in 0 until priceList.size) {
                        var view: View? = null
                        if (priceList[item]?.quantity == 0) {
                            view = LayoutInflater.from(context)
                                .inflate(
                                    R.layout.item_select_chip_black,
                                    product_info_chips_group_type,
                                    false
                                )
                        } else {
                            view = LayoutInflater.from(context)
                                .inflate(
                                    R.layout.item_select_chip_black,
                                    product_info_chips_group_type,
                                    false
                                )
                        }
                        val chip =
                            view.findViewById<com.google.android.material.chip.Chip>(R.id.chips_item)
                        chipsArray.add(chip)
                        chip.text = priceList[item]?.name ?: ""
                        chip.id = item
                        chip.setOnClickListener {
                            selectedChip = item
                            var priceCalc = info.price ?: 0
                            priceCalc += priceList[item]?.price ?: 0

                            basketItemInfo = BasketItemInfo()
                            basketItemInfo?.povId = priceList[item]?.povId ?: 0
                            basketItemInfo?.poId = priceList[item]?.poId ?: 0
                            basketItemInfo?.optionValue = priceList[item]?.name ?: ""
                            basketItemInfo?.optionDescription = priceList[item]?.value ?: ""
                            basketItemInfo?.optionPrice = priceList[item]?.price ?: 0
//                            //price
                            //optionPrice = priceCalc
                            optionPrice = priceList[item]?.price ?: 0

                            var newPrice = formatNumberWithSpaces(
                                priceCalc
                            ) + CURRENCY
                            product_info_price.text = newPrice


                            product_info_code.text =
                                messageCode + priceList.get(item)?.artikul
                            if (priceList[item]?.quantity == 0) {
                            } else {
                                product_info_buy_oneclick_button.isEnabled = true
                                product_info_basket_button.isEnabled = true
                                product_info_basket_button.setColorFilter(
                                    Color.argb(
                                        255,
                                        255,
                                        0,
                                        0
                                    )
                                )
                                product_info_no_count.visibility = View.GONE
                                product_info_price.setTextColor(
                                    ContextCompat.getColor(
                                        context!!,
                                        R.color.colorMainRed
                                    )
                                )
                            }
                        }
                        product_info_chips_group_type.addView(chip as View)
                    }

                    product_info_chips_group_type.invalidate()
                    //find price with quantity
                    var flagEmpty: Boolean = false
                    if (!flagEmpty && !pauseFlag) {
                        product_info_chips_group_type.getChildAt(0).performClick()
                        product_info_chips_group_type.check(
                            product_info_chips_group_type.getChildAt(
                                0
                            ).id
                        )
                    }
                }
            }

            product_info_infinite_viewpager?.setIndicatorSmart(true)

            itemInfo.answer?.info?.let { info ->
                itemInfo.answer?.info?.imageBitmap?.let {
                    bitmapList.clear()
                    bitmapList.addAll(it)
                }
                if (bitmapList.size == 0) {
                    var bitmap = BitmapFactory.decodeResource(
                        context?.resources,
                        R.drawable.placeholder_big
                    )
                    bitmapList.add(bitmap)
                }
                adapter = ProductImageInfiniteAdapter(context!!, bitmapList, true, null)
                product_info_infinite_viewpager?.adapter = adapter
                adapter?.setItemList(bitmapList)
                product_info_infinite_page_indicator?.count = product_info_infinite_viewpager.indicatorCount
                product_info_infinite_page_indicator?.selection = 0
                product_info_infinite_viewpager?.setIndicatorPageChangeListener(object :
                    LoopingViewPager.IndicatorPageChangeListener {
                    override fun onIndicatorProgress(
                        selectingPosition: Int,
                        progress: Float
                    ) {
                        product_info_infinite_page_indicator?.selection = selectingPosition
                    }

                    override fun onIndicatorPageChange(newIndicatorPosition: Int) {
                        product_info_infinite_page_indicator?.selection = newIndicatorPosition
                    }
                })
            }
            product_info_buy_oneclick_button.setOnClickListener {

                var token = Paper.book().read(PAPER_TOKEN, "")
                itemInfo.answer?.info?.let { info ->

                    var imageUrl = ""
                    if (info.image != null) {
                        imageUrl = info.image?.get(0) ?: ""
                    }
                    var item = ItemProductCatalog(info.name ?: "", 0)
                    item.productId = info.productId ?: 0
                    item.imageUrl = imageUrl
                    item.price = mainPrice.toString()
                    item.priceCurrency = CURRENCY
                    item.quantity = 1
                    item.basketItemInfo = basketItemInfo


                    (activity as MainActivity).showCustomAppBar("", true, true)
                    (activity as MainActivity).replaceFragment(
                        BuyNowFragment(item),
                        BUY_NOW_FRAGMENT,
                        false,
                        ""
                    )
                }
            }
        }
        progressBar?.visibility = View.GONE

        itemInfo.answer?.info?.let { info ->
            product_info_basket_button.setOnClickListener {
                var imageUrl = ""
                if (info.image != null) {
                    imageUrl = info.image?.get(0) ?: ""
                }
                item = ItemProductCatalog(info.name ?: "", 0)
                item.productId = info.productId ?: 0
                item.imageUrl = imageUrl
                item.price = mainPrice.toString()
                item.optionValueWeight = optionPrice.toString()

                item.priceCurrency = CURRENCY
                item.quantity = 1
                item.basketItemInfo = basketItemInfo

                var token = Paper.book().read(PAPER_TOKEN, "")
                if (token != "") {
                    token = TOKEN_PREFIX + token
                    //if exist in basket - renew count
                    var existInBasketFlag = false
                    for (itemBasket in viewModel.basketList) {
                        if (item.productId == itemBasket.productId) {
                            var checkSubpriceFlag = false
                            if (item.basketItemInfo != null)
                                if (item.price == itemBasket.basketItemInfo?.optionPrice?.toString()) {
                                    checkSubpriceFlag = true
                                }
                            if ((item.basketItemInfo == null) || (item.basketItemInfo != null && checkSubpriceFlag)) {
                                existInBasketFlag = true
                                viewModel.apiUpdateBasket(
                                    token, itemBasket.basketId ?: 0,
                                    itemBasket.quantity + 1
                                ).observe(this, Observer { response ->
                                    if (response.isSuccessful) {
                                        val data = response.body()
                                        if (data?.error == false) {

                                            context?.toast(getString(R.string.products_count_changed))
                                            (activity as MainActivity).getBasketFromServer(
                                                false,
                                                ({})
                                            )

                                        } else
                                            context?.toast(data?.message.toString())
                                    } else
                                        context?.toast(response?.message().toString())
                                })
                            }

                        }

                    }
                    if (!existInBasketFlag)
                        if (basketItemInfo == null) {
                            viewModel.apiAddBasket(
                                token, "1", (info.productId ?: 0).toString(),
                                null, null, null, null, null
                            ).observe(this, Observer { response ->
                                if (response.isSuccessful) {
                                    val data = response.body()
                                    if (data?.error == false) {
                                        if (data.answer == true) {
                                            context?.toast(getString(R.string.product_added_basket))
                                            (activity as MainActivity).getBasketFromServer(
                                                false,
                                                ({})
                                            )
                                        } else {
                                            context?.toast(getString(R.string.product_not_add_bsaket))
                                        }
                                    } else
                                        context?.toast(data?.message.toString())
                                } else
                                    context?.toast(response?.message().toString())


                            })
                        } else {
                            viewModel.apiAddBasket(
                                token, "1", (info.productId ?: 0).toString(),
                                basketItemInfo?.povId.toString(), basketItemInfo?.poId.toString(),
                                basketItemInfo?.optionValue, basketItemInfo?.optionDescription,
                                basketItemInfo?.optionPrice?.toString()
                            ).observe(this, Observer { response ->
                                if (response.isSuccessful) {
                                    val data = response.body()
                                    if (data?.error == false) {
                                        if (data.answer == true) {
                                            context?.toast(getString(R.string.product_added_basket))
                                            (activity as MainActivity).getBasketFromServer(
                                                false,
                                                ({})
                                            )
                                        } else {
                                            context?.toast(getString(R.string.product_not_add_bsaket))
                                        }
                                    } else
                                        context?.toast(data?.message.toString())
                                } else
                                    context?.toast(response?.message().toString())

                            })
                        }
                } else {
                    //if no user logged in
                    //if exist in basket - renew count


                    var basketList: MutableList<ItemProductCatalog> = mutableListOf()
                    basketList.addAll(viewModel.basketList)
                    if (basketList.size > 0) {
                        var index = 0
                        var foundFlag = false
                        for (itemBasket in basketList) {
                            if (item.productId == itemBasket.productId) {
                                if (item.basketItemInfo != null) {
                                    if (item.basketItemInfo?.optionValue == itemBasket.basketItemInfo?.optionValue) {
                                        itemBasket.quantity += 1
                                        context?.toast(getString(R.string.products_count_changed))
                                        foundFlag = true
                                        break
                                    }
                                } else {
                                    itemBasket.quantity += 1
                                    context?.toast(getString(R.string.products_count_changed))
                                    foundFlag = true
                                }
                            }
                            index++
                        }
                        // if not found  -add to basket
                        if (!foundFlag) {
                            viewModel.basketItemLD.value = item
                            context?.toast(getString(R.string.product_added_basket))
                        }
                    } else {
                        //else if not in basket, add to basket
                        viewModel.basketItemLD.value = item
                        context?.toast(getString(R.string.product_added_basket))
                    }
                }
            }
        }

        product_info_link_button.setOnClickListener {
            itemInfo.answer?.let { info ->

                val link = info.link ?: "vmiske.ua"
                val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
                sharingIntent.type = "text/plain"
                val shareSub = getString(R.string.message)
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, shareSub)
                sharingIntent.putExtra(Intent.EXTRA_TEXT, link)

                try {
                    startActivity(Intent.createChooser(sharingIntent, getString(R.string.share)))
                } catch (e: ActivityNotFoundException) {
                    context?.toast(getString(R.string.toolbar_name_error))
                }

            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun convertDateToLong(date: String): Long {
        val df = SimpleDateFormat("yyyy-MM-dd")
        return df.parse(date).time
    }

    @SuppressLint("DefaultLocale")
    fun convertSecondsToHMmSs(seconds: Long): String? {

        val hms = java.lang.String.format(
            "%02d:%02d:%02d:%02d", TimeUnit.MILLISECONDS.toDays(seconds),
            TimeUnit.MILLISECONDS.toHours(seconds) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(seconds)),
            TimeUnit.MILLISECONDS.toMinutes(seconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(seconds)),
            TimeUnit.MILLISECONDS.toSeconds(seconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(seconds))
        )


        return hms
    }


    var item: ItemProductCatalog = ItemProductCatalog("", 0)
}

