package noty.team.miska.product


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_buy_now_send.*
import noty.team.miska.CATALOG_FRAGMENT
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.catalog.CatalogMainFragment
import noty.team.miska.model.ItemProductCatalog


class BuyNowSendFragment(var product: ItemProductCatalog) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_buy_now_send, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar("", true, false)

        send_buy_now_arrow_back.setOnClickListener {
            var fragment = CatalogMainFragment()
            fragment.setShowHideAdver(false)
            (activity as MainActivity).replaceFragment(fragment, CATALOG_FRAGMENT, false, "")
        }

        send_buy_now_back_button.setOnClickListener {
            var fragment = CatalogMainFragment()
            fragment.setShowHideAdver(false)
            (activity as MainActivity).replaceFragment(fragment, CATALOG_FRAGMENT, false, "")
        }
    }
}
