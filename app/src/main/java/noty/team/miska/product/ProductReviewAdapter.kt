package noty.team.miska.product

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_product_review_item.view.*
import noty.team.miska.R
import noty.team.miska.model.ItemProductReview
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class ProductReviewAdapter(
    private val mValues: List<ItemProductReview>,
    private val mListener: ProductReviewFragment.OnProductReviewInteraction?
) : RecyclerView.Adapter<ProductReviewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener
    var listType: Boolean? = null

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ItemProductReview
            mListener?.onProductReviewClick(item)
        }
        this.listType = listType
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view: View? = null
        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_product_review_item, parent, false)

        return ViewHolder(view!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        //"date": "2019-10-12 20:21:00"

        if (item.date != "")
            holder.mUserDate.text = convertdate(item.date)
        else {
            holder.mUserDate.text = ""
        }
        holder.mUserName.text = item.author
        holder.mUserRating.rating = item.rating
        holder.mUserText.text = item.text

        item?.answer?.let {
            if (it) {
                holder.mResponseBlock.visibility = View.VISIBLE
            } else {
                holder.mResponseBlock.visibility = View.GONE
            }

            holder.mManagerName.text = "Модератор"
            holder.mManagerText.text = item.managerText
            if (item.date != "")
                holder.mManagerDate.text = convertdate(item.date)
            else {
                holder.mManagerDate.text = ""
            }
        }

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {

        val mUserName: TextView = mView.product_review_user_name
        val mUserDate: TextView = mView.product_review_user_date
        val mUserRating: RatingBar = mView.product_review_rating_bar
        val mUserText: TextView = mView.product_review_user_text

        val mManagerName: TextView = mView.product_review_manager_name
        val mManagerDate: TextView = mView.product_review_manager_date
        val mManagerText: TextView = mView.product_review_manager_text
        val mResponseBlock: RelativeLayout = mView.review_response

    }
}

fun convertdate(srcDate: String): String {
    val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    df.setTimeZone(TimeZone.getDefault())
    val result: Date = df.parse(srcDate)

    var calendar = Calendar.getInstance()
    calendar.timeInMillis = result.time
    var formattedDate: DateFormat = SimpleDateFormat("dd MMM yyyy")
    return formattedDate.format(calendar.timeInMillis).toString()
}