package noty.team.miska.product


import android.os.Bundle
import android.text.Layout.JUSTIFICATION_MODE_INTER_WORD
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_product_description.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.modelRetrofit.ProductInfoResponse


class ProductDescriptionFragment(val itemInfo: ProductInfoResponse, paperLang: String) :
    Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_product_description, container, false)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser)
            activity?.let {
                (it as MainActivity).fabHide()
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            product_info_description_text.justificationMode = JUSTIFICATION_MODE_INTER_WORD
        }

        val text=itemInfo.answer?.info?.description ?: ""
        product_info_description_text.text = text.replace("&nbsp;","")

    }
}
