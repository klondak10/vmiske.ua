package noty.team.miska.product


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_product_review_edit_send.*
import noty.team.miska.MainActivity

import noty.team.miska.R


class ProductReviewEditSendFragment : Fragment() {

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser)
            activity?.let {
                (it as MainActivity).fabHide()
            }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_product_review_edit_send, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).showCustomAppBar("", true, false)

        activity?.let {
            (it as MainActivity).fabHide()
        }
        review_edit_send_back_button.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        review_thanks_send_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
    }
}
