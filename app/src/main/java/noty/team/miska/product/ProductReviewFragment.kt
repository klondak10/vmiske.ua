package noty.team.miska.product


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_product_review_edit.*
import kotlinx.android.synthetic.main.fragment_product_review_list.*
import noty.team.miska.MainActivity
import noty.team.miska.PAPER_LANG_DEFAULT
import noty.team.miska.R
import noty.team.miska.model.ItemProductReview
import noty.team.miska.modelRetrofit.ProductInfoResponse
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import noty.team.miska.viewmodel.LANG_RU
import java.util.regex.Pattern


const val PRODUCT_REVIEW_EDIT_SEND_FRAGMENT = "PRODUCT_REVIEW_EDIT_SEND_FRAGMENT"

class ProductReviewFragment(val itemInfo: ProductInfoResponse, paperLang: String) : Fragment() {


    private var listener: OnProductReviewInteraction? = null
    var mainList: MutableList<ItemProductReview> = arrayListOf()
    var paperLang: String = LANG_RU
    private var viewModel: DataViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(
            noty.team.miska.R.layout.fragment_product_review_list,
            container,
            false
        )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnProductReviewInteraction) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnMainCatalogListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnProductReviewInteraction {
        fun onProductReviewClick(item: ItemProductReview?)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser)
            activity?.let {
                (it as MainActivity).fabHide()
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).setResourcesByLocale(
            Paper.book().read(
                PAPER_LANG_DEFAULT,
                LANG_RU
            )
        )
        mainList.clear()

        itemInfo.answer?.reviews?.let { listReview ->

            if (listReview.size > 0) {
                review_list_empty.visibility = View.GONE
                review_list_recycle_frame.visibility = View.VISIBLE
                for (item in listReview) {
                    var newItem = ItemProductReview(
                        item?.author ?: "", item?.date ?: "",
                        (item?.rating ?: "0.0").toFloat(), item?.text ?: ""
                    )

                    if (item?.answer != null) {

                            newItem.answer = true
                            newItem.managerName = item?.answer?.name ?: ""
                            newItem.managerDate = item?.answer?.date ?: ""
                            newItem.managerText = item?.answer?.text ?: ""


                        mainList.add(newItem)
                    }else{
                        newItem.answer = false
                    }

                    recycler_list_review?.layoutManager = LinearLayoutManager(context)
                    recycler_list_review?.adapter = ProductReviewAdapter(mainList, listener)
                }


            } else {
                review_list_recycle_frame.visibility = View.GONE
                review_list_empty.visibility = View.VISIBLE
            }
        }



        product_send_review_button.setOnClickListener {

            review_list_frame.visibility = View.GONE
            review_edit_frame.visibility = View.VISIBLE

            product_send_new_review_button.setOnClickListener {
                //send new review

                if (review_edit_name.text.trim().toString().isEmpty()) {
                    context?.toast(getString(R.string.message_input_name))
                    return@setOnClickListener
                }
                if (review_edit_email.text.trim().toString().isEmpty()) {
                    context?.toast(getString(R.string.message_input_email))
                    return@setOnClickListener
                }

                val pattern1 =
                    Pattern.compile("^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+")

                val matcher1 = pattern1.matcher(review_edit_email.text)

                if (!matcher1.matches()) {
                    //show your message if not matches with email pattern
                    context?.toast(getString(R.string.message_input_correct_email))
                    return@setOnClickListener
                }

                if (review_edit_new_text.text.trim().isEmpty()) {
                    context?.toast(getString(R.string.input_review))
                    return@setOnClickListener
                }
                val pId = itemInfo.answer?.info?.productId ?: 0

                if (pId != 0)
                    viewModel?.apiSetNewProductReview(
                        pId.toString(),
                        review_edit_email.text.toString(),
                        review_edit_name.text.toString(),
                        product_review_rating_bar.rating.toString(),
                        review_edit_new_text.text.toString()
                    )?.observe(this, Observer {

                            response ->

                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data?.error == false) {

                                (activity as MainActivity).replaceFragment(
                                    ProductReviewEditSendFragment(),
                                    PRODUCT_REVIEW_EDIT_SEND_FRAGMENT,
                                    false, ""
                                )
                            } else {
                                context?.toast(data?.message.toString())
                            }
                        } else {
                        }
                    })
            }
        }
    }

}
