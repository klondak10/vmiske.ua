package noty.team.miska.product

import android.content.Context
import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import noty.team.miska.R
import noty.team.miska.modelRetrofit.ProductInfoResponse
import noty.team.miska.util.InfoNoInternetFragment


private val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.tab_text_2,
    R.string.tab_text_3,
    R.string.tab_text_4

)

class ProductPagerAdapter(
    private val context: Context, fm: FragmentManager,
    private val itemInfo: ProductInfoResponse, val paperLang: String
) : FragmentStatePagerAdapter(fm, BEHAVIOR_SET_USER_VISIBLE_HINT) {
    //FragmentStatePagerAdapter
    override fun getItem(position: Int): Fragment {
        var fragmnet: Fragment = InfoNoInternetFragment()

        when (position) {
            0 -> fragmnet = ProductInfoFragment(itemInfo, paperLang)
            1 -> fragmnet = ProductReviewFragment(itemInfo, paperLang)
            2 -> fragmnet = ProductDescriptionFragment(itemInfo, paperLang)
            3 -> fragmnet = ProductPropertyFragment(itemInfo, paperLang)
        }
        return fragmnet
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return 4
    }



    var registeredFragments = SparseArray<Fragment>()

    override fun instantiateItem(container: View, position: Int): Any {
        //return super.instantiateItem(container, position)
        val fragment = super.instantiateItem(container, position) as Fragment
        registeredFragments.put(position, fragment)
        return fragment
    }

    override fun destroyItem(container: ViewGroup, position: Int, objectFr: Any) {
        //super.destroyItem(container, position, `object`)
        registeredFragments.remove(position)
        super.destroyItem(container, position, objectFr)
    }

    fun getRegisteredFragment(position: Int): Fragment {
        return registeredFragments.get(position)
    }
}
