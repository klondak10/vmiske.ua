package noty.team.miska.product


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_catalog_product_info_pager.*
import noty.team.miska.MainActivity
import noty.team.miska.PAPER_LANG_DEFAULT
import noty.team.miska.PAPER_LIST_TYPE_PRODUCT
import noty.team.miska.R
import noty.team.miska.model.ItemProductCatalog
import noty.team.miska.viewmodel.DataViewModel
import noty.team.miska.viewmodel.LANG_RU


class CatalogProductInfoPagerFragment(var caption: String, var item: ItemProductCatalog?) :
    Fragment() {
    var paperTypeList: Boolean = true
    var paperLang: String = LANG_RU
    private var viewModel: DataViewModel? = null

    override fun onPause() {
        progressBar?.visibility = View.GONE
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        if (item == null)
            progressBar?.visibility = View.GONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_catalog_product_info_pager, container, false)
    }

    var progressBar: ProgressBar? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).showCustomAppBar(caption, false, true)
        activity?.let {
            (it as MainActivity).fabShow()
        }

        progressBar = ProgressBar(context, null, android.R.attr.progressBarStyleLarge)
        val params = RelativeLayout.LayoutParams(100, 100)
        params.addRule(RelativeLayout.CENTER_IN_PARENT)
        var mainView = product_layout_viewpager
        mainView.addView(progressBar, params)

        if (item == null) {


                progressBar?.visibility = View.VISIBLE
                paperTypeList = Paper.book().read(PAPER_LIST_TYPE_PRODUCT, true)
                paperLang = Paper.book().read(PAPER_LANG_DEFAULT, LANG_RU)

                viewModel?.apiGetProductInfo(paperLang, Paper.book().read<String>("roductID").toString())
                    ?.observe(this, Observer {

                            response ->
                        if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED))
                            if (response.isSuccessful) {
                                val data = response.body()
                                if (data?.error == false) {
                                    val list = data?.answer
                                    product_view_pager.adapter = ProductPagerAdapter(
                                        context!!,
                                        childFragmentManager,
                                        data,
                                        paperLang
                                    )
                                    product_tabs.setupWithViewPager(product_view_pager)
                                    val tab = product_tabs.getTabAt(0)
                                    tab?.select()
                                }
                            }
                        progressBar?.visibility = View.GONE
                    })


        } else {
            item?.let { item ->

                progressBar?.visibility = View.VISIBLE
                paperTypeList = Paper.book().read(PAPER_LIST_TYPE_PRODUCT, true)
                paperLang = Paper.book().read(PAPER_LANG_DEFAULT, LANG_RU)

                val searchString = (activity as MainActivity).productIdSubCategory
                viewModel?.apiGetProductInfo(paperLang, item.productId.toString())
                    ?.observe(this, Observer {

                            response ->
                        if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED))
                            if (response.isSuccessful) {
                                val data = response.body()
                                if (data?.error == false) {
                                    val list = data?.answer
                                    product_view_pager.adapter = ProductPagerAdapter(
                                        context!!,
                                        childFragmentManager,
                                        data,
                                        paperLang
                                    )
                                    product_tabs.setupWithViewPager(product_view_pager)
                                    val tab = product_tabs.getTabAt(0)
                                    tab?.select()
                                }
                            }
                        progressBar?.visibility = View.GONE
                    })
            }
        }
    }

}
