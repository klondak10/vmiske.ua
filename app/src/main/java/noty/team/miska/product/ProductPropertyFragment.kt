package noty.team.miska.product


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_product_property.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.model.ItemProductProperty
import noty.team.miska.modelRetrofit.ProductInfoResponse


class ProductPropertyFragment(val itemInfo: ProductInfoResponse, paperLang: String) : Fragment() {

    private var listener: ProductReviewFragment.OnProductReviewInteraction? = null

    var mainList: MutableList<ItemProductProperty> = arrayListOf()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product_property, container, false)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser)
            activity?.let {
                (it as MainActivity).fabHide()
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainList.clear()
        mainList.clear()

        itemInfo.answer?.spec?.let { listProperty ->

            if (listProperty.size > 0) {

                for (item in listProperty) {
                    val newItem = ItemProductProperty(
                        item?.name ?: "", item?.text ?: ""
                    )
                    mainList.add(newItem)
                }

                recycler_list_property?.layoutManager = LinearLayoutManager(context)
                recycler_list_property?.adapter = ProductPropertyAdapter(mainList)
            }
        }
    }


}
