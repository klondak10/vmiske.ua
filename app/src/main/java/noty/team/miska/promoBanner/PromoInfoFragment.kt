package noty.team.miska.promoBanner


import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_promo_info.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.catalog.INFINITE_INFO_FRAGMENT
import noty.team.miska.util.WebviewFragment


class PromoInfoFragment : Fragment() {
    var imageId: Drawable? = null
    var imageUrl:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_promo_info, container, false)
    }


    fun setInfoImageId(image: Drawable) {
        this.imageId = image
    }

    fun setUrlImage(infoUrl: String) {
        this.imageUrl = infoUrl
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(getString(R.string.toolbar_name_promo_action), true, false)
        if (imageId != null)
            infinite_info_image_view.setImageDrawable(imageId)

        promo_app_now_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        promo_next_button.setOnClickListener{

            (activity as MainActivity).replaceFragment(
                WebviewFragment(imageUrl),
                INFINITE_INFO_FRAGMENT,
                false,
                ""
            )
        }
    }

}
