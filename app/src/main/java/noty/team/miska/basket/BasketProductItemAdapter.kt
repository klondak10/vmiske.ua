package noty.team.miska.basket


import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_basket_product_item.view.*
import noty.team.miska.R
import noty.team.miska.catalog.CURRENCY
import noty.team.miska.model.ItemProductCatalog
import noty.team.miska.viewmodel.IMAGE_URL

enum class BasketOperations {
    PLUS,
    MINUS,
    DELETE
}

class BasketProductItemAdapter(
    private val mValues: List<ItemProductCatalog>,
    private val mListener: BasketProductFragment.OnBasketProductInteraction?,
    var callback: (ops: BasketOperations, itemPos: Int, itemDelete: ItemProductCatalog) -> Unit
) : RecyclerView.Adapter<BasketProductItemAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener


    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ItemProductCatalog
            mListener?.onBasketListClick(item)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view: View? = null
        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_basket_product_item, parent, false)
        return ViewHolder(view!!)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        Picasso.get()
            .load(IMAGE_URL + item.imageUrl)
            .placeholder(R.drawable.placeholder_small)
            .error(R.drawable.placeholder_small)
            .into(holder.mImage)

        var newName = item.name.replace("&quot;", "")
        var newName2 = newName.replace("amp;", "")

        holder.mName.text = "$newName2 \n\n${item.basketItemInfo?.optionValue} : ${item.basketItemInfo?.optionDescription} "

        if (item.priceCurrency == "") {
            item.priceCurrency = CURRENCY
        }


        val newPrice = item.price.toInt() + (item.basketItemInfo?.optionPrice ?: 0)
        if (item.basketItemInfo != null) {
            item.newPrice = newPrice

        } else {

        }

        if (item.quantity > 0) {
            var sum = item.price.toInt() * item.quantity
            if (item.newPrice != null)
                holder.mPrice.text = item.newPrice.toString() + " " + item.priceCurrency
            else
                holder.mPrice.text = item.price + " " + item.priceCurrency
            holder.mQuantity.text = item.quantity.toString()
        } else {
            if (item.newPrice != null)
                holder.mPrice.text = item.newPrice.toString() + " " + item.priceCurrency
            else
                holder.mPrice.text = item.price + " " + item.priceCurrency
        }

        holder.mPlusButton.setOnClickListener {

            if (item.quantity <= 1000) {

                item.quantity++
                callback(BasketOperations.PLUS, position, item)

                holder.mQuantity.text = item.quantity.toString()

                holder.mMinusButton.setImageResource(R.drawable.stepper_minus_red)

                if (item.quantity > 0) {
                    if (item.newPrice != null)
                        holder.mPrice.text = item.newPrice.toString() + " " + item.priceCurrency
                    else
                        holder.mPrice.text = item.price + " " + item.priceCurrency
                } else {
                    if (item.newPrice != null)
                        holder.mPrice.text = item.newPrice.toString() + " " + item.priceCurrency
                    else
                        holder.mPrice.text = item.price + " " + item.priceCurrency
                }
            }
        }

        holder.mMinusButton.setOnClickListener {
            if (item.quantity > 1) {
                item.quantity--

                holder.mQuantity.text = item.quantity.toString()
                callback(BasketOperations.MINUS, position, item)

                if (item.quantity == 1) {
                    holder.mMinusButton.setImageResource(R.drawable.stepper_minus_grey)
                }
                if (item.quantity > 0) {
                    if (item.newPrice != null)
                        holder.mPrice.text = item.newPrice.toString() + " " + item.priceCurrency
                    else
                        holder.mPrice.text = item.price + " " + item.priceCurrency
                } else {
                    if (item.newPrice != null)
                        holder.mPrice.text = item.newPrice.toString() + " " + item.priceCurrency
                    else
                        holder.mPrice.text = item.price + " " + item.priceCurrency
                }

            }
        }

        if (item.quantity == 1) {
            holder.mMinusButton.setImageResource(R.drawable.stepper_minus_grey)
        } else {
            holder.mMinusButton.setImageResource(R.drawable.stepper_minus_red)
        }
        holder.mDeleteButton.setOnClickListener {
            callback(BasketOperations.DELETE, position, item)
        }
        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {

        val mImage: ImageView = mView.basket_product_info_image
        val mName: TextView = mView.basket_product_info_name
        val mPrice: TextView = mView.basket_product_info_price
        val mDeleteButton: ImageView = mView.basket_delete_button
        val mMinusButton: ImageView = mView.basket_minus_button
        val mPlusButton: ImageView = mView.basket_plus_button
        val mQuantity: TextView = mView.basket_product_quantity
    }
}
