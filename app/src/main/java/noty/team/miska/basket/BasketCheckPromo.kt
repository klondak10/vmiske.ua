package noty.team.miska.basket


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_basket_check_promo.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.modelRetrofit.AnswerPromo
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel

class BasketCheckPromo(var sum: Int) : Fragment() {
    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_basket_check_promo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.promo_code),
            true,
            false
        )

        promo_check_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }


        promo_check_edit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                promo_check_layout.error = getString(R.string.input_promo)
            }
        })

        promo_check_button.setOnClickListener {

            promo_check_layout.error = ""
            var promoCode = promo_check_edit.text.toString()
            if (promoCode.isEmpty()) {
                promo_check_layout.error = getString(R.string.input_promo)
                return@setOnClickListener
            }

            var token = Paper.book().read(PAPER_TOKEN, "")
            if (token != "") {
                token = TOKEN_PREFIX + token
                viewModel.apiCheckPromoCode(
                    token, promoCode
                ).observe(this, Observer { response ->
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data?.error == false) {

                            var info =
                                data.answer?.name + " " + getString(R.string.min_order_sum) + data.answer?.minsumm.toString() +
                                        getString(R.string.grn)
                            context?.toast(info)
                            promo_check_layout.error = info


                            if (sum >= data.answer?.minsumm ?: 0) {
                                viewModel.promoCode = promoCode
                                viewModel.promoCodeAnswer =data.answer?: AnswerPromo()

                                GlobalScope.launch (Dispatchers.Main){
                                    kotlinx.coroutines.delay(1000)
                                    (activity as MainActivity).onBackPressed()
                                }

                            } else {
                                context?.toast(getString(R.string.sum_less_min_sum))
                            }


                        } else {
                            context?.toast(data?.message.toString())
                            promo_check_layout.error = getString(R.string.promo_not_work)
                        }
                    } else
                        context?.toast(response?.message().toString())
                })
            }
        }

    }
}
