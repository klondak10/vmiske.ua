package noty.team.miska.basket

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import noty.team.miska.R

class CountDrawable(context: Context) : Drawable() {

    private val mBadgePaint: Paint
    private val mTextPaint: Paint
    private val mTxtRect = Rect()

    private var mCount = ""
    private var mWillDraw: Boolean = false

    init {

        val mTextSize = context.resources.getDimension(R.dimen.basket_counter_text_size_6)
        mBadgePaint = Paint()
        mBadgePaint.setColor(
            ContextCompat.getColor(
                context.getApplicationContext(),
                R.color.colorMainRed
            )
        )
        mBadgePaint.setAntiAlias(true)
        mBadgePaint.setStyle(Paint.Style.FILL)

        mTextPaint = Paint()
        mTextPaint.setColor(Color.WHITE)
        mTextPaint.setTypeface(Typeface.DEFAULT)
        mTextPaint.setTextSize(mTextSize)
        mTextPaint.setAntiAlias(true)
        mTextPaint.setTextAlign(Paint.Align.CENTER)
    }

    override fun draw(canvas: Canvas) {

        if (!mWillDraw) {
            return
        }
        val bounds = bounds
        val width = bounds.right - bounds.left
        val height = bounds.bottom - bounds.top


        val radius = Math.max(width, height) / 2 / 2
        val centerX = width - radius - 1f + 5
        val centerY: Float = radius.toFloat() - 5
        if (mCount.length <= 2) {
            canvas.drawCircle(
                centerX,
                centerY.toFloat(),
                (radius.toFloat() + 5.5).toFloat(),
                mBadgePaint
            )
        } else {
            canvas.drawCircle(centerX, centerY, (radius + 6.5).toFloat(), mBadgePaint)
        }
        mTextPaint.getTextBounds(mCount, 0, mCount.length, mTxtRect)
        val textHeight = mTxtRect.bottom - mTxtRect.top
        val textY = centerY + textHeight / 2f
        if (mCount.length > 2)
            canvas.drawText("99+", centerX, textY, mTextPaint)
        else
            canvas.drawText(mCount, centerX, textY, mTextPaint)
    }


    fun setCount(count: String) {
        mCount = count

        mWillDraw = true

        invalidateSelf()
    }

    override fun setAlpha(alpha: Int) {
    }

    override fun setColorFilter(cf: ColorFilter?) {
    }

    override fun getOpacity(): Int {
        return PixelFormat.UNKNOWN
    }
}