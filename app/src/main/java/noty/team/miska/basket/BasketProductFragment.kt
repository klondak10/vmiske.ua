package noty.team.miska.basket

import android.content.Context
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_basket_product_list.*
import noty.team.miska.BASKET_EMPTY_FRAGMENT
import noty.team.miska.CATALOG_FRAGMENT
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.catalog.CURRENCY
import noty.team.miska.catalog.CatalogMainFragment
import noty.team.miska.checkout.CheckoutPagerFragment
import noty.team.miska.checkout.CheckoutPickupFragment
import noty.team.miska.login.CHECKOUT_PICKUP_FRAGMENT
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.model.ItemProductCatalog
import noty.team.miska.modelRetrofit.AnswerPromo
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import kotlin.math.roundToInt


const val CHECKOUT_PAGER_FRAGMENT = "CHECKOUT_PAGER_FRAGMENT"
const val CHECKOUT_SUCCESS_FRAGMENT = "CHECKOUT_SUCCESS_FRAGMENT"
const val MIN_ORDER_SUM = 500

class BasketProductFragment : Fragment() {
    private var listener: OnBasketProductInteraction? = null
    private var mainList: MutableList<ItemProductCatalog> = arrayListOf()
    private var mainListNewBasket: MutableList<ItemProductCatalog> = arrayListOf()
    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }
    private var checkOutFlag = false
    private var bonuses = 0


    fun setCheckoutFlag(flag: Boolean) {
        checkOutFlag = flag
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_basket_product_list, container, false)
        return view
    }


    private fun reCheckPromo() {

        val bonus = viewModel.bonusesUsed
        val startSum = sumList(mainList)


        checkHideBonuseLayout()


        if (title_total_sum != null)
            if (viewModel.promoCode != "") {
                viewModel.promoCodeAnswer?.let { answerPromo ->

                    if (startSum >= viewModel.promoCodeAnswer?.minsumm ?: 0) {
                        title_total_sum_promo.visibility = View.VISIBLE
                        title_total_sum_promo.paintFlags =
                            title_total_sum_promo.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        title_total_sum_promo.text = startSum.toString() + CURRENCY
                        if (answerPromo.type == "P") {
                            //calc %
                            val pr = answerPromo.summ ?: 0
                            val resSum: Float = startSum - (startSum.toFloat() / 100) * pr
                            val rounSum = resSum.roundToInt()
                            var total = rounSum - bonus
                            if (total < 0) total = 0
                            viewModel.sumPromoBonus = total
                            title_total_sum.text = total.toString() + CURRENCY
                        } else {
                            //if "F" then -
                            val minus = answerPromo.summ ?: 0
                            val resSum = startSum - minus
                            var total = resSum - bonus
                            if (total < 0) total = 0
                            viewModel.sumPromoBonus = total
                            title_total_sum.text = total.toString() + CURRENCY

                        }
                    } else {
                        title_total_sum_promo.visibility = View.GONE
                        title_total_sum_promo.text = ""
                        title_total_sum.text = startSum.toString() + CURRENCY
                        viewModel.sumPromoBonus = startSum
                    }
                }
            } else {
                //only bonuses
                if (startSum >= MIN_ORDER_SUM && bonus > 0) {
                    var total = startSum - bonus
                    if (total < 0) total = 0
                    viewModel.sumPromoBonus = total
                    title_total_sum?.text = total.toString() + CURRENCY
                    title_total_sum_promo?.visibility = View.VISIBLE
                    title_total_sum_promo?.paintFlags =
                        title_total_sum_promo.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    title_total_sum_promo?.text = startSum.toString() + CURRENCY


                } else {
                    title_total_sum_promo?.visibility = View.GONE
                    title_total_sum_promo?.text = ""
                    viewModel.bonusesUsed = 0
                    viewModel.sumPromoBonus = startSum
                    // showNoProducts()
                }
            }

    }

    fun removeItemList(itemDelete: ItemProductCatalog, pos: Int) {
        mainList.let {

            var deleteId = itemDelete.productId
            for (i in 0 until mainList.size) {
                if (i == pos) {
                    mainList.removeAt(i)
                    recycler_basket_list.adapter?.notifyDataSetChanged()
                    if (mainList.size == 0) {
                        listener?.onBasketShowEmpty()
                        showNoProducts()
                    }
                    viewModel.basketList.clear()
                    viewModel.basketList.addAll(mainList)
                    viewModel.basketCountLD.value = countList(mainList)
                }
            }
        }
    }


    var token = ""
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_basket),
            false,
            false
        )
        token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
        }

        mainList.clear()
        mainList.addAll(viewModel.basketList)

        recycler_basket_list?.layoutManager = LinearLayoutManager(context)


        recycler_basket_list?.adapter = BasketProductItemAdapter(mainList, listener)
        { basketOperations: BasketOperations, pos: Int, item: ItemProductCatalog ->

            viewModel.basketList.clear()
            viewModel.basketList.addAll(mainList)

            if (basketOperations == BasketOperations.PLUS || basketOperations == BasketOperations.MINUS) {
                for (itemPos in 0..mainList.size) {
                    if (pos == itemPos) {

                        val count = mainList[pos].quantity
                        viewModel.basketList[pos].quantity = count
                        if (token != "") {
                            viewModel.apiUpdateBasket(
                                token,
                                viewModel.basketList[pos].basketId ?: 0,
                                count
                            ).observe(this, Observer { response ->
                                if (response.isSuccessful) {
                                    val data = response.body()
                                    if (data?.error == false) {
                                        context?.toast(getString(R.string.products_count_changed))
                                        viewModel.basketCountLD.value = countList(mainList)
                                        reloadBasket()
                                    } else
                                        context?.toast(data?.message.toString())
                                } else
                                    context?.toast(response?.message().toString())
                            })
                        } else {
                            //if no user logged in
                            context?.toast(getString(R.string.products_count_changed))
                            viewModel.basketCountLD.value = countList(mainList)
                        }
                    }
                }
            }
            //delete item
            if (basketOperations == BasketOperations.DELETE) {

                if (token != "")
                    viewModel.apiDeleteBasket(token, item.basketId ?: 0)
                        .observe(this, Observer { response ->
                            if (response.isSuccessful) {
                                val data = response.body()
                                if (data?.error == false) {
                                    context?.toast(getString(R.string.product_deleted))

                                    reloadBasket()
                                } else {
                                    context?.toast(data?.message.toString())
                                    (activity as MainActivity).getBasketFromServer(false, ({}))
                                }
                            } else
                                context?.toast(response?.message().toString())
                        }) else {
                    //if no user loggedin
                    context?.toast(getString(R.string.product_deleted))
                }
                removeItemList(item, pos)

            }
            renewInfoBasket()

        }

        if (checkOutFlag) {
            //mainListNewBasket = viewModel.basketList

            (activity as MainActivity).getBasketFromServer(true, ({
                mainList.clear()
                mainList.addAll(viewModel.basketList)
                recycler_basket_list?.adapter?.notifyDataSetChanged()

                val counter = countList(mainListNewBasket)
                (activity as MainActivity).showBasketCounter(counter)
                (activity as MainActivity).setMenuCounter(counter, true)
                showNoProducts()

            }))

            checkOutFlag = false
        } else {
            (activity as MainActivity).getBasketFromServer(false, ({

                if (activity != null) {
                    (activity as MainActivity).getBasketFromServer(false, ({
                        mainList.clear()
                        mainList.addAll(viewModel.basketList)
                        recycler_basket_list?.adapter?.notifyDataSetChanged()

                        title_total_sum?.text = sumList(mainList).toString() + CURRENCY
                        val counter = countList(mainList)
                        (activity as MainActivity).showBasketCounter(counter)
                        (activity as MainActivity).setMenuCounter(counter, true)
                        reCheckPromo()
                        if (mainList.size > 0)
                            showNoProducts()
                    }))
                }
            }))
        }
        title_total_sum.setText(sumList(mainList).toString() + CURRENCY)
        reCheckPromo()





        basket_bottom_promo_code_button.setOnClickListener {
            if (bottom_sheet_bonus_info.visibility == View.VISIBLE)
                bottom_sheet_bonus_info.visibility = View.GONE
            else {

                bottom_sheet_bonus_info.visibility = View.VISIBLE
                checkHideBonuseLayout()
            }
        }

        basket_bottom_make_order_button.setOnClickListener {


            if (token == "") {
                (activity as MainActivity).replaceFragment(
                    CheckoutPagerFragment(true),
                    CHECKOUT_PAGER_FRAGMENT, false, getString(R.string.toolbar_name_authorization)
                )
            } else {
                (activity as MainActivity).replaceFragment(
                    CheckoutPickupFragment(),
                    CHECKOUT_PICKUP_FRAGMENT, false, ""
                )
            }
        }

        basket_back_button.setOnClickListener {
            var fragment = CatalogMainFragment()
            fragment.setShowHideAdver(false)
            (activity as MainActivity).replaceFragment(fragment, CATALOG_FRAGMENT, true, "")
        }


        title_total_sum.text = sumList(mainList).toString() + CURRENCY

        getProfileBonuses()


        check_promo_button.setOnClickListener {
            if (token != "")
                (activity as MainActivity).replaceFragment(
                    BasketCheckPromo(sumList(mainList)),
                    "",
                    false,
                    ""
                ) else {
                context?.toast(getString(R.string.check_promo_cabinet))
            }
        }

        bottom_sheet_caption_hint.setOnClickListener {

            if (token != "")
                (activity as MainActivity).replaceFragment(
                    BasketUseBonuse(bonuses, sumList(mainList)),
                    "",
                    false,
                    ""
                ) else {
                context?.toast(getString(R.string.check_bonus_cabinet))
            }
        }
        bottom_sheet_caption.setOnClickListener { bottom_sheet_caption_hint.performClick() }
        bottom_sheet_bonus.setOnClickListener { bottom_sheet_caption_hint.performClick() }

        checkHideBonuseLayout()
        showNoProducts()
    }

    fun checkHideBonuseLayout() {
        val startSum = sumList(mainList)
        if (startSum < MIN_ORDER_SUM) {

            bottom_sheet_caption?.visibility = View.INVISIBLE
            bottom_sheet_caption_hint?.visibility = View.INVISIBLE
            bottom_sheet_bonus?.visibility = View.INVISIBLE
            bottom_line?.visibility = View.INVISIBLE

            val params = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
            )
            params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE)
            check_promo_button?.layoutParams = params
        } else
            if (startSum >= MIN_ORDER_SUM && token != "") {
                bottom_sheet_caption?.visibility = View.VISIBLE
                bottom_sheet_caption_hint?.visibility = View.VISIBLE
                bottom_sheet_bonus?.visibility = View.VISIBLE
                bottom_line?.visibility = View.VISIBLE

                val params = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
                )
                params.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE)
                ///.setMargins(top, left, bottom, right);
                params.setMargins(8, 8, 16, 16)
                check_promo_button?.layoutParams = params
            } else {
                //user not logged in
                bottom_sheet_caption?.visibility = View.INVISIBLE
                bottom_sheet_caption_hint?.visibility = View.INVISIBLE
                bottom_sheet_bonus?.visibility = View.INVISIBLE
                bottom_line?.visibility = View.INVISIBLE

                val params = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
                )
                params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE)
                check_promo_button?.layoutParams = params

            }

    }

    fun showNoProducts() {
        if (mainList.size == 0) {

            viewModel.bonusesUsed = 0
            viewModel.promoCode = ""
            viewModel.promoCodeAnswer = AnswerPromo()

            (activity as MainActivity).replaceFragment(
                BasketProductEmptyFragment(),
                BASKET_EMPTY_FRAGMENT,
                false,
                getString(R.string.toolbar_name_basket)
            )
        } else {

            mainList.clear()
            mainList.addAll(viewModel.basketList)
            recycler_basket_list?.adapter?.notifyDataSetChanged()
            renewInfoBasket()
        }
    }

    fun reloadBasket() {
        title_total_sum.text = sumList(mainList).toString() + CURRENCY
        val counter = countList(mainList)
        (activity as MainActivity).showBasketCounter(counter)
        (activity as MainActivity).setMenuCounter(counter, true)
        //reload basket important
        (activity as MainActivity).getBasketFromServer(false, ({
            showNoProducts()
        }))
        reCheckPromo()
    }

    fun renewInfoBasket() {
        title_total_sum?.text = sumList(mainList).toString() + CURRENCY
        val counter = countList(mainList)
        (activity as MainActivity).showBasketCounter(counter)
        (activity as MainActivity).setMenuCounter(counter, true)
        reCheckPromo()
    }

    fun sumList(list: MutableList<ItemProductCatalog>): Int {
        var startSum = 0
        for (itemPos in 0 until list.size) {
            var sum = 0
            var mainPr = list[itemPos].price.toInt()
            if (list[itemPos].basketItemInfo != null) {

                var optPrice = mainPr + (list[itemPos].basketItemInfo?.optionPrice ?: 0)
                sum = optPrice * list[itemPos].quantity
            } else {
                sum = list[itemPos].price.toInt() * list[itemPos].quantity
            }
            startSum += sum
        }
        return startSum
    }

    fun countList(list: MutableList<ItemProductCatalog>): Int {
        return list.size
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnBasketProductInteraction) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnBasketProductInteraction")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    fun getProfileBonuses() {
        var token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
            viewModel.apiGetProfile(
                token
            ).observe(this, Observer { response ->
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {

                        if (data.answer?.bonus == null) {
                            bonuses = 0

                        } else {
                            bonuses = data.answer?.bonus?.toInt() ?: 0
                        }
                        bottom_sheet_bonus?.text = bonuses.toString()

                    } else
                        context?.toast(data?.message.toString())
                } else
                    context?.toast(response?.message().toString())
            })
        }
    }

    interface OnBasketProductInteraction {
        fun onBasketListClick(item: ItemProductCatalog?)
        fun onBasketDeleteClick(item: ItemProductCatalog?)
        fun onBasketShowEmpty()
    }


}
