package noty.team.miska.basket


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_basket_product_empty.*
import noty.team.miska.MainActivity
import noty.team.miska.R


class BasketProductEmptyFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_basket_product_empty, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_basket),
            false,
            false
        )

        basket_empty_back_button.setOnClickListener {

            (activity as MainActivity).showCatalog(false)
        }
    }

}
