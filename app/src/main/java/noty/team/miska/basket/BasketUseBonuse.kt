package noty.team.miska.basket


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_basket_use_bonuse.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.catalog.CURRENCY
import noty.team.miska.viewmodel.DataViewModel

/**
 * A simple [Fragment] subclass.
 */
class BasketUseBonuse(var bonuses: Int, var sum: Int) : Fragment() {

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_basket_use_bonuse, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.promo_code),
            true,
            false
        )
        viewModel.bonusesUsed=0

        bonus_order_sum_data.text=sum.toString()+ CURRENCY

        use_bonus_check_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        bonus_use_data.text = bonuses.toString()


        use_bonus_check_edit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                use_bonus_check_layout.error = getString(R.string.input_bonuse)
            }
        })

        use_bonus_check_button.setOnClickListener {

            use_bonus_check_layout.error = ""
            var bonusInput = use_bonus_check_edit.text.toString()

            if (bonusInput.isEmpty()) {
                use_bonus_check_layout.error = getString(R.string.input_bonuses)
                return@setOnClickListener
            }

            if (bonusInput.toInt() > sum) {
                use_bonus_check_layout.error =
                    getString(R.string.bonus_over_sum) + " " + sum.toString() + " " + getString(
                        R.string.grn_
                    )
                return@setOnClickListener
            }
            if (bonusInput.toInt() > bonuses) {
                use_bonus_check_layout.error = getString(R.string.bonus_over_bonuses)
                return@setOnClickListener
            }

            if ( sum<500) {
                use_bonus_check_layout.error =
                    getString(R.string.sum_greater_500)
                     getString(
                        R.string.grn_
                    )
                return@setOnClickListener
            }
            viewModel.bonusesUsed=use_bonus_check_edit.text.toString().toInt()
            (activity as MainActivity).onBackPressed()
        }

    }

}
