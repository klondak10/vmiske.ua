package noty.team.miska.info


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_info_list_detail_help.*
import noty.team.miska.MainActivity
import noty.team.miska.R


class InfoListDetailHelpFragment() : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_info_list_detail_help, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_help),
            false,
            true
        )

        (activity as MainActivity).hamburgerShow(false)

        help_menu_detail.visibility = View.GONE
        help_menu_detail_2.visibility = View.GONE
        help_menu_detail_3.visibility = View.GONE
        help_menu_detail_4.visibility = View.GONE
        help_menu_detail_5.visibility = View.GONE

        help_menu_expand_button.setOnClickListener {
            if (help_menu_detail.visibility == View.GONE) {
                help_menu_expand_button.setBackgroundResource(R.drawable.arrow_up_grey)
                help_menu_detail.visibility = View.VISIBLE
            } else {
                help_menu_expand_button.setBackgroundResource(R.drawable.arrow_down_grey)
                help_menu_detail.visibility = View.GONE
            }
        }
        help_menu_caption_layout.setOnClickListener {
            help_menu_expand_button.callOnClick()
        }

        help_menu_expand_button_2.setOnClickListener {
            if (help_menu_detail_2.visibility == View.GONE) {
                help_menu_expand_button_2.setBackgroundResource(R.drawable.arrow_up_grey)
                help_menu_detail_2.visibility = View.VISIBLE
            } else {
                help_menu_expand_button_2.setBackgroundResource(R.drawable.arrow_down_grey)
                help_menu_detail_2.visibility = View.GONE
            }
        }
        help_menu_caption_layout_2.setOnClickListener {
            help_menu_expand_button_2.callOnClick()
        }

        help_menu_expand_button_3.setOnClickListener {
            if (help_menu_detail_3.visibility == View.GONE) {
                help_menu_expand_button_3.setBackgroundResource(R.drawable.arrow_up_grey)
                help_menu_detail_3.visibility = View.VISIBLE
            } else {
                help_menu_expand_button_3.setBackgroundResource(R.drawable.arrow_down_grey)
                help_menu_detail_3.visibility = View.GONE
            }
        }
        help_menu_caption_layout_3.setOnClickListener {
            help_menu_expand_button_3.callOnClick()
        }

        help_menu_expand_button_4.setOnClickListener {
            if (help_menu_detail_4.visibility == View.GONE) {
                help_menu_expand_button_4.setBackgroundResource(R.drawable.arrow_up_grey)
                help_menu_detail_4.visibility = View.VISIBLE
            } else {
                help_menu_expand_button_4.setBackgroundResource(R.drawable.arrow_down_grey)
                help_menu_detail_4.visibility = View.GONE
            }
        }
        help_menu_caption_layout_4.setOnClickListener {
            help_menu_expand_button_4.callOnClick()
        }

        help_menu_expand_button_5.setOnClickListener {
            if (help_menu_detail_5.visibility == View.GONE) {
                help_menu_expand_button_5.setBackgroundResource(R.drawable.arrow_up_grey)
                help_menu_detail_5.visibility = View.VISIBLE
            } else {
                help_menu_expand_button_5.setBackgroundResource(R.drawable.arrow_down_grey)
                help_menu_detail_5.visibility = View.GONE
            }
        }
        help_menu_caption_layout_5.setOnClickListener {
            help_menu_expand_button_5.callOnClick()
        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        (activity as MainActivity).hamburgerShow(true)
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).hamburgerShow(false)
    }

    override fun onPause() {
        super.onPause()
        (activity as MainActivity).hamburgerShow(true)
    }

}

