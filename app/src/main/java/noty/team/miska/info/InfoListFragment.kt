package noty.team.miska.info


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_info_list.*
import noty.team.miska.CATALOG_FRAGMENT
import noty.team.miska.MainActivity
import noty.team.miska.R


class InfoListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_info_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).showCustomAppBar(
            getString(R.string.status_bar_info),
            false,
            true
        )

        (activity as MainActivity).hamburgerShow(true)

        var arrayInfo: Array<String> = resources.getStringArray(R.array.items_info_menu)
        var adapter = ArrayAdapter(activity!!, R.layout.fragment_infolist_item, arrayInfo)
        info_list_view.setAdapter(adapter)
        info_list_view.setOnItemClickListener { adapterView: AdapterView<*>, view1: View, pos: Int, id: Long ->

            var selectedItem = info_list_view.getItemAtPosition(pos)
            if (selectedItem is String)


                when (pos) {
                    0 -> {
                        (activity as MainActivity).replaceFragment(
                            InfoListDetailFragment(selectedItem.toString(),getString(R.string.info_about_us)),
                            CATALOG_FRAGMENT,
                            false,
                            getString(R.string.toolbar_name_about_us)
                        )
                    }
                    1 -> {
                        (activity as MainActivity).replaceFragment(
                            InfoListDetailHelpFragment(),
                            CATALOG_FRAGMENT,
                            false,
                            getString(R.string.toolbar_name_help)
                        )
                    }
                    2 -> {
                        (activity as MainActivity).replaceFragment(
                            InfoListDetailFragment(selectedItem.toString(),getString(R.string.info_delivery_payment)),
                            CATALOG_FRAGMENT,
                            false,
                            getString(R.string.toolbar_name_payment_delivery)
                        )
                    }
                    3 -> {
                        (activity as MainActivity).replaceFragment(
                            InfoListDetailFragment(selectedItem.toString(),getString(R.string.label_zgray_message)),
                            CATALOG_FRAGMENT,
                            false,
                            getString(R.string.label_zgray)
                        )
                    }
                    4 -> {
                        (activity as MainActivity).replaceFragment(
                            InfoListDetailFragment(selectedItem.toString(),getString(R.string.info_ambar)),
                            CATALOG_FRAGMENT,
                            false,
                            getString(R.string.toolbar_name_ambar)
                        )
                    }
                    5 -> {
                        (activity as MainActivity).replaceFragment(
                            InfoListDetailFragment(selectedItem.toString(),getString(R.string.info_terms)),
                            CATALOG_FRAGMENT,
                            false,
                            getString(R.string.toolbar_name_rules_use)
                        )
                    }
                }
        }
    }
}