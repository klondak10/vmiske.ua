package noty.team.miska.info


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_info_list_detail.*
import noty.team.miska.MainActivity
import noty.team.miska.R


class InfoListDetailFragment(helpCaption: String, val helpInfo: String) : Fragment() {
    var caption = ""

    init {
        caption = helpCaption
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            noty.team.miska.R.layout.fragment_info_list_detail,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).showCustomAppBar(caption, false, true)
        (activity as MainActivity).hamburgerShow(false)
        fragment_info_list_detail_text.text = helpInfo

        if (caption == getString(R.string.toolbar_name_about_us)) {
            info_image.visibility = View.VISIBLE
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).hamburgerShow(false)
    }

    override fun onPause() {
        super.onPause()
        (activity as MainActivity).hamburgerShow(true)
    }
}
