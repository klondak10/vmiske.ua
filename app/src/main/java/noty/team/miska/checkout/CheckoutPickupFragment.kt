package noty.team.miska.checkout


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import io.paperdb.Paper
import kotlinx.android.synthetic.main.dialog_check_order.*
import kotlinx.android.synthetic.main.fragment_checkout_pickup.*
import kotlinx.android.synthetic.main.fragment_checkout_pickup.view.*
import noty.team.miska.MainActivity
import noty.team.miska.PAPER_LANG_DEFAULT
import noty.team.miska.R
import noty.team.miska.login.CHECKOUT_CASE_COURIER_FRAGMENT
import noty.team.miska.login.CHECKOUT_CASE_NOVAPOSHTA_FRAGMENT
import noty.team.miska.login.CHECKOUT_CASE_PICKUP_FRAGMENT
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.model.DELIVERYTYPE
import noty.team.miska.model.DeliveryAdress
import noty.team.miska.model.ItemProductCatalog
import noty.team.miska.model.PAYMENTTYPE
import noty.team.miska.modelRetrofit.AnswerDeliveryType
import noty.team.miska.modelRetrofit.AnswerPayment
import noty.team.miska.paymentLiqpay.enums.PaymentStatuses
import noty.team.miska.paymentLiqpay.ilisteners.ICheckPaymentStatusListener
import noty.team.miska.paymentLiqpay.ilisteners.ICheckoutResultListener
import noty.team.miska.paymentLiqpay.service.ConfigurationService
import noty.team.miska.paymentLiqpay.service.LiqPayService
import noty.team.miska.paymentLiqpay.service.background.params.CheckPaymentStatusParams
import noty.team.miska.paymentLiqpay.service.background.params.CheckoutPaymentParams
import noty.team.miska.paymentLiqpay.vo.PaymentResult
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.util.WebviewFragment
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import noty.team.miska.viewmodel.LANG_RU
import org.json.JSONException
import java.util.*


const val READ_PHONE_STATE_CODE = 2
const val READY_PICKUP = "pickup.pickup"
const val READY_COURIER = "citylink.citylink"
const val READY_NOVA = "NovaPoshta.NovaPoshta"

const val READY_PAYMENT_CASH = "cheque"
const val READY_PAYMENT_LIQPAY = "liqpay"
const val READY_PAYMENT_ONDELIVERY = "cod"


class CheckoutPickupFragment : Fragment(), ICheckoutResultListener, ICheckPaymentStatusListener {

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }
    var token = ""
    //AnswerDeliveryType
    var listDeliveryType: ArrayList<AnswerDeliveryType> = arrayListOf()
    var listPaymentType: ArrayList<AnswerPayment> = arrayListOf()
    //payment
    private var mPaymentResult: PaymentResult? = null
    private var mTimer: Timer? = null
    private var checkPaymentResultTask: CheckPaymentResultTask? =
        null
    private var mAutoRefresh = false
    private var mCounter = 0
    val currency: String = "UAH"
    var amount2pay = "0.10"
    var payErrorFlag = false
    var paySuccessFlag = false
    var payLinqFlag = false


    override fun onSaveInstanceState(outState: Bundle) {
        payLinqFlag = true
    }

    override fun onResume() {
        super.onResume()

        if (payErrorFlag) {
            payErrorFlag = false
            (activity as MainActivity).clearBasket()
            viewModel.basketList.clear()

            showFailedOrder()

        }
        if (paySuccessFlag) {
            (activity as MainActivity).clearBasket()
            paySuccessFlag = false
            viewModel.basketList.clear()

            showSuccessOrder()
        }
        if (payLinqFlag) {
            payLinqFlag = false
            (activity as MainActivity).clearBasket()

            showFailedOrder()
        }
    }

    fun clearfragmentStack() {

        Handler().postDelayed({
            val fm = activity?.supportFragmentManager
            val count = fm?.getBackStackEntryCount() ?: 0
            for (i in 0 until count - 1) {
                fm?.popBackStackImmediate()
            }
        }, 200)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_checkout_pickup, container, false)
    }

    fun isPermissionGranted(context: Context): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity?.checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted")
                return true
            } else {

                Log.v("TAG", "Permission is revoked")
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(Manifest.permission.READ_PHONE_STATE),
                    2
                )
                return false
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted")
            return true
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {

            2 -> {

                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(
                        activity?.applicationContext,
                        getString(R.string.permission_granted),
                        Toast.LENGTH_SHORT
                    ).show()
                    //do ur specific task after read phone state granted
                } else {
                    Toast.makeText(
                        activity?.applicationContext,
                        getString(R.string.permission_notgranted),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request

    }

    fun showSuccessOrder() {
        (activity as MainActivity).replaceFragment(
            CheckoutThankyouFragment(),
            noty.team.miska.CHECKOUT_SUCCESS_FRAGMENT,
            false,
            ""
        )
        (activity as MainActivity).getBasketFromServer(false, ({}))
    }

    fun showFailedOrder() {
        (activity as MainActivity).replaceFragment(
            CheckoutErrorFragment(),
            noty.team.miska.CHECKOUT_ERROR_FRAGMENT,
            false,
            ""
        )
    }


    override fun onPause() {
        super.onPause()
        viewModel.deliveryType = DeliveryAdress()
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_delivery_way)
            , true, false
        )


        checkout_pickup_detail_textview.setOnClickListener {
            //https://vmiske.ua/ambar-uk
            val info = getString(R.string.ambar_info_url)
            (activity as MainActivity).replaceFragment(
                WebviewFragment(info),
                "",
                false,
                getString(R.string.toolbar_name_basket)
            )

        }
        getProfile()

        //delivery_select_group.setch
        var deliveryInfo = viewModel.deliveryType

        delivery_select_group.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                when (checkedId) {
                    R.id.delivery_select_courier -> {
                        deliveryInfo.typeDeliverySelected = DELIVERYTYPE.COURIER
                        delivery_type_caption_name.text = getString(R.string.label_delivery_name)
                        // if (delivery.typeDeliverySelected == DELIVERYTYPE.COURIER) {
                        if (deliveryInfo.dataCourier.cityName == "") {
                            delivery_address_name.text = getString(R.string.select_address)
                        } else {


                            var newAdress = deliveryInfo.dataCourier.regionName + ", " +
                                    deliveryInfo.dataCourier.cityName + ", " +
                                    deliveryInfo.dataCourier.streetName + ", " +
                                    deliveryInfo.dataCourier.houseNumber

                            if (deliveryInfo.dataCourier.apartmentNumber != "")
                                newAdress += ", " + deliveryInfo.dataCourier.apartmentNumber

                            if (deliveryInfo.dataCourier.comment != "")
                                newAdress += ", " + deliveryInfo.dataCourier.comment
                            delivery_address_name.text = newAdress

                        }
                        payment_select_group.payment_select_cash_on_delivery.visibility = View.GONE
                        payment_select_group.payment_select_cash.visibility = View.VISIBLE
                        payment_select_group.check(payment_select_cash.id)
                        payment_select_group.payment_select_cash.isChecked = true

                    }
                    R.id.delivery_select_nova_poshta -> {
                        deliveryInfo.typeDeliverySelected = DELIVERYTYPE.NOVA
                        delivery_type_caption_name.text = getString(R.string.label_department)
                        if (deliveryInfo.dataDepNova.name == "") {
                            delivery_address_name.text = getString(R.string.select_dep)
                        } else
                            delivery_address_name.text = deliveryInfo.dataDepNova.name
                        payment_select_group.payment_select_cash.visibility = View.GONE
                        payment_select_group.payment_select_cash_on_delivery.visibility =
                            View.VISIBLE
                        payment_select_group.check(payment_select_cash_on_delivery.id)
                        payment_select_group.payment_select_cash_on_delivery.isChecked = true

                    }
                    R.id.delivery_select_pickup -> {
                        deliveryInfo.typeDeliverySelected = DELIVERYTYPE.PICKUP
                        delivery_type_caption_name.text = getString(R.string.label_select_shop)
                        if (deliveryInfo.dataPickup.address == "") {
                            delivery_address_name.text = getString(R.string.select_shop)
                        } else
                            delivery_address_name.text = deliveryInfo.dataPickup.address
                        payment_select_group.payment_select_cash_on_delivery.visibility = View.GONE
                        payment_select_group.payment_select_cash.visibility = View.VISIBLE
                        payment_select_group.check(payment_select_cash.id)
                        payment_select_group.payment_select_cash.isChecked = true
                    }
                    else -> {
                        deliveryInfo.typeDeliverySelected = null
                        delivery_type_caption_name.text = ""
                        delivery_address_name.text = ""
                    }
                }
            })




        when (deliveryInfo.typeDeliverySelected) {
            DELIVERYTYPE.PICKUP -> {
                delivery_select_group.check(delivery_select_pickup.id)
                delivery_select_group.delivery_select_pickup.isChecked = true
                delivery_type_caption_name.text = getString(R.string.label_select_shop)
                payment_select_group.payment_select_cash_on_delivery.visibility = View.GONE
                payment_select_group.payment_select_cash.visibility = View.VISIBLE
            }
            DELIVERYTYPE.NOVA -> {
                delivery_select_group.check(delivery_select_nova_poshta.id)
                delivery_select_group.delivery_select_nova_poshta.isChecked = true
                payment_select_group.payment_select_cash.visibility = View.GONE
                payment_select_group.payment_select_cash_on_delivery.visibility = View.VISIBLE

            }
            DELIVERYTYPE.COURIER -> {
                delivery_select_group.check(delivery_select_courier.id)
                delivery_select_group.delivery_select_courier.isChecked = true
                payment_select_group.payment_select_cash_on_delivery.visibility = View.GONE
                payment_select_group.payment_select_cash.visibility = View.VISIBLE

            }
            else -> {
                deliveryInfo.typeDeliverySelected = DELIVERYTYPE.COURIER
                delivery_select_group.check(delivery_select_courier.id)
                delivery_select_group.delivery_select_courier.isChecked = true
                payment_select_group.payment_select_cash_on_delivery.visibility = View.GONE
                payment_select_group.payment_select_cash.visibility = View.VISIBLE

            }
        }
//-----------------------------------------------------
//        payment_select_group

        var payment = viewModel.deliveryType.paymentType

        payment_select_group.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                when (checkedId) {
                    R.id.payment_select_cash -> {
                        deliveryInfo.paymentTypeSelected = PAYMENTTYPE.CASH
                    }
                    R.id.payment_select_cash_on_delivery -> {
                        deliveryInfo.paymentTypeSelected = PAYMENTTYPE.ONDELIVERY
                    }
                    R.id.payment_select_liqpay -> {
                        deliveryInfo.paymentTypeSelected = PAYMENTTYPE.LIQPAY
                    }
                    else -> {
                        deliveryInfo.paymentTypeSelected = PAYMENTTYPE.CASH
                    }
                }


            })



        when (deliveryInfo.paymentTypeSelected) {
            PAYMENTTYPE.CASH -> {
                payment_select_group.check(payment_select_cash.id)
            }
            PAYMENTTYPE.ONDELIVERY -> {
                payment_select_group.check(payment_select_cash_on_delivery.id)
            }
            PAYMENTTYPE.LIQPAY -> {
                payment_select_group.check(payment_select_liqpay.id)
            }

            else -> {
                payment_select_group.check(payment_select_cash.id)
                deliveryInfo.paymentTypeSelected = PAYMENTTYPE.CASH
            }
        }


//-----------------------------------------------------
        delivery_select_address.setOnClickListener {
            checkType()
        }


        checkout_pickup_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
        }

        if (token != "") {
            //get delivery types
            viewModel.apiCheckoutDeliveryTypesList(token)
                .observe(this, androidx.lifecycle.Observer { response ->
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data?.error == false) {
                            listDeliveryType = data.answer ?: arrayListOf()
                            deliveryInfo.deliveryType = data.answer ?: arrayListOf()
                        } else {
                            context?.toast(data?.message.toString())
                        }
                    } else
                        context?.toast(response?.message().toString())
                })
            var paperLang = Paper.book().read(PAPER_LANG_DEFAULT, LANG_RU)
            //get payment types
            viewModel.apiCheckoutPaymentTypesList(token, paperLang)
                .observe(this, androidx.lifecycle.Observer { response ->
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data?.error == false) {
                            listPaymentType = data.answer ?: arrayListOf()
                            for (item in listPaymentType) {
                                if (item.type == "cheque")
                                    payment_select_cash.text = item.name
                                else
                                    if (item.type == "cod")
                                        payment_select_cash_on_delivery.text = item.name
                                    else
                                        if (item.type == "liqpay")
                                            payment_select_liqpay.text = item.name
                            }

                            deliveryInfo.paymentType = data.answer ?: arrayListOf()
                        } else {
                            context?.toast(data?.message.toString())
                        }
                    } else
                        context?.toast(response?.message().toString())
                })
        }

        checkout_pickup_next_button.setOnClickListener {
            if (!isPermissionGranted(context!!)) {
                if (!isPermissionGranted(context!!)) {
                    context?.toast(getString(R.string.access_to_phone))
                    return@setOnClickListener
                }
            }
            val deliveryReady = deliveryInfo.typeDeliverySelected
            when (deliveryReady) {
                DELIVERYTYPE.PICKUP -> {
                    if (deliveryInfo.dataPickup.address == "") {
                        context?.toast(getString(R.string.select_shop_))
                        return@setOnClickListener
                    }
                }
                DELIVERYTYPE.COURIER -> {
                    if (deliveryInfo.dataCourier.cityName == "") {
                        context?.toast(getString(R.string.select_delivery_address))
                        return@setOnClickListener
                    }
                }

                DELIVERYTYPE.NOVA -> {
                    if (deliveryInfo.dataDepNova.name == "") {
                        context?.toast(getString(R.string.select_nova_dep_))
                        return@setOnClickListener
                    }
                }
            }
            showDialog(activity!!)
        }
        if (!isPermissionGranted(context!!)) {
            isPermissionGranted(context!!)
        }
    }

    fun showDialog(activity: Activity) {
        val dialog = Dialog(activity)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_check_order)
        val deliveryInfo = viewModel.deliveryType

        when (deliveryInfo.paymentTypeSelected) {
            PAYMENTTYPE.CASH -> {
                dialog.tvTypePay.findViewById<TextView>(R.id.tvTypePay).text =
                    getString(R.string.label_payment) + " " + getString(R.string.label_cash)
            }
            PAYMENTTYPE.ONDELIVERY -> {
                dialog.tvTypePay.findViewById<TextView>(R.id.tvTypePay).text =
                    getString(R.string.label_payment) + " " + getString(R.string.pay_ondelivery)
            }
            PAYMENTTYPE.LIQPAY -> {
                dialog.tvTypePay.findViewById<TextView>(R.id.tvTypePay).text =
                    getString(R.string.label_payment) + " " + getString(R.string.label_liqpay)
            }

            else -> {
                dialog.tvTypePay.findViewById<TextView>(R.id.tvTypePay).text =
                    getString(R.string.label_payment) + " " + getString(R.string.label_cash)
            }
        }

        val deliveryReady = deliveryInfo.typeDeliverySelected
        when (deliveryReady) {
            DELIVERYTYPE.PICKUP -> {
                dialog.clNovPost.findViewById<View>(R.id.clNovPost).visibility = View.GONE

                val address =
                    deliveryInfo.dataPickup.address
                dialog.tvTypeDelivery.findViewById<TextView>(R.id.tvTypeDelivery).text =
                    getString(R.string.label_pickup)
//                context?.toast(address!!)
                dialog.tvCity.findViewById<TextView>(R.id.tvCity).visibility = View.GONE
                dialog.tvAddress.findViewById<TextView>(R.id.tvAddress).text = address
                dialog.tvFlat.findViewById<TextView>(R.id.tvFlat).visibility = View.GONE
            }
            DELIVERYTYPE.COURIER -> {
                dialog.clNovPost.findViewById<View>(R.id.clNovPost).visibility = View.GONE

                dialog.tvTypeDelivery.findViewById<TextView>(R.id.tvTypeDelivery).text =
                    getString(R.string.label_courier)

                dialog.tvCity.findViewById<TextView>(R.id.tvCity).text =
                    getString(R.string.short_city) + deliveryInfo.dataCourier.cityName
                dialog.tvAddress.findViewById<TextView>(R.id.tvAddress).text =
                    deliveryInfo.dataCourier.streetName + " " + deliveryInfo.dataCourier.houseNumber

                if (deliveryInfo.dataCourier.apartmentNumber.isNotEmpty()) {
                    dialog.tvFlat.findViewById<TextView>(R.id.tvFlat).visibility =
                        View.VISIBLE
                    dialog.tvFlat.findViewById<TextView>(R.id.tvFlat).text =
                        getString(R.string.label_flat) + " " + deliveryInfo.dataCourier.apartmentNumber
                } else {
                    dialog.tvFlat.findViewById<TextView>(R.id.tvFlat).visibility = View.GONE
                }
                if (deliveryInfo.dataCourier.comment.isNotEmpty()) {
                    dialog.tvDescription.findViewById<TextView>(R.id.tvDescription).visibility =
                        View.VISIBLE
                    dialog.tvDescription.findViewById<TextView>(R.id.tvDescription).text =
                        deliveryInfo.dataCourier.comment
                } else {
                    dialog.tvDescription.findViewById<TextView>(R.id.tvDescription).visibility =
                        View.GONE
                }
                val address = deliveryInfo.dataCourier.cityName + " " +
                        deliveryInfo.dataCourier.streetName + " " + deliveryInfo.dataCourier.houseNumber +
                        " " + deliveryInfo.dataCourier.apartmentNumber


            }
            DELIVERYTYPE.NOVA -> {

                dialog.tvTypeDelivery.findViewById<TextView>(R.id.tvTypeDelivery).text =
                    getString(R.string.label_nova_poshta)
                dialog.tvCity.findViewById<TextView>(R.id.tvCity).visibility = View.GONE
                dialog.tvAddress.findViewById<TextView>(R.id.tvAddress).text =
                    deliveryInfo.dataDepNova.name
                dialog.tvFlat.findViewById<TextView>(R.id.tvFlat).visibility = View.GONE


                dialog.clNovPost.findViewById<View>(R.id.clNovPost).visibility = View.VISIBLE

                val sumToPay = viewModel.sumPromoBonus

                if (sumToPay < 500) {
                    dialog.tvSumDigit.findViewById<TextView>(R.id.tvSumDigit).text = "$sumToPay грн"
                    val total = sumToPay + 55
                    dialog.tvNovPostDescription.findViewById<TextView>(R.id.tvNovPostDescription)
                        .text = "55 грн"
                    dialog.tvTotalDigit.findViewById<TextView>(R.id.tvTotalDigit).text =
                        "$total грн"
                } else {
                    dialog.tvSumDigit.findViewById<TextView>(R.id.tvSumDigit).text = "$sumToPay грн"
                    dialog.tvTotalDigit.findViewById<TextView>(R.id.tvTotalDigit).text =
                        "$sumToPay грн"

                    when (deliveryInfo.paymentTypeSelected) {
                        PAYMENTTYPE.ONDELIVERY -> {
                            dialog.tvNovPostDescription.findViewById<TextView>(R.id.tvNovPostDescription)
                                .text = getString(R.string.nov_post_description)
                        }
                        PAYMENTTYPE.LIQPAY -> {
                            dialog.tvNovPostDescription.findViewById<TextView>(R.id.tvNovPostDescription)
                                .text = getString(R.string.delivery_free)
                        }
                    }
                }
            }
        }

        dialog.show()

        val window = dialog.window
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.setLayout(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        window?.setGravity(Gravity.CENTER)

        val btnNext =
            dialog.btnNext.findViewById<Button>(R.id.btnNext)

        btnNext.setOnClickListener {


            val bonuses = viewModel.bonusesUsed.toString()

            if (token == "")
                return@setOnClickListener
            var ambar = ""
            if (checkout_pickup_ambar_checkbox.isChecked)
                ambar = "1" else ambar = "0"

            var paymentReady = ""
            when (deliveryInfo.paymentTypeSelected) {
                PAYMENTTYPE.CASH -> {
                    paymentReady = READY_PAYMENT_CASH
                }
                PAYMENTTYPE.ONDELIVERY -> {
                    paymentReady = READY_PAYMENT_ONDELIVERY
                }
                PAYMENTTYPE.LIQPAY -> {
                    paymentReady = READY_PAYMENT_LIQPAY
                }

                else -> {
                    paymentReady = READY_PAYMENT_CASH
                }
            }

            var vaucher: String? = null
            if (viewModel.promoCode != "") {
                vaucher = viewModel.promoCode
            }

            val deliveryReady = deliveryInfo.typeDeliverySelected
            when (deliveryReady) {
                DELIVERYTYPE.PICKUP -> {
                    if (deliveryInfo.dataPickup.address == "") {
                        context?.toast(getString(R.string.select_shop_))
                        return@setOnClickListener
                    }

                    var address =
                        deliveryInfo.dataPickup.address

                    viewModel.apiOrderMake(
                        token, ambar, address, null, null, null,
                        paymentReady, READY_PICKUP, vaucher, bonuses
                    ).observe(this, androidx.lifecycle.Observer { response ->
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data?.error == false) {
                                if (data.answer?.status == true) {
                                    if (paymentReady == READY_PAYMENT_LIQPAY) {
                                        val sumToPay = viewModel.sumPromoBonus
                                        if (sumToPay == 0) {
                                            context?.toast(getString(R.string.sum_greater_zero))
                                        } else {
                                            val desciption = getString(R.string.order_number) +
                                                    data.answer?.data?.orderId.toString() + " " + getString(
                                                R.string.internert_shop
                                            ) + " " +
                                                    address + " " + profileData
                                            makePaymentLinq(sumToPay.toString(), desciption)
                                        }
                                    } else showSuccessOrder()
                                }

                            } else {
                                context?.toast(data?.message.toString())
                            }
                        } else
                            context?.toast(response?.message().toString())
                    })
                }
                DELIVERYTYPE.COURIER -> {
                    if (deliveryInfo.dataCourier.cityName == "") {
                        context?.toast(getString(R.string.select_delivery_address))
                        return@setOnClickListener
                    }

                    val address = deliveryInfo.dataCourier.cityName + " " +
                            deliveryInfo.dataCourier.streetName + " " + deliveryInfo.dataCourier.houseNumber +
                            " " + deliveryInfo.dataCourier.apartmentNumber

                    viewModel.apiOrderMake(
                        token, ambar, address, null, null, null,
                        paymentReady, READY_COURIER, vaucher, bonuses
                    ).observe(this, androidx.lifecycle.Observer { response ->
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data?.error == false) {
                                if (data.answer?.status == true) {

                                    if (paymentReady == READY_PAYMENT_LIQPAY) {
                                        //val sum = viewModel.basketList
                                        //val sumToPay = sumList(sum)
                                        val sumToPay = viewModel.sumPromoBonus
                                        if (sumToPay == 0) {
                                            context?.toast(getString(R.string.sum_greater_zero))
                                        } else {
                                            val desciption = getString(R.string.order_number) +
                                                    data.answer?.data?.orderId.toString() + " " + getString(
                                                R.string.internert_shop
                                            ) + " " +
                                                    address + " " + profileData
                                            makePaymentLinq(sumToPay.toString(), desciption)
                                        }
                                    } else showSuccessOrder()
                                }
                            } else {
                                context?.toast(data?.message.toString())
                            }
                        } else
                            context?.toast(response?.message().toString())
                    })
                }

                DELIVERYTYPE.NOVA -> {
                    if (deliveryInfo.dataDepNova.name == "") {
                        context?.toast(getString(R.string.select_nova_dep_))
                        return@setOnClickListener
                    }

                    viewModel.apiOrderMake(
                        token,
                        ambar,
                        deliveryInfo.dataDepNova.name,
                        deliveryInfo.dataCityNova.cityId,
                        deliveryInfo.dataZoneNova.zoneId,
                        null,
                        paymentReady,
                        READY_NOVA,
                        vaucher, bonuses
                    ).observe(this, androidx.lifecycle.Observer { response ->
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data?.error == false) {
                                if (data.answer?.status == true) {

                                    if (paymentReady == READY_PAYMENT_LIQPAY) {
                                        //   val sum = viewModel.basketList
                                        //val sumToPay = sumList(sum)
                                        var sumToPay = viewModel.sumPromoBonus
                                        if (sumToPay == 0) {
                                            context?.toast(getString(R.string.sum_greater_zero))
                                        } else {
                                            if (sumToPay < 500) {
                                                sumToPay += 55
                                            }
                                            val desciption = getString(R.string.order_number) +
                                                    data.answer?.data?.orderId.toString() + " " + getString(
                                                R.string.internert_shop
                                            ) + " " +
                                                    deliveryInfo.dataCityNova.name + " " + deliveryInfo.dataDepNova.name + " " + profileData

                                            makePaymentLinq(sumToPay.toString(), desciption)
                                        }
                                    } else
                                        showSuccessOrder()
                                }
                            } else {
                                context?.toast(data?.message.toString())
                            }
                        } else
                            context?.toast(response?.message().toString())
                    })
                }
            }
            dialog.dismiss()
        }
    }

    fun sumList(list: MutableList<ItemProductCatalog>): Int {
        var startSum = 0
        for (itemPos in 0 until list.size) {
            var sum = 0
            if (list[itemPos].basketItemInfo != null) {
                var optPrice = list[itemPos].basketItemInfo?.optionPrice ?: 0
                sum = optPrice * list[itemPos].quantity
            } else {
                sum = list[itemPos].price.toInt() * list[itemPos].quantity
            }
            startSum += sum
        }
        return startSum
    }


    fun makePaymentLinq(amount2pay: String, description: String) {
        //amount2pay: String = "0.1"
        if (isPermissionGranted(context!!)) {
            LiqPayService.getInstance()
                .checkout(activity, amount2pay, currency, this, description)
        } else {
            context?.toast(getString(R.string.access_to_phone))
        }
    }


    fun checkType() {
        if (delivery_select_group.delivery_select_courier.isChecked) {

            (activity as MainActivity).replaceFragment(
                CheckoutCaseCourierAddressFragment(),
                CHECKOUT_CASE_COURIER_FRAGMENT, false, ""
            )

        } else if (delivery_select_group.delivery_select_nova_poshta.isChecked) {
            (activity as MainActivity).replaceFragment(
                CheckoutCaseNovaPoshtaFragment(),
                CHECKOUT_CASE_NOVAPOSHTA_FRAGMENT, false, ""
            )


        } else if (delivery_select_group.delivery_select_pickup.isChecked) {

            (activity as MainActivity).replaceFragment(
                CheckoutCasePickupShopFragment(),
                CHECKOUT_CASE_PICKUP_FRAGMENT, false, ""
            )

        }
    }
    //----------------------------------------------------------------------

    override fun CheckPaymentStatusResult(params: CheckPaymentStatusParams?) {
        try {
            mPaymentResult?.updateResult(params)
        } catch (e: JSONException) {
            enableAutoRefreshPaymentStatus(false)
        }
        activity?.runOnUiThread(Runnable {
            mCounter++
            updateResultStats()
            if (mAutoRefresh) {
                refreshPaymentStatus()
            }
        })
    }


    override fun checkoutResult(params: CheckoutPaymentParams?) {
//11
        try {
            mPaymentResult = PaymentResult(params!!.operationResult)
            updateResultStats()

            if (mPaymentResult?.getErrorCode() == null &&
                mPaymentResult?.getPaymentStatus() !== PaymentStatuses.success && mPaymentResult?.getTransactionId() != null
            ) {
                //Start timer that will check payment result
                enableAutoRefreshPaymentStatus(true)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun updateResultStats() {
        //important
        if (mPaymentResult!!.paymentStatus == PaymentStatuses.success
            || mPaymentResult!!.errorCode != null && !mPaymentResult!!.errorCode.isEmpty()
        ) {
            //success
            val orderId = mPaymentResult?.orderId
            if (orderId != null)
                viewModel.apiConfirmPayment(token, orderId)
                    .observe(this, androidx.lifecycle.Observer { response ->
                        if (response.isSuccessful) {
                            paySuccessFlag = true
                        } else {
                        }

                    })

            enableAutoRefreshPaymentStatus(false)
        } else {
            enableAutoRefreshPaymentStatus(false)
            payErrorFlag = true
        }
    }

    private fun refreshPaymentStatus() {
        if (!mAutoRefresh) return
        mTimer = Timer()
        checkPaymentResultTask = CheckPaymentResultTask()
        mTimer?.schedule(
            checkPaymentResultTask,
            ConfigurationService.getInstance().retryInterval
        )
    }

    private fun enableAutoRefreshPaymentStatus(enable: Boolean) {
        mAutoRefresh = enable
        refreshPaymentStatus()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            2 -> {
                if (resultCode == PackageManager.PERMISSION_GRANTED) {
                    context?.toast("Permission granted")
                    //do ur specific task after read phone state granted

                } else {
                    context?.toast("Permission")
                }
                return
            }
        }
    }

    var profileData = ""


    fun getProfile() {

        var token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
            viewModel.apiGetProfile(
                token
            ).observe(this, Observer { response ->
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {
                        profileData = ""

                        if (data.answer?.firstname != null)
                            profileData += data.answer?.firstname + ", "

                        if (data.answer?.lastname != null)
                            profileData += data.answer?.lastname + ", "

                        if (data.answer?.telephone != null)
                            profileData += data.answer?.telephone + ", "

                        if (data.answer?.email != null)
                            profileData += data.answer?.email


                    } else
                        context?.toast(data?.message.toString())
                } else
                    context?.toast(response?.message().toString())
            })
        }
    }

    inner class CheckPaymentResultTask :
        TimerTask() {
        override fun run() {

            mPaymentResult?.let { result ->
                LiqPayService.getInstance()
                    .checkPayment(activity, result, this@CheckoutPickupFragment)
            }

        }
    }
}




