package noty.team.miska.checkout

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import noty.team.miska.R
import noty.team.miska.login.LoginFragment
import noty.team.miska.util.InfoNoInternetFragment


private val TAB_TITLES_CHECKOUT = arrayOf(
    R.string.checkout_tab_text_1,
    R.string.checkout_tab_text_2
)

class CheckoutPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {

        var fragmnet: Fragment = InfoNoInternetFragment()

        when (position) {
            0 -> fragmnet = CheckoutNewCustomerFragment()
            1 -> fragmnet = LoginFragment(true)
        }

        return fragmnet
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES_CHECKOUT[position])
    }

    override fun getCount(): Int {
        return 2
    }
}
