package noty.team.miska.checkout


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_check_out_pager.*
import noty.team.miska.MainActivity
import noty.team.miska.R


class CheckoutPagerFragment(checkoutTypeNewUser: Boolean) : Fragment() {

    var checkoutTypeNew: Boolean = true

    init {
        checkoutTypeNew = checkoutTypeNewUser
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_check_out_pager, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_authorization),
            false, true
        )


        checkout_view_pager.adapter = CheckoutPagerAdapter(context!!, childFragmentManager)

        checkout_tabs.setupWithViewPager(checkout_view_pager)


        if (checkoutTypeNew) {
            val tab = checkout_tabs.getTabAt(0)
            tab?.select()
        } else {
            val tab = checkout_tabs.getTabAt(1)
            tab?.select()
        }
    }

    override fun onPause() {
        super.onPause()
        (activity as MainActivity).showCustomAppBar("", false, true)
    }


}
