package noty.team.miska.checkout


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_catalog_main.*
import kotlinx.android.synthetic.main.fragment_checkout_case_courier.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.model.AnswerCourier
import noty.team.miska.model.DELIVERYTYPE
import noty.team.miska.model.DeliveryAdress
import noty.team.miska.modelRetrofit.AnswerCity
import noty.team.miska.modelRetrofit.AnswerZone
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel


class CheckoutCaseCourierAddressFragment : Fragment() {
    var deliveryType: DeliveryAdress = DeliveryAdress()


    var token = ""
    var listZoneSpinner: ArrayList<AnswerZone> = arrayListOf()
    var shortListZoneSpinner: ArrayList<AnswerZone> = arrayListOf()
    var adapterZone: ArrayAdapter<AnswerZone>? = null

    var listCitySpinner: ArrayList<AnswerCity> = arrayListOf()
    var shortListCitySpinner: ArrayList<AnswerCity> = arrayListOf()

    var adapterCity: ArrayAdapter<AnswerCity>? = null

    var dataCourier: AnswerCourier = AnswerCourier()

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_checkout_case_courier, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_delivery_address),
            true,
            false
        )

        checkout_address_courier_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        //-------------
        listZoneSpinner.clear()
        listCitySpinner.clear()
        shortListCitySpinner.clear()
        deliveryType = DeliveryAdress()

        token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
        }

        if (token != "") {
            //get zone
            viewModel.apiCheckoutZoneList(token)
                .observe(this, androidx.lifecycle.Observer { response ->
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data?.error == false) {
                            listZoneSpinner = data.answer ?: arrayListOf()

                            for (item in listZoneSpinner) {
                                if (item.zoneId == "3490" || item.zoneId == "3487")
                                    shortListZoneSpinner.add(item)
                            }
                            var firstItem = AnswerZone()
                            firstItem.name = getString(R.string.input_region_name)
                            shortListZoneSpinner.add(0, firstItem)
                            adapterZone = ArrayAdapter<AnswerZone>(
                                context!!,
                                android.R.layout.simple_spinner_item,
                                shortListZoneSpinner.toList()
                            )
                            adapterZone?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            courier_region_spinner.adapter = adapterZone

                            courier_region_spinner.onItemSelectedListener = object :
                                AdapterView.OnItemSelectedListener {

                                override fun onNothingSelected(parent: AdapterView<*>?) {
                                }

                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {
                                    val item = parent?.selectedItem as AnswerZone
                                    deliveryType.dataZoneCourier = item
                                    selectCity()
                                }
                            }
                        } else {
                            context?.toast(data?.message.toString())
                        }
                    } else
                        context?.toast(response?.message().toString())
                })
        }


        //-------------
        deliveryType = DeliveryAdress()
        checkout_save_courier_button.setOnClickListener {

            dataCourier.regionName = deliveryType.dataZoneCourier.name ?: ""
            dataCourier.cityName = deliveryType.dataCityCourier.name ?: ""
            dataCourier.streetName = checkout_street_name_data.text.toString()
            dataCourier.houseNumber = checkout_house_number_data.text.toString()
            dataCourier.apartmentNumber = checkout_apartment_number_data.text.toString()
            dataCourier.comment = checkout_comment_data.text.toString()


            if (dataCourier.regionName == shortListZoneSpinner[0].name) {
                context?.toast(getString(R.string.input_region_name))
                return@setOnClickListener
            }

            if (dataCourier.cityName == shortListCitySpinner[0].name) {
                context?.toast(getString(R.string.select_city))
                return@setOnClickListener
            }

            if (checkout_street_name_data.text.toString().trim().isEmpty()) {
                context?.toast(getString(R.string.input_street_name))
                return@setOnClickListener
            }
            if (checkout_house_number_data.text.toString().trim().isEmpty()) {
                context?.toast(getString(R.string.input_house_number_name))
                return@setOnClickListener
            }


            deliveryType.typeDeliverySelected = DELIVERYTYPE.COURIER
            deliveryType.dataCourier = dataCourier
            viewModel.deliveryType = deliveryType

            (activity as MainActivity).onBackPressed()
        }
    }


    fun selectCity() {
        viewModel.apiCheckoutCityList(token, deliveryType.dataZoneCourier.area ?: "")
            .observe(this, androidx.lifecycle.Observer { response ->
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {
                        shortListCitySpinner.clear()

                        listCitySpinner = data.answer ?: arrayListOf()

                        for(city in listCitySpinner){
                            if(city.cityId=="3662"||city.cityId=="4263")
                            shortListCitySpinner.add(city)
                        }
                        var firstItem = AnswerCity()
                        firstItem.name = getString(R.string.select_city)
                        shortListCitySpinner.add(0, firstItem)
                        adapterCity = ArrayAdapter<AnswerCity>(
                            context!!,
                            android.R.layout.simple_spinner_item,
                            shortListCitySpinner.toList()
                        )
                        adapterCity?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        courier_city_spinner.adapter = adapterCity
                        courier_city_spinner.onItemSelectedListener = object :
                            AdapterView.OnItemSelectedListener {
                            override fun onNothingSelected(parent: AdapterView<*>?) {
                            }

                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View?,
                                position: Int,
                                id: Long
                            ) {
                                val item = parent?.selectedItem as AnswerCity
                                deliveryType.dataCityCourier = item
                                //selectDepartment()
                            }
                        }
                    } else {
                        context?.toast(data?.message.toString())
                    }
                } else
                    context?.toast(response?.message().toString())
            })
    }

}
