package noty.team.miska.checkout


import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.github.vacxe.phonemask.PhoneMaskManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_checkout_new_customer.*
import noty.team.miska.MainActivity
import noty.team.miska.PAPER_USER_LOGGED
import noty.team.miska.PAPER_USER_NAME
import noty.team.miska.R
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.login.RC_SIGN_IN
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import org.json.JSONObject
import java.util.regex.Pattern


class CheckoutNewCustomerFragment : Fragment() {

    var fbId: String? = null
    var ggId: String? = null
    var userAccount: GoogleSignInAccount? = null
    var callbackManager: CallbackManager? = null
    var account: GoogleSignInAccount? = null


    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_checkout_new_customer, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        checkout_customer_name.text.clear()
        checkout_customer_surname.text.clear()
        checkout_customer_email.text.clear()
        checkout_customer_phone.text.clear()

        callbackManager = CallbackManager.Factory.create()

        checkout_FacebookRegisterButton.setReadPermissions("public_profile")
        checkout_FacebookRegisterButton.setReadPermissions("email")
        // If using in a fragment
        checkout_FacebookRegisterButton.fragment = this

        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    //   context?.toast("Facebook логин успешен!")
                    loginFacebookUser()
                }

                override fun onCancel() {
                    context?.toast(getString(R.string.message_facebook_cancel))
                }

                override fun onError(error: FacebookException?) {
                    context?.toast(getString(R.string.message_facebook_cancel))
                }
            }
        )
        checkout_customer_facebook_button.setOnClickListener {
            registerFacebookUser()
            true
        }
        checkout_customer_google_button.setOnClickListener {
            registerGoogleUser()
            true
        }

        checkout_customer_next_facebook_google_button.setOnClickListener {
            registerNewUser()
            true
        }

        //checkout_customer_phone
        PhoneMaskManager()
            .withMask(getString(R.string.phone_mask))
            .withRegion(getString(R.string.phone_prefix))
            .withValueListener { phone -> System.out.println(phone) }
            .bindTo((view.findViewById(R.id.checkout_customer_phone) as EditText))

    }

    fun startSuccess() {
        (activity as MainActivity).showBasket(true)
    }


    fun loginFacebookUser() {
        val request: GraphRequest =
            GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                object : GraphRequest.GraphJSONObjectCallback {
                    override fun onCompleted(fbAccount: JSONObject?, response: GraphResponse?) {
                        try {
                            checkout_customer_name.setText(fbAccount?.getString("first_name"))
                            checkout_customer_surname.setText(fbAccount?.getString("last_name"))
                            checkout_customer_email.setText(fbAccount?.getString("email"))
                            fbId = fbAccount?.getString("id")
                        } catch (e: Exception) {
                            context?.toast(e.localizedMessage.toString())
                        }
                    }
                })

        val parameters = Bundle()
        parameters.putString("fields", "id,name,email,gender, birthday,first_name,last_name")
        request.parameters = parameters
        request.executeAsync()
    }

    fun registerGoogleUser() {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        val mGoogleSignInClient = GoogleSignIn.getClient(context!!, gso)
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data)
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                account = task.getResult(ApiException::class.java)
                //(activity as MainActivity).setGoogleAccount(account)
                //updateUI(accountGoogle)
                (activity as MainActivity).setGoogleAccount(account)
                userAccount = account
                fbId = null
                ggId = null
                userAccount?.let {
                    checkout_customer_name.setText(it.givenName)
                    checkout_customer_surname.setText(it.familyName)
                    checkout_customer_email.setText(it.email)
                    ggId = it.id
                }
            } catch (e: ApiException) {
                // The ApiException status code indicates the detailed failure reason.
                // Please refer to the GoogleSignInStatusCodes class reference for more information.
                Log.w(ContentValues.TAG, "signInResult:failed code=" + e.statusCode)
                //updateUI(null)
                context?.toast(e.localizedMessage.toString())
            }
        }
    }

    fun registerFacebookUser() {
        if (AccessToken.getCurrentAccessToken() != null) {
            loginFacebookUser()
        } else {
            checkout_FacebookRegisterButton.performClick()
        }
    }


    fun registerNewUser() {
        if (checkout_customer_name.text.toString().trim().isEmpty()) {
            context?.toast(getString(R.string.message_input_name))
            return
        }
        if (checkout_customer_surname.text.toString().trim().isEmpty()) {
            context?.toast(getString(R.string.message_input_surname))
            return
        }
        if (checkout_customer_email.text.toString().trim().isEmpty()) {
            context?.toast(getString(R.string.message_input_email))
            return
        }

        val pattern1 =
            Pattern.compile("^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+")

        val matcher1 = pattern1.matcher(checkout_customer_email.text)

        if (!matcher1.matches()) {
            context?.toast(getString(R.string.message_input_correct_email))
            return
        }

        if (checkout_customer_phone.text.toString().trim().isEmpty()) {
            context?.toast(getString(R.string.input_phone))
            return
        }

        if (checkout_customer_phone.text.toString().length < 19 &&
            checkout_customer_phone.text.toString().length > 5
        ) {
            context?.toast(getString(R.string.message_input_10_numbers))
            return
        }
        if (checkout_customer_password.text?.toString()?.trim()?.isEmpty() ?: false) {
            context?.toast(getString(R.string.message_input_password))
            return
        }
        if (checkout_customer_password.text?.toString()?.length ?: 0 < 4) {
            context?.toast(getString(R.string.message_input_password_length))
            return
        }

        val patternPassword =
            Pattern.compile("[A-ZА-Яа-яa-z0-9_-Ёё]{4,20}\$")

        val matcherPassword = patternPassword.matcher(checkout_customer_password.text.toString())

        if (!matcherPassword.matches()) {
            context?.toast(getString(R.string.message_password_only_number_letters))
            return
        }

        viewModel.apiRegisterUser(
            checkout_customer_name.text.toString(),
            checkout_customer_surname.text.toString(),
            checkout_customer_email.text.toString(),
            checkout_customer_phone.text.toString(),
            checkout_customer_password.text.toString(),

            null, fbId, ggId
        ).observe(this, Observer { response ->

            if (response.isSuccessful) {
                val data = response.body()
                if (data?.error == false) {
                    val token = data.answer?.token ?: ""
                    Paper.book().write(PAPER_TOKEN, token)
                    Paper.book().write(PAPER_USER_LOGGED, true)
                    Paper.book().write(PAPER_USER_NAME, checkout_customer_name.text.toString())
                    (activity as MainActivity).showUserMenu(
                        true,
                        checkout_customer_name.text.toString()
                    )
                    startSuccess()
                } else
                    context?.toast(data?.message.toString())
            } else
                context?.toast(response.message().toString())
        })
    }

}
