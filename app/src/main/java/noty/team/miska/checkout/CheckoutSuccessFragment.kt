package noty.team.miska.checkout


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_checkout_success.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.login.CHECKOUT_PICKUP_FRAGMENT
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel

class CheckoutSuccessFragment : Fragment() {

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_checkout_success, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_contact_data),
            true,
            false
        )

        checkout_next_button.setOnClickListener {
            (activity as MainActivity).replaceFragment(
                CheckoutPickupFragment(),
                CHECKOUT_PICKUP_FRAGMENT, false, ""
            )
        }
        checkout_contact_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        getProfileData()
    }

    fun getProfileData() {
        var token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
            viewModel.apiGetProfile(
                token
            ).observe(this, Observer { response ->
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {

                        if (data.answer?.firstname != null)
                            checkout_name.setText(data.answer?.firstname)
                        else
                            checkout_name.setText("")

                        if (data.answer?.lastname != null)
                            checkout_surname.setText(data.answer?.lastname)
                        else
                            checkout_surname.setText("")

                        if (data.answer?.email != null)
                            checkout_email.setText(data.answer?.email)
                        else
                            checkout_email.setText("")

                        if (data.answer?.telephone != null)
                            checkout_phone.setText(data.answer?.telephone)
                        else
                            checkout_phone.setText("")


                    } else
                        context?.toast(data?.message.toString())
                } else
                    context?.toast(response?.message().toString())
            })
        }
    }

}
