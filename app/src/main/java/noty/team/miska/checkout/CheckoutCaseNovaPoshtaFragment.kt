package noty.team.miska.checkout


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_checkout_case_courier.checkout_address_courier_arrow_back
import kotlinx.android.synthetic.main.fragment_checkout_case_nova.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.model.DELIVERYTYPE
import noty.team.miska.model.DeliveryAdress
import noty.team.miska.modelRetrofit.AnswerCity
import noty.team.miska.modelRetrofit.AnswerDep
import noty.team.miska.modelRetrofit.AnswerZone
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel


class CheckoutCaseNovaPoshtaFragment : Fragment() {

    var deliveryType: DeliveryAdress = DeliveryAdress()

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_checkout_case_nova, container, false)
    }

    var token = ""
    var listZoneSpinner: ArrayList<AnswerZone> = arrayListOf()
    var adapterZone: ArrayAdapter<AnswerZone>? = null

    var listCitySpinner: ArrayList<AnswerCity> = arrayListOf()
    var adapterCity: ArrayAdapter<AnswerCity>? = null

    var listDepSpinner: ArrayList<AnswerDep> = arrayListOf()
    var adapterDep: ArrayAdapter<AnswerDep>? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.name_nova_department),
            true,
            false
        )

        checkout_address_courier_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        listZoneSpinner.clear()
        listCitySpinner.clear()
        listDepSpinner.clear()
        deliveryType = DeliveryAdress()

        token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
        }

        if (token != "") {
            //get zone
            viewModel.apiCheckoutZoneList(token)
                .observe(this, androidx.lifecycle.Observer { response ->
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data?.error == false) {
                            listZoneSpinner = data.answer ?: arrayListOf()

                            var firstItem = AnswerZone()
                            firstItem.name = getString(R.string.input_region_name)
                            listZoneSpinner.add(0, firstItem)
                            adapterZone = ArrayAdapter<AnswerZone>(
                                context!!,
                                android.R.layout.simple_spinner_item,
                                listZoneSpinner.toList()
                            )
                            adapterZone?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            nova_region_spinner.adapter = adapterZone

                            nova_region_spinner.onItemSelectedListener = object :
                                AdapterView.OnItemSelectedListener {

                                override fun onNothingSelected(parent: AdapterView<*>?) {
                                }

                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {
                                    val item = parent?.selectedItem as AnswerZone
                                    deliveryType.dataZoneNova = item
                                    selectCity()
                                }
                            }
                        } else {
                            context?.toast(data?.message.toString())
                        }
                    } else
                        context?.toast(response?.message().toString())
                })
        }
        deliveryType.typeDeliverySelected = null


        checkout_save_nova_button.setOnClickListener {
            if (deliveryType.dataZoneNova.name == listZoneSpinner[0].name) {
                context?.toast(getString(R.string.input_region_name))
                return@setOnClickListener
            }
            if (deliveryType.dataCityNova.name == listCitySpinner[0].name) {
                context?.toast(getString(R.string.select_city))
                return@setOnClickListener
            }
            if (deliveryType.dataDepNova.name == listDepSpinner[0].name) {
                context?.toast(getString(R.string.select_dep))
                return@setOnClickListener
            }


            deliveryType.typeDeliverySelected = DELIVERYTYPE.NOVA
            viewModel.deliveryType = deliveryType
            (activity as MainActivity).onBackPressed()
        }
    }

    fun selectCity() {
        viewModel.apiCheckoutCityList(token, deliveryType.dataZoneNova.area ?: "")
            .observe(this, androidx.lifecycle.Observer { response ->
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {

                        listCitySpinner = data.answer ?: arrayListOf()

                        var firstItem = AnswerCity()
                        firstItem.name = getString(R.string.select_city)
                        listCitySpinner.add(0, firstItem)
                        adapterCity = ArrayAdapter<AnswerCity>(
                            context!!,
                            android.R.layout.simple_spinner_item,
                            listCitySpinner.toList()
                        )
                        adapterCity?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        nova_city_spinner.adapter = adapterCity

                        nova_city_spinner.onItemSelectedListener = object :
                            AdapterView.OnItemSelectedListener {

                            override fun onNothingSelected(parent: AdapterView<*>?) {
                            }

                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View?,
                                position: Int,
                                id: Long
                            ) {
                                val item = parent?.selectedItem as AnswerCity
                                deliveryType.dataCityNova = item
                                selectDepartment()
                            }
                        }
                    } else {
                        context?.toast(data?.message.toString())
                    }
                } else
                    context?.toast(response?.message().toString())
            })
    }


    fun selectDepartment() {
        viewModel.apiCheckoutDepList(token, deliveryType.dataCityNova.Ref ?: "")
            .observe(this, androidx.lifecycle.Observer { response ->
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {

                        listDepSpinner = data.answer ?: arrayListOf()

                        var firstItem = AnswerDep()
                        firstItem.name = getString(R.string.select_dep)
                        listDepSpinner.add(0, firstItem)
                        adapterDep = ArrayAdapter<AnswerDep>(
                            context!!,
                            android.R.layout.simple_spinner_item,
                            listDepSpinner
                        )
                        adapterDep?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        nova_department.adapter = adapterDep

                        nova_department.onItemSelectedListener = object :
                            AdapterView.OnItemSelectedListener {

                            override fun onNothingSelected(parent: AdapterView<*>?) {
                            }

                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View?,
                                position: Int,
                                id: Long
                            ) {
                                val item = parent?.selectedItem as AnswerDep
                                deliveryType.dataDepNova = item
                            }
                        }
                    } else {
                        context?.toast(data?.message.toString())
                    }
                } else
                    context?.toast(response?.message().toString())
            })
    }
}




