package noty.team.miska.checkout


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_checkout_case_courier.checkout_address_courier_arrow_back
import kotlinx.android.synthetic.main.fragment_checkout_case_nova.checkout_save_nova_button
import kotlinx.android.synthetic.main.fragment_checkout_case_pickup.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.model.DELIVERYTYPE
import noty.team.miska.model.DeliveryAdress
import noty.team.miska.modelRetrofit.AnswerPickup
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel


class CheckoutCasePickupShopFragment : Fragment() {

    var deliveryType: DeliveryAdress = DeliveryAdress()
    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    var token = ""
    var listPickupSpinner: ArrayList<AnswerPickup> = arrayListOf()
    var adapterPickup: ArrayAdapter<AnswerPickup>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_checkout_case_pickup, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_delivery_address),
            true,
            false
        )

        checkout_address_courier_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        listPickupSpinner.clear()
        deliveryType = DeliveryAdress()
        deliveryType.typeDeliverySelected = null

        token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
        }

        if (token != "") {
            //get zone
            viewModel.apiCheckoutPickupList(token)
                .observe(this, androidx.lifecycle.Observer { response ->
                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data?.error == false) {
                            listPickupSpinner.add(data.answer ?: AnswerPickup())

                            var firstItem = AnswerPickup()
                            firstItem.address = getString(R.string.select_shop)
                            listPickupSpinner.add(0, firstItem)
                            adapterPickup = ArrayAdapter<AnswerPickup>(
                                context!!,
                                android.R.layout.simple_spinner_item,
                                listPickupSpinner.toList()
                            )
                            adapterPickup?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            pickup_spinner.adapter = adapterPickup

                            pickup_spinner.onItemSelectedListener = object :
                                AdapterView.OnItemSelectedListener {

                                override fun onNothingSelected(parent: AdapterView<*>?) {
                                }

                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {
                                    val item = parent?.selectedItem as AnswerPickup
                                    deliveryType.dataPickup = item

                                }
                            }
                        } else {
                            context?.toast(data?.message.toString())
                        }
                    } else
                        context?.toast(response?.message().toString())
                })
        }

        checkout_save_nova_button.setOnClickListener {
            if (deliveryType.dataPickup.address==listPickupSpinner[0].address){
                context?.toast(getString(R.string.select_shop_))
                return@setOnClickListener
            }


            deliveryType.typeDeliverySelected = DELIVERYTYPE.PICKUP
            viewModel.deliveryType = deliveryType
            (activity as MainActivity).onBackPressed()
        }


    }

}
