package noty.team.miska.checkout


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_checkout_thankyou.*
import noty.team.miska.MainActivity
import noty.team.miska.R


class CheckoutThankyouFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_checkout_thankyou, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_order_sent),
            true,
            false
        )
        checkout_thankyou_next_button.setOnClickListener {
            (activity as MainActivity).showCatalog(false)
        }

        checkout_thankyou_arrow_back.setOnClickListener {
            (activity as MainActivity).showCatalog(false)
        }
        (activity as MainActivity).showBasketCounter(0)
        (activity as MainActivity).setMenuCounter(0, true)

    }
}
