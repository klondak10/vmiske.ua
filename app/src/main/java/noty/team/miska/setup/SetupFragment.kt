package noty.team.miska.setup


import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_setup.*
import noty.team.miska.*
import noty.team.miska.catalog.CatalogMainFragment
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.viewmodel.LANG_RU
import noty.team.miska.viewmodel.LANG_UA


class SetupFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_setup, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_promo_setup),
            false,
            true
        )

        var lang = Paper.book().read<String>(PAPER_LANG_DEFAULT, LANG_RU)

        when (lang) {
            LANG_RU -> setup_select_ukr.isChecked = true
            LANG_UA -> setup_select_rus.isChecked = true
        }

        setup_select_group
            .setOnCheckedChangeListener { group, checkedId ->
                when (checkedId) {
                    R.id.setup_select_rus -> {
                        Paper.book().write(PAPER_LANG_DEFAULT, LANG_UA)
                        (activity as MainActivity).recreate()
                    }
                    R.id.setup_select_ukr -> {
                        Paper.book().write(PAPER_LANG_DEFAULT, LANG_RU)
                        (activity as MainActivity).recreate()
                    }
                }
            }


        var userLogged = Paper.book().read<Boolean>(PAPER_USER_LOGGED, false)
        if (userLogged) {
            setup_exit_account_button.visibility = View.VISIBLE
        } else {
            setup_exit_account_button.visibility = View.GONE
        }

        setup_exit_account_button?.setOnClickListener {

            var dialog = AlertDialog.Builder(activity)
            dialog.setNegativeButton(getString(R.string.cancel)) { dialog, which ->
                dialog.dismiss()
            }
            dialog.setPositiveButton(getString(R.string.message_yes)) { alert, which ->

                run {
                    alert.dismiss()
                    Paper.book().write(PAPER_USER_LOGGED, false)
                    Paper.book().write(PAPER_TOKEN, "")


                    //facebook signout
                    LoginManager.getInstance().logOut()
                    //google signout
                    var accountGoogle = GoogleSignIn.getLastSignedInAccount(context)
                    if (accountGoogle != null) {

                        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .requestEmail()
                            .build()
                        val mGoogleSignInClient = GoogleSignIn.getClient(context!!, gso)

                        mGoogleSignInClient.revokeAccess()
                            .addOnCompleteListener(activity!!, object : OnCompleteListener<Void> {
                                override fun onComplete(p0: Task<Void>) {
                                }
                            })
                    }
                    (activity as MainActivity).clearBasket()
                    (activity as MainActivity).showUserMenu(false, "")
                    (activity as MainActivity).replaceFragment(
                        CatalogMainFragment(),
                        CATALOG_FRAGMENT,
                        true,
                        ""
                    )
                }
            }
            dialog
                .setTitle(getString(R.string.message_oy))
                .setMessage(getString(R.string.do_you_really_quit))
                .show()
        }
    }
}
