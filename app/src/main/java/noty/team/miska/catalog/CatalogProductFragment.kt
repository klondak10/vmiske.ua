package noty.team.miska.catalog

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_catalog_product.*
import noty.team.miska.MainActivity
import noty.team.miska.PAPER_LANG_DEFAULT
import noty.team.miska.PAPER_LIST_TYPE_PRODUCT
import noty.team.miska.R
import noty.team.miska.model.ItemProductCatalog
import noty.team.miska.modelRetrofit.ListProductResponse
import noty.team.miska.util.SetDataFromFilter
import noty.team.miska.viewmodel.DataViewModel
import noty.team.miska.viewmodel.IMAGE_URL
import noty.team.miska.viewmodel.LANG_RU
import retrofit2.Response


class CatalogProductFragment(var captionData: String) : Fragment(), SetDataFromFilter {

    var searchCategory: String? = null
    var pauseFlag = false

    override fun setNewDataFromFilter(newData: Response<ListProductResponse>?) {

        newData?.let {
            updateList(newData)
        }
    }


    private var viewModel: DataViewModel? = null
    private var listener: OnProductCatalogInteraction? = null
    var paperTypeList: Boolean = true
    var paperLang: String = LANG_RU
    var productAdapter: CatalogProductItemAdapter? = null
    var mainList: MutableList<ItemProductCatalog> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retainInstance = true
        viewModel = activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser)
            activity?.let {
                (it as MainActivity).fabHide()
            }
    }

    //apiGetProductsListByCategory
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_catalog_product, container, false)

        return view
    }

    fun updateList(response: Response<ListProductResponse>) {

        if (response.isSuccessful) {
            val data = response.body()
            if (data?.error == false) {
                val list = data.answer
                val size = list?.products?.size ?: 0
                if (size > 0) {

                    mainList.clear()

                    val newList = list?.products
                    newList?.let { newListData ->
                        for (newItem in newListData) {

                            var item = ItemProductCatalog(newItem?.name ?: "", R.drawable.product_1)
                            item.imageUrl = IMAGE_URL + newItem?.image
                            newItem?.discount?.let {
                                item.discount.dateEnd = newItem.discount?.dateEnd
                                item.discount.dateStart = newItem.discount?.dateStart
                                item.discount.price = newItem.discount?.price
                            }
                            item.quantity = newItem?.quantity ?: 0
                            item.price = newItem?.price.toString()
                            item.subprice = newItem?.subprice ?: arrayListOf()
                            item.productId = newItem?.productId ?: 0
                            item.weight = ""
                            item.rating = newItem?.rew ?: 0.0f

                            item.salesEnabled = newItem?.discount != null
                            item.SubCategory = captionData
                            mainList.add(item)
                        }
                    }
                    productAdapter?.notifyDataSetChanged()
                }
            }
        }
    }


    fun setCaption(str: String) {
        captionData = str
        Paper.book().write("captionData",captionData)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {
            (it as MainActivity).fabHide()
        }


        (activity as MainActivity).setRadioId(null)
        frame_catalog_product_empty.visibility = View.GONE
        recycler_list_catalog_product.visibility = View.VISIBLE
        buttons_sort_filter.visibility = View.VISIBLE

        paperTypeList = Paper.book().read(PAPER_LIST_TYPE_PRODUCT, true)
        paperLang = Paper.book().read(PAPER_LANG_DEFAULT, LANG_RU)
        (activity as MainActivity).showCustomAppBar(captionData, false, true)
        product_progress.visibility = View.VISIBLE

        mainList.clear()
        searchCategory = (activity as MainActivity).productIdSubCategory
        viewModel?.apiGetProductsListByCategory(paperLang, searchCategory ?: "")
            ?.observe(this, Observer { response ->
                if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED))
                    if (response.isSuccessful) {

                        recycler_list_catalog_product.visibility = View.VISIBLE
                        val data = response.body()
                        if (data?.error == false) {
                            val list = data.answer
                            val size = list?.products?.size ?: 0
                            if (size > 0) {
                                frame_catalog_product_empty.visibility = View.GONE
                                recycler_list_catalog_product.visibility = View.VISIBLE
                                buttons_sort_filter.visibility = View.VISIBLE
                                mainList.clear()

                                val newList = list?.products
                                newList?.let { newListData ->
                                    for (newItem in newListData) {
                                        var item = ItemProductCatalog(
                                            newItem?.name ?: "",
                                            R.drawable.product_1
                                        )
                                        //val itemImage = IMAGE_URL + newItem?.image
                                        item.imageUrl = IMAGE_URL + newItem?.image
                                        item.imageView = ImageView(context)
                                        newItem?.discount?.let {
                                            item.discount.dateEnd = newItem.discount?.dateEnd
                                            item.discount.dateStart =
                                                newItem.discount?.dateStart
                                            item.discount.price = newItem.discount?.price
                                        }
                                        item.quantity = newItem?.quantity ?: 0
                                        item.price = newItem?.price.toString()
                                        item.subprice = newItem?.subprice ?: arrayListOf()
                                        item.productId = newItem?.productId ?: 0
                                        item.weight = ""
                                        item.status = newItem?.status ?: ""
                                        item.rating = newItem?.rew ?: 0.0f

                                        item.salesEnabled = newItem?.discount != null
                                        item.SubCategory = captionData
                                        mainList.add(item)
                                    }
                                }
                                (activity as MainActivity).setNewFilterData(
                                    response,
                                    searchCategory ?: "",
                                    captionData
                                )
                                productAdapter?.notifyDataSetChanged()
                                product_progress.visibility = View.VISIBLE

                            } else {
                                mainList.clear()
                                recycler_list_catalog_product.visibility = View.GONE
                                //show no stuff
                                frame_catalog_product_empty.visibility = View.VISIBLE
                                buttons_sort_filter.visibility = View.GONE
                            }
                        }
                    }
                product_progress.visibility = View.INVISIBLE
            })
        //  }

        productAdapter =
            CatalogProductItemAdapter(context!!, mainList, listener, paperTypeList, paperLang)
            { savePosition(it) }

        if (paperTypeList) {
            recycler_list_catalog_product?.layoutManager = LinearLayoutManager(context)
            list_type_product_button.setBackgroundResource(R.drawable.layout_grid_icon)
        } else {
            recycler_list_catalog_product?.layoutManager = GridLayoutManager(context, 2)
            list_type_product_button.setBackgroundResource(R.drawable.layout_list_icon)
        }
        recycler_list_catalog_product?.adapter = productAdapter
        recycler_list_catalog_product?.adapter?.registerAdapterDataObserver(object :
            RecyclerView.AdapterDataObserver() {

            override fun onChanged() {
                super.onChanged()
                var index = listPosition
                if (index < 0) index = 0
                recycler_list_catalog_product?.smoothScrollToPosition(index)
            }
        })

        list_type_product_button.setOnClickListener {
            if (paperTypeList) {
                list_type_product_button.setBackgroundResource(R.drawable.layout_list_icon)
                paperTypeList = false
                Paper.book().write(PAPER_LIST_TYPE_PRODUCT, false)
                recycler_list_catalog_product?.layoutManager = GridLayoutManager(context, 2)
                productAdapter = CatalogProductItemAdapter(
                    context!!,
                    mainList,
                    listener,
                    paperTypeList,
                    paperLang
                ) { savePosition(it) }

                recycler_list_catalog_product.adapter = productAdapter
                recycler_list_catalog_product.adapter?.notifyDataSetChanged()
                scrollToPosition()

            } else {
                list_type_product_button.setBackgroundResource(R.drawable.layout_grid_icon)
                paperTypeList = true
                Paper.book().write(PAPER_LIST_TYPE_PRODUCT, true)
                recycler_list_catalog_product?.layoutManager = LinearLayoutManager(context)
                productAdapter = CatalogProductItemAdapter(
                    context!!,
                    mainList,
                    listener,
                    paperTypeList,
                    paperLang
                ) { savePosition(it) }
                recycler_list_catalog_product?.adapter = productAdapter
                recycler_list_catalog_product.adapter?.notifyDataSetChanged()
                scrollToPosition()
            }
        }
        list_sort_product_button.setOnClickListener {
            (activity as MainActivity).showSortDrawer(true)
        }

        list_sort_product_text.setOnClickListener {
            list_sort_product_button.callOnClick()
        }

        list_filter_product_button.setOnClickListener {
            (activity as MainActivity).showFilterDrawer(true)
        }

        list_filter_product_text.setOnClickListener {
            list_filter_product_button.callOnClick()
        }
    }

    fun scrollToPosition() {
        recycler_list_catalog_product?.adapter?.notifyDataSetChanged()
    }

    var listPosition = 0
    fun savePosition(pos: Int) {
        listPosition = pos
    }

    override fun onResume() {
        super.onResume()
        //scrollToPosition()
        (recycler_list_catalog_product.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(
            viewModel?.listPositionCatalog ?: 0,
            viewModel?.listOffsetCatalog ?: 0
        )

        if (pauseFlag)
            product_progress.visibility = View.INVISIBLE
    }


    override fun onPause() {
        super.onPause()
        pauseFlag = true

        val linearLayoutManager = recycler_list_catalog_product.layoutManager as LinearLayoutManager
        val v = linearLayoutManager.getChildAt(0)
        val top = if (v == null) 0 else v.top - linearLayoutManager.paddingTop
        viewModel?.listPositionCatalog = linearLayoutManager.findFirstVisibleItemPosition()
        viewModel?.listOffsetCatalog = top

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnProductCatalogInteraction) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnMainCatalogListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnProductCatalogInteraction {
        fun onProductListClick(item: ItemProductCatalog?)
    }

    companion object {


        @JvmStatic
        fun newInstance(caption: String) =
            CatalogProductFragment(caption).apply {
            }
    }

}
