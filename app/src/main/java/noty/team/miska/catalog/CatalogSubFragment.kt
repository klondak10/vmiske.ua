package noty.team.miska.catalog

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_catalog_sub.*
import noty.team.miska.MainActivity
import noty.team.miska.PAPER_LANG_DEFAULT
import noty.team.miska.PAPER_LIST_TYPE_PRODUCT
import noty.team.miska.R
import noty.team.miska.model.ItemCatalog
import noty.team.miska.modelRetrofit.CatalogResponse
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import noty.team.miska.viewmodel.LANG_RU


class CatalogSubFragment(caption: String, subNumber: String) : Fragment() {

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    private var caption = ""
    private var subNumber = ""
    var paperTypeList: Boolean = true
    var paperLang: String = LANG_RU

    init {
        this.caption = caption
        this.subNumber = subNumber
    }

    private var listener: OnSubCatalogInteraction? = null


    var mainList: MutableList<ItemCatalog> = arrayListOf()
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser)
            activity?.let {
                (it as MainActivity).fabHide()
            }
    }


    var progressBar: ProgressBar? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_catalog_sub, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        paperTypeList = Paper.book().read(PAPER_LIST_TYPE_PRODUCT, true)
        paperLang = Paper.book().read(PAPER_LANG_DEFAULT, LANG_RU)

        activity?.let {
            (it as MainActivity).fabHide()
        }

        viewModel.apiGetSubCategoryCatalog(subNumber, paperLang)

        progressBar = ProgressBar(context, null, android.R.attr.progressBarStyleLarge)
        val params = RelativeLayout.LayoutParams(100, 100)
        params.addRule(RelativeLayout.CENTER_IN_PARENT)
        var mainView = subcategory_frame
        mainView.addView(progressBar, params)

        progressBar?.visibility = View.VISIBLE

        (activity as MainActivity).showCustomAppBar(caption, false, true)
        mainList.clear()
        recycler_list_catalog_sub?.layoutManager = LinearLayoutManager(context)
        recycler_list_catalog_sub?.adapter = CatalogSubItemAdapter(mainList, listener)


        viewModel.mainSubCategoryLD.observe(this, Observer<CatalogResponse> { response ->
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
                var newList: ArrayList<ItemCatalog> = arrayListOf()
                if (!response.error && response.serverError == null) {
                    response.answer?.let {
                        progressBar?.visibility = View.GONE

                        for (item in response.answer!!) {
                            if (item?.name != null && item.img != null) {
                                var    itemCatalog =
                                    ItemCatalog(
                                        item.name!!,
                                        item.img!!,
                                        item.cId!!,
                                        item.pcount!!
                                    )


                                newList.add(itemCatalog)
                            }
                        }
                        mainList.clear()
                        mainList.addAll(newList)
                        recycler_list_catalog_sub?.adapter?.notifyDataSetChanged()

                        (recycler_list_catalog_sub.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(
                            viewModel.listPositionCatalogSub ?: 0,
                            viewModel.listOffsetCatalogSub ?: 0
                        )
                    }
                } else {
                    //show error
                    if (response.error)
                        context?.toast(response.message.toString())
                    else {
                        context?.toast(response?.serverError.toString())
                    }
                }
            }
        })


    }


    var lastFirstVisiblePosition: Int? = null
    override fun onPause() {
        super.onPause()

        val linearLayoutManager = recycler_list_catalog_sub.layoutManager as LinearLayoutManager
        val v = linearLayoutManager.getChildAt(0)
        val top = if (v == null) 0 else v.top - linearLayoutManager.paddingTop
        viewModel?.listPositionCatalogSub = linearLayoutManager.findFirstVisibleItemPosition()
        viewModel?.listOffsetCatalogSub = top

    }

    override fun onResume() {
        super.onResume()


        (recycler_list_catalog_sub.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(
            viewModel?.listPositionCatalogSub ?: 0,
            viewModel?.listOffsetCatalogSub ?: 0
        )

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnSubCatalogInteraction) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnMainCatalogListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnSubCatalogInteraction {
        fun onSubListClick(item: ItemCatalog?)
    }
}
