package noty.team.miska.catalog

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.asksira.loopingviewpager.LoopingViewPager.IndicatorPageChangeListener
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_catalog_main.*
import noty.team.miska.MainActivity
import noty.team.miska.PAPER_LANG_DEFAULT
import noty.team.miska.PAPER_LIST_TYPE_PRODUCT
import noty.team.miska.R
import noty.team.miska.model.ItemCatalog
import noty.team.miska.modelRetrofit.BannerResponse
import noty.team.miska.modelRetrofit.CatalogResponse
import noty.team.miska.promoBanner.PromoInfoFragment
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import noty.team.miska.viewmodel.LANG_RU


const val INFINITE_INFO_FRAGMENT = "INFINITE_INFO_FRAGMENT"

class CatalogMainFragment : Fragment() {

    private var listener: OnMainCatalogListener? = null
    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }
    var mainList: MutableList<ItemCatalog> = arrayListOf()
    var bannersInfo: BannerResponse = BannerResponse()
    var showHideAdverFlag: Boolean = true
    var paperTypeList: Boolean = true
    var paperLang: String = LANG_RU
    var bitmapList: ArrayList<Bitmap> = arrayListOf()
    var onBannerClick: View.OnClickListener? = null

    fun setList(list: MutableList<ItemCatalog>) {
        mainList = list
    }

    fun setShowHideAdver(show: Boolean) {
        showHideAdverFlag = show
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.mainCatalogLD.observe(this, Observer
        <CatalogResponse>() { response ->
            progressBar?.visibility = View.GONE
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
                var newList: ArrayList<ItemCatalog> = arrayListOf()
                if (!response.error && response.serverError == null) {
                    response.answer?.let {

                        for (item in response.answer!!) {
                            if (item?.name != null || item?.img != null) {

                                var    itemCatalog =
                                        ItemCatalog(
                                            item.name!!,
                                            item.img!!,
                                            item.cId!!,
                                            item.pcount!!
                                        )

                                newList.add(itemCatalog)
                            }
                        }
                        mainList.clear()
                        mainList.addAll(newList)
                        recycler_list_catalog_main?.adapter?.notifyDataSetChanged()
                    }
                } else {
                    //show error
                    if (response.error)
                        context?.toast(response.message.toString())
                    else {
                        context?.toast(response?.serverError.toString())
                    }
                }
            }
        })

        viewModel.mainBannersLD.observe(this, Observer
        <BannerResponse>() { response ->
            banner_progress?.visibility = View.GONE

            if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {

                if (!response.error!! || response.serverError == null) {
                    response.answer?.let {

                        bannersInfo = response
                        bitmapList.clear()
                        for (item in response.answer!!) {
                            if (item?.bitmap != null) {
                                bitmapList.add(item.bitmap!!)
                            }
                        }
                        bannersAdapter?.setItemList(bitmapList)
                        infinite_page_indicator.setCount(infinite_viewpager.getIndicatorCount())
                        infinite_viewpager.reset()
                        infinite_page_indicator?.setSelection(0)
                    }
                } else {
                    //show error
                    context?.toast(response.message.toString() + "\n " + response.serverError)
                }
            }
        })
    }


    var progressBar: ProgressBar? = null

    var bannersAdapter: ImageInfiniteAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(noty.team.miska.R.layout.fragment_catalog_main, container, false)

        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        paperTypeList = Paper.book().read(PAPER_LIST_TYPE_PRODUCT, true)
        paperLang = Paper.book().read(PAPER_LANG_DEFAULT, LANG_RU)
        mainList.clear()
        bitmapList.clear()
        banner_progress?.visibility = View.VISIBLE


        viewModel.apiGetCatalogList(paperLang)
        viewModel.apiGetBannersList()

        recycler_list_catalog_main?.layoutManager = LinearLayoutManager(context)
        recycler_list_catalog_main?.adapter = CatalogMainItemAdapter(context!!, mainList, listener)

        onBannerClick = View.OnClickListener {

            var fragment = PromoInfoFragment()

            if (it is FrameLayout) {
                var image =
                    it.findViewById<ImageView>(noty.team.miska.R.id.image_infinite_view).drawable
                fragment.setInfoImageId(image)

                if (!bannersInfo.error!! && bannersInfo.serverError == null) {
                    fragment.setUrlImage(bannersInfo.answer?.get(number)?.link!!)

                    (activity as MainActivity).fabHide()
                    (activity as MainActivity).replaceFragment(
                        fragment,
                        INFINITE_INFO_FRAGMENT,
                        false,
                        ""
                    )
                }
            }

        }
        bannersAdapter = ImageInfiniteAdapter(context!!, bitmapList, true, onBannerClick, ({
            number = it!!

        }))
        infinite_viewpager.adapter = bannersAdapter
        infinite_viewpager.adapter?.notifyDataSetChanged()
        infinite_viewpager?.setIndicatorSmart(true)
        val metrics = DisplayMetrics()
        if (metrics.widthPixels > 500) {

            infinite_viewpager?.scaleX = 1.6f
            infinite_viewpager?.scaleY = 1.6f

        }
        //Custom bind indicator
        infinite_page_indicator?.setCount(infinite_viewpager.getIndicatorCount())
        infinite_viewpager.reset()
        infinite_page_indicator?.setSelection(0)

        infinite_viewpager?.setIndicatorPageChangeListener(object :
            IndicatorPageChangeListener {
            override fun onIndicatorProgress(
                selectingPosition: Int,
                progress: Float
            ) {
                infinite_page_indicator?.setSelection(selectingPosition)
            }

            override fun onIndicatorPageChange(newIndicatorPosition: Int) {

            }
        })
        if (showHideAdverFlag) {
            infinite_frame.visibility = View.VISIBLE
            (activity as MainActivity).showCustomAppBar("", false, true)
            (activity as MainActivity).fabShow()
        } else {
            infinite_frame.visibility = View.GONE
            (activity as MainActivity).fabHide()
            (activity as MainActivity).showCustomAppBar(
                getString(noty.team.miska.R.string.toolbar_name_catalog),
                false,
                true
            )
        }
    }

    var number: Int = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnMainCatalogListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnMainCatalogListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnMainCatalogListener {
        fun onMainListInteraction(item: ItemCatalog?)
    }

    override fun onPause() {

        infinite_viewpager.pauseAutoScroll();
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        infinite_viewpager.resumeAutoScroll();
    }
}

