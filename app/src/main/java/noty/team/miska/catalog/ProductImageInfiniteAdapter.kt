package noty.team.miska.catalog


import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.asksira.loopingviewpager.LoopingPagerAdapter
import noty.team.miska.R


class ProductImageInfiniteAdapter(
    context: Context,
    itemList: ArrayList<Bitmap>,
    isInfinite: Boolean,
    onClick: View.OnClickListener?
) : LoopingPagerAdapter<Bitmap>(context, itemList, isInfinite) {

    var onClick: View.OnClickListener? = null

    init {
        this.onClick = onClick
    }

    override fun inflateView(viewType: Int, container: ViewGroup, listPosition: Int): View {
        return LayoutInflater.from(context)
            .inflate(R.layout.image_infinite_product, container, false)
    }

    override fun bindView(convertView: View, listPosition: Int, viewType: Int) {
        convertView.findViewById<ImageView>(R.id.image_infinite_view)
            .setImageBitmap(itemList[listPosition])

        onClick?.let {
            convertView.setOnClickListener(onClick)
        }

    }
}