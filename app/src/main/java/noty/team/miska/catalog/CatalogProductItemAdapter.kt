package noty.team.miska.catalog


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.squareup.picasso.Picasso
import io.paperdb.Paper
import kotlinx.android.synthetic.main.catalog_main_item_product_list.view.*
import noty.team.miska.R
import noty.team.miska.model.ItemProductCatalog
import noty.team.miska.viewmodel.LANG_RU
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit


const val CURRENCY = " грн"

class CatalogProductItemAdapter(
    val context: Context,
    private val mValues: List<ItemProductCatalog>,
    private val mListener: CatalogProductFragment.OnProductCatalogInteraction?,
    listType: Boolean,
    langDefault: String,
    var callback: (position: Int) -> Unit
) : RecyclerView.Adapter<CatalogProductItemAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener
    var listType: Boolean? = null
    var langDefault: String = LANG_RU

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ItemProductCatalog
            mListener?.onProductListClick(item)
            callback(item.listPosition)

        }
        this.listType = listType
        this.langDefault = langDefault
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view: View? = null

        if (listType!!) {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.catalog_main_item_product_list, parent, false)
        } else {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.catalog_main_item_product_grid, parent, false)
        }
        return ViewHolder(view!!) {
            callback(it)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var item = mValues[position]

        item.listPosition = position
        var newName = item.name.replace("&quot;", "")
        holder.mText.text = newName.replace("amp;", "")

        holder.mmRating.rating = item.rating

        holder.mPrice.setTextColor(
            ContextCompat.getColor(
                context,
                R.color.colorMainRed
            )
        )
        if (item.quantity != 0) { }
        Picasso.get()
            .load(item.imageUrl)
            .placeholder(R.drawable.placeholder_small)
            .error(R.drawable.placeholder_small)
            .into(holder.mImage)
        var nPrice = formatNumberWithSpaces(item.price.toInt())

        holder.mPrice.text = nPrice + CURRENCY
        holder.mWeight.text = item.weight
        val sizeSubprice = item.subprice.size
        if (sizeSubprice > 0) {

            item.subprice.let { subprice ->
                val flag = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                var builder = SpannableStringBuilder()

                var reversPrice = subprice.reversed()
                for (newWeight in reversPrice) {
                    var newWeightStr: SpannableString = SpannableString(newWeight.name + " ")

                    if (newWeight.quantity != 0) {
                        newWeightStr.setSpan(
                            ForegroundColorSpan(Color.BLACK),
                            0,
                            newWeightStr.length,
                            flag
                        )
                    } else {
                        newWeightStr.setSpan(
                            //ForegroundColorSpan(Color.LTGRAY),
                            ForegroundColorSpan(Color.BLACK),
                            0,
                            newWeightStr.length,
                            flag
                        )
                    }
                    builder.append(newWeightStr)
                }

                holder.mWeight.text = builder


                var newSubrice: ArrayList<Int> = arrayListOf()


                for (newPrice in subprice) {
                    val basePrice = item.price.toInt()
                    val subPrice = newPrice.price ?: 0

                    newSubrice.add(basePrice + subPrice)
                }

                for (newPrice in subprice) {
                    val basePrice = item.price.toInt()
                    val subPrice = newPrice.price ?: 0
                    newSubrice.add(basePrice + subPrice)
                }

                newSubrice.sort()
                if (newSubrice.size > 1) {
                    var minPrice = newSubrice[0]
                    var maxPrice = newSubrice[newSubrice.size - 1]

                    //val resPrice = minPrice.toString() + "-" + maxPrice.toString() + CURRENCY
                    var resPrice = formatNumberWithSpaces(minPrice) + " - " + formatNumberWithSpaces(maxPrice) + CURRENCY
                    if (minPrice == maxPrice)
                        resPrice = formatNumberWithSpaces(maxPrice) + CURRENCY
                    holder.mPrice.text = resPrice
                }
                if (newSubrice.size == 1) {
                    var minPrice = newSubrice[0]
                    val resPrice = formatNumberWithSpaces(minPrice) + CURRENCY
                    holder.mPrice.text = resPrice
                }
            }
        }

        if (item.salesEnabled) {
            holder.mChip.visibility = View.VISIBLE
            holder.mDiscountPrice.visibility = View.VISIBLE
            holder.mDiscountPrice.text = formatNumberWithSpaces(item.price.toInt()) + CURRENCY
            holder.mPrice.text = formatNumberWithSpaces(item.discount.price?.toInt()!!) + CURRENCY
            holder.mDiscountPrice.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            if (convertDateToLong(item.discount.dateEnd!!)- System.currentTimeMillis()<0){
                holder.mChip.visibility = View.GONE
            }else {
                holder.mChipLabel.visibility = View.VISIBLE
                holder.mChipLabel.text = context.getString(R.string.label)
                holder.mChip.text =
                    context.resources.getString(R.string.label_sales_count) + convertSecondsToHMmSs(
                        convertDateToLong(item.discount.dateEnd!!) - System.currentTimeMillis()
                    )
            }

        }
        else {
            holder.mChip.visibility = View.GONE
            holder.mDiscountPrice.visibility = View.GONE
        }
        if (item.status.contains("2-3")){

            if (Paper.book().exist("captionData")&&Paper.book().read<String>("captionData").contains("Товар")){
                holder.mChipLabel.visibility = View.VISIBLE
                holder.mChipLabel.text = context.getString(R.string.label)
            }

        }

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun convertDateToLong(date: String): Long {
        val df = SimpleDateFormat("yyyy-MM-dd")
        return df.parse(date).time
    }

    @SuppressLint("DefaultLocale")
    fun convertSecondsToHMmSs(seconds: Long): String? {

        val hms = java.lang.String.format(
            "%02d:%02d:%02d:%02d", TimeUnit.MILLISECONDS.toDays(seconds),
            TimeUnit.MILLISECONDS.toHours(seconds) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(seconds)),
            TimeUnit.MILLISECONDS.toMinutes(seconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(seconds)),
            TimeUnit.MILLISECONDS.toSeconds(seconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(seconds))
        )

        return hms
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View, var callback: (position: Int) -> Unit) :
        RecyclerView.ViewHolder(mView) {

        val mImage: ImageView = mView.product_info_image
        val mText: TextView = mView.product_info_text
        val mPrice: TextView = mView.product_info_price
        val mDiscountPrice: TextView = mView.product_info_discount_price
        val mWeight: TextView = mView.product_info_chips_group_type
        val mmRating: RatingBar = mView.product_info_rating_bar
        val mChip: Chip = mView.sales_chip
        val mChipLabel: Chip = mView.sales_chip_label
    }
}

fun formatNumberWithSpaces(number: Int): String {
    val symbols = DecimalFormatSymbols.getInstance()
    symbols.groupingSeparator = ' '
    val formatter = DecimalFormat("###,###", symbols)
    return formatter.format(number)
}
