package noty.team.miska.catalog


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.catalog_main_item.view.*
import kotlinx.android.synthetic.main.catalog_main_item.view.item_name
import kotlinx.android.synthetic.main.catalog_main_item_sub.view.*
import kotlinx.android.synthetic.main.catalog_main_item_sub.view.product_info_image
import noty.team.miska.R
import noty.team.miska.model.ItemCatalog


class CatalogSubItemAdapter(
    private val mValues: List<ItemCatalog>,
    private val mListener: CatalogSubFragment.OnSubCatalogInteraction?
) : RecyclerView.Adapter<CatalogSubItemAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ItemCatalog
            mListener?.onSubListClick(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.catalog_main_item_sub, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mName.text = item.name.replace("amp;", "")

        if (item.subCategoryCount ?: 0 > 0) {
            holder.mArrow.visibility = View.VISIBLE
        } else {
            holder.mArrow.visibility = View.GONE
        }


            Glide.with(holder.mImage.context)
                .load("https://vmiske.ua/image/"+item.imageUrl)
                .placeholder(R.drawable.placeholder_small)
                .error(R.drawable.placeholder_small)
                .into(holder.mImage)



        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mName: TextView = mView.item_name
        val mImage: ImageView = mView.product_info_image
        val mArrow: ImageView = mView.item_next_arrow_category
    }
}
