package noty.team.miska.catalog


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide.with
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.catalog_main_item.view.*
import noty.team.miska.R
import noty.team.miska.catalog.CatalogMainFragment.OnMainCatalogListener
import noty.team.miska.model.ItemCatalog
import noty.team.miska.viewmodel.IMAGE_URL


class CatalogMainItemAdapter(
    private val mContext: Context,
    private val mValues: List<ItemCatalog>,
    private val mListener: OnMainCatalogListener?
) : RecyclerView.Adapter<CatalogMainItemAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ItemCatalog
            mListener?.onMainListInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.catalog_main_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mName.text = item.name.replace("amp;", "")

        if (!item.imageUrl.isNullOrEmpty()) {
            Picasso.get()
                .load("https://vmiske.ua/image/"+item.imageUrl)
                .placeholder(R.drawable.placeholder_small)
                .error(R.drawable.placeholder_small)
                .into(holder.mImage)
        }

            with(holder.mView) {
                tag = item
                setOnClickListener(mOnClickListener)
            }

    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mName: TextView = mView.item_name
        val mNextArrow: ImageView = mView.item_next
        val mImage: ImageView = mView.product_info_image
        override fun toString(): String {
            return super.toString() + " '" + mName.text + "'"
        }
    }
}
