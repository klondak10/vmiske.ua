package noty.team.miska.paymentLiqpay.enums;

public enum PaymentStatuses {
    success, failure, error, reversed, sandbox,
    wait_secure, wait_accept, processing
}

