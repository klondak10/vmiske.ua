package noty.team.miska.paymentLiqpay.ilisteners;

import noty.team.miska.paymentLiqpay.service.background.params.CheckPaymentStatusParams;

public interface ICheckPaymentStatusListener {
    void CheckPaymentStatusResult(CheckPaymentStatusParams params);
}
