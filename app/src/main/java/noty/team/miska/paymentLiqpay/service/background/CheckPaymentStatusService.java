package noty.team.miska.paymentLiqpay.service.background;

import android.os.AsyncTask;

import noty.team.miska.paymentLiqpay.ilisteners.ICheckPaymentStatusListener;
import noty.team.miska.paymentLiqpay.service.ConfigurationService;
import noty.team.miska.paymentLiqpay.service.background.params.CheckPaymentStatusParams;

import java.util.HashMap;

import ua.privatbank.paylibliqpay.ErrorCode;
import ua.privatbank.paylibliqpay.LiqPay;
import ua.privatbank.paylibliqpay.LiqPayCallBack;

public class CheckPaymentStatusService
        extends AsyncTask<CheckPaymentStatusParams, Integer, CheckPaymentStatusParams>
        implements LiqPayCallBack {

    private CheckPaymentStatusParams params;


    @Override
    protected CheckPaymentStatusParams doInBackground(CheckPaymentStatusParams... checkPaymentStatusParams) {
        params = checkPaymentStatusParams[0];
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("version", params.getVersion());
        map.put("public_key", params.getPublicKey());
        map.put("order_id", params.getOrderId());
        if (params !=null){
        LiqPay.api(params.getExecutionContext(), "status", map, ConfigurationService.getInstance().getLiqPayPrivateKey(), this);}

        return params;
    }

    @Override
    protected void onPostExecute(CheckPaymentStatusParams params) {
        super.onPostExecute(params);
    }

    @Override
    public void onResponseSuccess(String s) {
        params.setOperationResult(s);
        ICheckPaymentStatusListener listener = params.getListener();
        listener.CheckPaymentStatusResult(params);
    }

    @Override
    public void onResponceError(ErrorCode errorCode) {
        params.setOperationError(errorCode);
        ICheckPaymentStatusListener listener = params.getListener();
        listener.CheckPaymentStatusResult(params);
    }
}
