package noty.team.miska.paymentLiqpay.ilisteners;

import noty.team.miska.paymentLiqpay.service.background.params.CheckoutPaymentParams;

public interface ICheckoutResultListener {
    void checkoutResult(CheckoutPaymentParams params);
}
