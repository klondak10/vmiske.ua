package noty.team.miska.paymentLiqpay.service;

public class ConfigurationService {
    private static ConfigurationService _instance;
    //private String LiqPayPublicKey = "i322*********";
    //private String LiqPayPrivateKey = "mEZL2X********************************";
    private String LiqPayPublicKey = "i82408937272";
    private String LiqPayPrivateKey = "4ef3Gr1GsVPKEdezpfrdUng8E6kCHtr19HjwaYz1";
    private Long RetryInterval = 10000L;     //Retry interval, milliseconds

    private ConfigurationService() {    }

    public static ConfigurationService getInstance() {
        if (_instance == null)
            _instance = new ConfigurationService();
        return _instance;
    }

    public String getLiqPayPrivateKey(){
        return LiqPayPrivateKey;
    }

    public String getLiqPayPublicKey() {
        return LiqPayPublicKey;
    }

    public Long getRetryInterval() { return RetryInterval; }
}
