package noty.team.miska.paymentLiqpay.service;

import android.content.Context;

import noty.team.miska.paymentLiqpay.ilisteners.ICheckPaymentStatusListener;
import noty.team.miska.paymentLiqpay.ilisteners.ICheckoutResultListener;
import noty.team.miska.paymentLiqpay.service.background.CheckPaymentStatusService;
import noty.team.miska.paymentLiqpay.service.background.CheckoutPaymentService;
import noty.team.miska.paymentLiqpay.service.background.params.CheckPaymentStatusParams;
import noty.team.miska.paymentLiqpay.service.background.params.CheckoutPaymentParams;
import noty.team.miska.paymentLiqpay.vo.PaymentResult;

public class LiqPayService {
    private static LiqPayService _instance;
    private LiqPayService(){}

    public static LiqPayService getInstance(){
        if (_instance == null)
        {
            _instance = new LiqPayService();
        }
        return _instance;
    }

    public void checkout(Context context, String amount2pay, String currency,
                         ICheckoutResultListener listener,String description){
        CheckoutPaymentParams params = new CheckoutPaymentParams(context);
        params.setAmount(amount2pay);
        params.setDescription(description);
        params.setCurrency(currency);
        params.setListener(listener);
        new CheckoutPaymentService().execute(params);
    }

    public void checkPayment(Context context, PaymentResult paymentResult, ICheckPaymentStatusListener listener){
        CheckPaymentStatusParams params = new CheckPaymentStatusParams(context, paymentResult);
        params.setListener(listener);
        new CheckPaymentStatusService().execute(params);
    }
}
