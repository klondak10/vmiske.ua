package noty.team.miska

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.PorterDuff
import android.graphics.drawable.LayerDrawable
import android.net.Uri
import android.os.*
import android.util.Base64.encode
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.FacebookSdk
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.material.navigation.NavigationView
import io.paperdb.Paper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_product_filter.*
import kotlinx.android.synthetic.main.fragment_product_sort.*
import kotlinx.android.synthetic.main.fragment_product_sort.view.*
import kotlinx.android.synthetic.main.product_info_filter_item.*
import noty.team.miska.basket.BasketProductEmptyFragment
import noty.team.miska.basket.BasketProductFragment
import noty.team.miska.basket.CountDrawable
import noty.team.miska.bonus.BonusFragment
import noty.team.miska.catalog.CatalogMainFragment
import noty.team.miska.catalog.CatalogProductFragment
import noty.team.miska.catalog.CatalogSubFragment
import noty.team.miska.info.InfoListFragment
import noty.team.miska.login.LoginPagerFragment
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.model.*
import noty.team.miska.modelRetrofit.CatalogResponse
import noty.team.miska.modelRetrofit.Discount
import noty.team.miska.modelRetrofit.ListProductResponse
import noty.team.miska.product.CatalogProductInfoPagerFragment
import noty.team.miska.product.ProductReviewFragment
import noty.team.miska.profile.IProfileData
import noty.team.miska.profile.ProfileEditFragment
import noty.team.miska.profile.ProfileFragment
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.search.SearchFragment
import noty.team.miska.setup.SetupFragment
import noty.team.miska.shopingAmbar.ShoppingListFragment
import noty.team.miska.util.*
import noty.team.miska.viewmodel.DataViewModel
import noty.team.miska.viewmodel.LANG_RU
import retrofit2.Response
import java.net.URLEncoder.encode
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

const val CATALOG_FRAGMENT = "CATALOG_FRAGMENT"
const val BONUS_FRAGMENT = "BONUS_FRAGMENT"
const val INFOLIST_FRAGMENT = "INFOLIST_FRAGMENT"
const val SETUP_FRAGMENT = "SETUP_FRAGMENT"
const val SHOPPING_FRAGMENT = "SHOPPING_FRAGMENT"
const val NOINTERNET_FRAGMENT = "NOINTERNET_FRAGMENT"
const val SHOPPING_ITEM_FRAGMENT = "SHOPPING_ITEM_FRAGMENT"
const val LOGIN_FRAGMENT = "LOGIN_FRAGMENT"
const val SEARCH_RESULT_FRAGMENT = "SEARCH_RESULT_FRAGMENT"
const val LOGIN_PAGER_FRAGMENT = "LOGIN_PAGER_FRAGMENT"
const val LOGIN_FORGOT_CHANGE_FRAGMENT = "LOGIN_FORGOT_CHANGE_FRAGMENT"
const val PROFILE_CHANGE_PASSWORD_FRAGMENT = "PROFILE_CHANGE_PASSWORD_FRAGMENT"
const val PROFILE_PASSWORD_CHANGED_FRAGMENT = "PROFILE_PASSWORD_CHANGED_FRAGMENT"
const val BASKET_FRAGMENT = "BASKET_FRAGMENT"
const val PROFILE_FRAGMENT = "PROFILE_FRAGMENT"
const val PROFILE_EDIT_FRAGMENT = "PROFILE_EDIT_FRAGMENT"
const val BASKET_EMPTY_FRAGMENT = "BASKET_EMPTY_FRAGMENT"
const val CHECKOUT_ERROR_FRAGMENT = "CHECKOUT_ERROR_FRAGMENT"
const val CHECKOUT_SUCCESS_FRAGMENT = "CHECKOUT_SUCCESS_FRAGMENT"
const val CATALOG_SUB_FRAGMENT = "CATALOG_SUB_FRAGMENT"
const val CATALOG_PRODUCT_FRAGMENT = "CATALOG_PRODUCT_FRAGMENT"
const val CATALOG_PRODUCT_INFO = "CATALOG_PRODUCT_INFO"
const val CATALOG_PRODUCT_PAGER_INFO = "CATALOG_PRODUCT_PAGER_INFO"
const val SEARCH_FRAGMENT = "SEARCH_FRAGMENT"
const val USER = false
const val PAPER_USER_LOGGED = "PAPER_USER_LOGGED"
const val PAPER_USER_NAME = "PAPER_USER_NAME"
const val PAPER_LIST_TYPE_PRODUCT = "PAPER_LIST_TYPE_PRODUCT"
const val PAPER_LANG_DEFAULT = "PAPER_LANG_DEFAULT"
const val PAPER_СURRENCY_DEFAULT = "PAPER_СURRENCY_DEFAULT"


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    UtilInterface, IRadioButton,

    CatalogMainFragment.OnMainCatalogListener, CatalogSubFragment.OnSubCatalogInteraction,
    CatalogProductFragment.OnProductCatalogInteraction,
    ProductReviewFragment.OnProductReviewInteraction,
    BasketProductFragment.OnBasketProductInteraction,
    SetFilterData,
    IProfileData, IGoogleAccountStore {

    var payFlag: Boolean = false

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        payFlag = true
    }

    fun getProfileName() {

        var token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
            viewModel?.apiGetProfile(
                token
            )?.observe(this, Observer { response ->
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {
                        showUserMenu(true, data.answer?.firstname.toString())
                        setProfileData(
                            UserProfile(
                                email = data.answer?.email.toString(),
                                telephone = data.answer?.telephone.toString(),
                                firstname = data.answer?.firstname.toString(),
                                lastname = data.answer?.lastname.toString(), token = token
                            )
                        )
                    } else
                        this.toast(data?.message.toString())
                } else
                    this.toast(response?.message().toString())
            })
        }
    }


    var userAccount: GoogleSignInAccount? = null

    override fun setGoogleAccount(account: GoogleSignInAccount?) {
        this.userAccount = account
    }

    override fun getGoogleAccount(): GoogleSignInAccount? {
        return this.userAccount
    }


    var userProfile: UserProfile? = null
    override fun setProfileData(userProfile: UserProfile?) {
        this.userProfile = userProfile
    }

    override fun getProfileData(): UserProfile? {
        return userProfile
    }

    var sortRadioIdType: Int? = null

    override fun getRadioId(): Int? {
        return sortRadioIdType
    }

    override fun setRadioId(newId: Int?) {
        sortRadioIdType = newId

    }

    var paperTypeList: Boolean = true
    var paperLang: String = LANG_RU


    var imageId: java.util.HashMap<String, Int>? = null
    var listCatalog: MutableList<ItemCatalog> = arrayListOf()
    var toggle_start: ActionBarDrawerToggle? = null


    var viewModel: DataViewModel? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(null)

        viewModel = ViewModelProviders.of(this).get(DataViewModel(application)::class.java)

        setContentView(R.layout.activity_main)

        viewModel?.basketCountLD?.observe(this, Observer {
            showBasketCounter(it)
            setMenuCounter(it, true)
        })

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        viewModel?.errorInternet?.observe(this, Observer {

            if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED))
                if (it)
                    supportFragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.main_container, InfoNoInternetFragment(), "").addToBackStack(null)
                        .commit()

        })

        FacebookSdk.sdkInitialize(applicationContext)
        paperTypeList = Paper.book().read(PAPER_LIST_TYPE_PRODUCT, true)
        paperLang = Paper.book().read(PAPER_LANG_DEFAULT, LANG_RU)

        val toolbar: Toolbar = findViewById(R.id.toolbar_main)
        toolbar.title = ""
        setSupportActionBar(toolbar)

        toolbar.setLogo(R.drawable.vmiske_logo_red)
        toolbar.setTitleTextColor(resources.getColor(R.color.colorMainRed))

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        showMenuButtons = true
        invalidateOptionsMenu()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR//  set status text dark
        } else {
            window.statusBarColor = resources.getColor(R.color.colorMainRed)// set status background whit
        }

        //important for back after splash
        window.decorView.setBackgroundColor(resources.getColor(R.color.colorWhite))

        fab.setOnClickListener {

            var builder = AlertDialog.Builder(this)
            builder.setPositiveButton(getString(R.string.cancel)) { _: DialogInterface, _: Int -> }
            val listNumber = arrayOf<String>("+38 044 390 4100", "+38 067 006 5511")
            builder.setTitle(getString(R.string.select_number))
                .setItems(
                    listNumber, object : DialogInterface.OnClickListener {
                        override fun onClick(p0: DialogInterface?, number: Int) {
                            when (number) {
                                0 -> {
                                    makeCall(true)
                                }
                                1 -> {
                                    makeCall(false)
                                }
                            }
                        }
                    })

            val dialog = builder.create()
            dialog.show()
            // makeCall()
        }
        fab.hide()

        toggle_start = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle_start!!)
        toggle_start!!.syncState()

        toggle_start!!.isDrawerIndicatorEnabled = true
        nav_view.setNavigationItemSelectedListener(this)
        nav_sort.setNavigationItemSelectedListener(this)


        drawer_layout.addDrawerListener(
            object : DrawerLayout.DrawerListener {
                override fun onDrawerStateChanged(newState: Int) {

                }

                override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

                }

                override fun onDrawerClosed(drawerView: View) {

                }

                override fun onDrawerOpened(drawerView: View) {
                    hideKeyboard()
                    getProfileName()

                    setMenuCounter(countList(viewModel?.basketList ?: arrayListOf()), true)
                }
            }
        )

        userLogged = Paper.book().read(PAPER_USER_LOGGED, false)
        userName = Paper.book().read(PAPER_USER_NAME, "")
        showUserMenu(userLogged, userName)

        var fragment = CatalogMainFragment()
        fragment.setShowHideAdver(true)

        supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
            .replace(R.id.main_container, fragment, CATALOG_FRAGMENT)
            .commit()


        val headerview = nav_view.getHeaderView(0)
        var menuLogin = headerview.findViewById<TextView>(R.id.menu_login_drawer)


        menuLogin.setOnClickListener {
            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            replaceFragmentNobackstack(
                LoginPagerFragment(true),
                LOGIN_PAGER_FRAGMENT,
                false,
                getString(R.string.toolbar_name_authorization)
            )
            true
        }
        var menuRegister = headerview.findViewById<TextView>(R.id.menu_register_drawer)
        menuRegister.setOnClickListener {
            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            replaceFragmentNobackstack(
                LoginPagerFragment(false),
                LOGIN_PAGER_FRAGMENT,
                false,
                getString(R.string.toolbar_name_authorization)
            )
            true
        }
        nav_call_menu.setOnClickListener {
            drawer_layout.closeDrawer(GravityCompat.START)
            fab.callOnClick()
        }
        //----
        getBasketFromServer(false, ({}))

        viewModel?.basketCountLD?.observe(this, Observer {
            showBasketCounter(it)
            setMenuCounter(it, true)
        })


    }

    fun makeCall(type: Boolean) {
        val intent = Intent(Intent.ACTION_DIAL)
        /*
        +38 (044) 390 41 00
        +38 (067) 006 55 11
         */if (type)
            intent.data = Uri.parse("tel:+380443904100")
        else
            intent.data = Uri.parse("tel:+380670065511")
        startActivity(intent)

    }

    fun getBasketFromServer(addFlag: Boolean, callback: () -> Unit?) {
        //read basket from server
        var token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
            val lang = Paper.book().read(PAPER_LANG_DEFAULT, LANG_RU)

            viewModel?.apiGetBasket(token, lang)?.observe(this, Observer { response ->
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {
                        //data.answer?.let { baskeList ->
                        data.answer?.let { serverBaskeList ->


                            var localBasket = viewModel?.basketList ?: arrayListOf()

                            if (!addFlag) viewModel?.basketList?.clear()

                            var globalFound = false
                            for (itemLocal in localBasket) {
                                var foundOnserverFlag = false

                                for (itemServer in serverBaskeList) {
                                    if (itemLocal.productId == itemServer?.productId)

                                        if (itemLocal.basketItemInfo == null) {
                                            foundOnserverFlag = true
                                            globalFound = true
                                            //inc quantity on server
                                            updateBasket(
                                                token,
                                                itemServer.baskId ?: 0,
                                                itemLocal.quantity + (itemServer.count ?: 0)
                                            )

                                        } else
                                            if (itemLocal.basketItemInfo?.optionValue == itemServer.opt?.value) {
                                                foundOnserverFlag = true
                                                globalFound = true
                                                //inc quantity on server
                                                updateBasket(
                                                    token,
                                                    itemServer.baskId ?: 0,
                                                    itemLocal.quantity + (itemServer.count ?: 0)
                                                )
                                            }
                                }
                                if (!foundOnserverFlag) {
                                    //add to server
                                    var count = itemLocal.quantity.toString()
                                    addBasket(
                                        token,
                                        itemLocal.productId.toString(),
                                        itemLocal.basketItemInfo ?: BasketItemInfo(), count
                                    )
                                    globalFound = true
                                }
                            }

                            if (globalFound) {
                                var handler = Handler()
                                handler.postDelayed({
                                    getBasketFromServer(false, callback)
                                }, 1000)
                            } else {
                                for (itemServer in serverBaskeList) {

                                    var newItem: ItemProductCatalog? = null

                                    newItem = ItemProductCatalog(itemServer?.name ?: "", 0)
                                    newItem.quantity = itemServer?.count ?: 0
                                    newItem.productId = itemServer?.productId ?: 0
                                    newItem.price = (itemServer?.price ?: 0).toString()
                                    newItem.imageUrl = itemServer?.image ?: ""
                                    newItem.discount = itemServer?.discount ?: Discount()
                                    newItem.basketId = itemServer?.baskId ?: 0

                                    if (itemServer?.opt?.value != null && itemServer.opt?.value != "") {
                                        val newOption = BasketItemInfo()
                                        val optPrice = itemServer.opt?.price ?: 0
                                        newOption.optionPrice = optPrice
                                        //  newItem.price = optPrice.toString()
                                        newOption.optionValue = itemServer.opt?.value ?: ""
                                        newOption.optionDescription = itemServer.opt?.name ?: ""
                                        newItem.basketItemInfo = newOption
                                    }

                                    //if (!addFlag) {
                                    viewModel?.basketItemLD?.value = newItem
                                }

                                callback()
                            }
                        }

                    } else
                        toast(data?.message.toString())


                } else
                    this.toast(response?.message().toString())
            })
        }

    }

    override fun setNewFilterData(
        newData: Response<ListProductResponse>,
        categoryId: String,
        categoryName: String
    ) {

        newFiltersListData = newData
        selectedCategoryId = categoryId
        selectedCategoryName = categoryName

    }

    var newFiltersListData: Response<ListProductResponse>? = null
    var selectedCategoryId: String? = null
    var selectedCategoryName: String? = null
    var catalogProductFragment: CatalogProductFragment? = null


    override fun showSortDrawer(show: Boolean) {

        filter_scroll.visibility = View.GONE
        sort_scroll.visibility = View.VISIBLE
        var view = sort_scroll
        var radioGroup = menu_replace.findViewById<RadioGroup>(R.id.sort_select_group)

        if (show) {
            if (!drawer_layout.isDrawerOpen(GravityCompat.END))
                drawer_layout.openDrawer(GravityCompat.END)
        } else {
            if (drawer_layout.isDrawerOpen(GravityCompat.END))
                drawer_layout.closeDrawer(GravityCompat.END)
        }

        sort_select_group.setOnCheckedChangeListener(null)
        sort_select_group.clearCheck()

        if (getRadioId() == null)
            sort_select_group.check(sort_select_1_default.id)
        else {
            sort_select_group.check(getRadioId()!!)
        }
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            setRadioId(checkedId)
            when (checkedId) {
                R.id.sort_select_1_default -> {
                    viewModel?.apiGetProductsListByCategory(paperLang, selectedCategoryId ?: "")
                        ?.observe(this, Observer { response ->
                            setNewProductListAfterSortFilter(response)
                            drawer_layout.closeDrawer(GravityCompat.END)
                        })
                }
                R.id.sort_select_2_cheap -> {
                    viewModel?.apiGetProductsListByPrice(paperLang, selectedCategoryId ?: "", "asc")
                        ?.observe(this, Observer { response ->

                            setNewProductListAfterSortFilter(response)
                            drawer_layout.closeDrawer(GravityCompat.END)
                        })
                }
                R.id.sort_select_3_exp -> {
                    viewModel?.apiGetProductsListByPrice(
                        paperLang,
                        selectedCategoryId ?: "",
                        "desc"
                    )
                        ?.observe(this, Observer { response ->
                            setNewProductListAfterSortFilter(response)
                            drawer_layout.closeDrawer(GravityCompat.END)
                        })
                }
                R.id.sort_select_4_alfa -> {
                    viewModel?.apiGetProductsListByName(paperLang, selectedCategoryId ?: "", "asc")
                        ?.observe(this, Observer { response ->
                            setNewProductListAfterSortFilter(response)
                            drawer_layout.closeDrawer(GravityCompat.END)
                        })
                }
                R.id.sort_select_5_zet -> {
                    viewModel?.apiGetProductsListByName(paperLang, selectedCategoryId ?: "", "desc")
                        ?.observe(this, Observer { response ->
                            setNewProductListAfterSortFilter(response)
                            drawer_layout.closeDrawer(GravityCompat.END)
                        })
                }
            }
        }
        view.sort_close_label_button.setOnClickListener {
            drawer_layout.closeDrawer(GravityCompat.END)
        }
    }

    private fun setNewProductListAfterSortFilter(response: Response<ListProductResponse>?) {

        response?.let {

            if (catalogProductFragment == null) {
                catalogProductFragment =
                    CatalogProductFragment.newInstance(selectedCategoryName ?: "")
            }
            catalogProductFragment?.let { fragment ->
                fragment.updateList(response = response)

                supportFragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.main_container, fragment, CATALOG_PRODUCT_FRAGMENT)
                    // .addToBackStack(null)
                    .commit()
            }
        }
    }

    var filterCheckBoxList: ArrayList<AppCompatCheckBox> = arrayListOf()
    var filterUser: ArrayList<Int> = arrayListOf()


    var filterApplyFlag = false
    override fun showFilterDrawer(show: Boolean) {

        if (!filterApplyFlag)
            for (checbox in filterCheckBoxList)
                checbox.isChecked = false

        if (show) {
            if (!drawer_layout.isDrawerOpen(GravityCompat.END))
                drawer_layout.openDrawer(GravityCompat.END)
        } else {
            if (drawer_layout.isDrawerOpen(GravityCompat.END))
                drawer_layout.closeDrawer(GravityCompat.END)
            return
        }
        filter_scroll.visibility = View.VISIBLE
        sort_scroll.visibility = View.GONE
        val viewDynamic = filter_dynamic_frame
        filter_cancel_label_button.setOnClickListener {
            filterApplyFlag = true
            for (checbox in filterCheckBoxList)
                checbox.isChecked = false
            //clear filters
            filterUser.clear()
            return@setOnClickListener
        }
        filter_apply_label_button.setOnClickListener {
            filterApplyFlag = true
            newFiltersListData = null
            val filterSize = filterUser.size
            if (filterSize > 0) {
                var filterStr = ""
                for (index in 0 until filterSize) {
                    if (index != filterSize - 1)
                        filterStr += filterUser[index].toString() + ","
                    else {
                        filterStr += filterUser[index].toString()
                    }
                }
                viewModel?.apiGetProductsListByFilter(
                    paperLang,
                    selectedCategoryId ?: "",
                    filterStr
                )
                    ?.observe(this, Observer { response ->
                        setNewProductListAfterSortFilter(response)
                    })
            } else {
                //no filter
                viewModel?.apiGetProductsListByFilter(
                    paperLang,
                    selectedCategoryId ?: "",
                    ""
                )
                    ?.observe(this, Observer { response ->
                        setNewProductListAfterSortFilter(response)
                    })


            }
            drawer_layout.closeDrawer(GravityCompat.END)
            return@setOnClickListener
        }



        newFiltersListData?.let {

            if (it.isSuccessful) {
                filterApplyFlag = false
                //save filter local
                //    newFiltersListData=it
                filterCheckBoxList = arrayListOf()
                var sizeFilter = it.body()?.answer?.filters?.size ?: 0
                if (sizeFilter > 0) {
                    it.body()?.answer?.filters?.let { filters ->
                        filter_dynamic_frame.removeAllViews()

                        for (newFilter in filters) {
                            //add caption
                            var viewNewItem = LayoutInflater.from(this)
                                .inflate(
                                    R.layout.product_info_filter_item,
                                    viewDynamic,
                                    false
                                )
                            var caption =
                                viewNewItem.findViewById<TextView>(R.id.filter_item_menu_caption)
                            caption.text = newFilter?.name ?: ""
                            //filter_item_menu_expand_button
                            var imageButton =
                                viewNewItem.findViewById<ImageButton>(R.id.filter_item_menu_expand_button)

                            imageButton.setBackgroundResource(R.drawable.arrow_up_grey)
                            var checkLayout =
                                viewNewItem.findViewById<LinearLayout>(R.id.filter_block_1)

                            checkLayout.visibility = View.VISIBLE
                            imageButton.setOnClickListener {
                                if (checkLayout.visibility == View.VISIBLE) {
                                    checkLayout.visibility = View.GONE
                                    imageButton.setBackgroundResource(R.drawable.arrow_down_grey)
                                } else {
                                    checkLayout.visibility = View.VISIBLE
                                    imageButton.setBackgroundResource(R.drawable.arrow_up_grey)
                                }
                            }
                            caption.setOnClickListener { imageButton.callOnClick() }
                            //-------------------------------
                            //add values -check boxes
                            val sizeValue = newFilter?.value?.size ?: 0
                            if (sizeValue > 0) {
                                filterUser.clear()
                                newFilter?.value?.let { newCheckboxList ->

                                    for (newCheckbox in newCheckboxList) {
                                        val viewNewCheckbox = LayoutInflater.from(this)
                                            .inflate(
                                                R.layout.product_info_filter_item_checkbox,
                                                filter_block_1,
                                                false
                                            )
                                        checkLayout.addView(viewNewCheckbox)

                                        var checkbox =
                                            viewNewCheckbox.findViewById<AppCompatCheckBox>(R.id.filter_item_checkbox_1)
                                        filterCheckBoxList.add(checkbox)
                                        checkbox.text = newCheckbox?.subname ?: ""
                                        checkbox.tag = newCheckbox?.id ?: 0
                                        checkbox.setOnCheckedChangeListener { compoundButton: CompoundButton, check: Boolean ->
                                            if (check) {
                                                compoundButton.tag?.let {
                                                    if (it is Int) {
                                                        filterUser.add(it)
                                                    }
                                                }
                                            } else {
                                                compoundButton.tag?.let {
                                                    if (it is Int) {
                                                        filterUser.remove(it)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //-----------------------------
                            viewDynamic.addView(viewNewItem)
                        }
                    }
                }
                drawer_layout.openDrawer(GravityCompat.END)
                newFiltersListData = null
            }
        }
    }


    override fun showUserEditProfileMenu(showEditProfile: Boolean) {
        showMenuEditProfileButton = showEditProfile
        invalidateOptionsMenu()
    }


    override fun showUserMenu(showUserMenu: Boolean, userName: String) {

        val headerview = nav_view.getHeaderView(0)
        var menuLogin = headerview.findViewById<TextView>(R.id.menu_login_drawer)
        var menuRegister = headerview.findViewById<TextView>(R.id.menu_register_drawer)
        var menuUserName = headerview.findViewById<TextView>(R.id.menu_user_name_drawer)
        var menuUserImage = headerview.findViewById<ImageButton>(R.id.menu_user_image_drawer)
        menuUserImage.setOnClickListener {

            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {

                drawer_layout.closeDrawer(GravityCompat.START)
            }

            showUserEditProfileMenu(true)
            replaceFragment(
                ProfileFragment(),
                PROFILE_FRAGMENT,
                false,
                getString(R.string.toolbar_name_personal_data)
            )

        }
        menuUserName.setOnClickListener { menuUserImage.callOnClick() }
        if (userName != "")
            menuUserName.text = userName


        val nav_Menu: Menu = nav_view.menu
        if (showUserMenu) {
            nav_Menu.findItem(R.id.nav_bonus).isVisible = true
            nav_Menu.findItem(R.id.nav_shopping).isVisible = true

            menuLogin.visibility = View.GONE
            menuRegister.visibility = View.GONE
            menuUserName.visibility = View.VISIBLE
            menuUserImage.visibility = View.VISIBLE
        } else {
            nav_Menu.findItem(R.id.nav_bonus).isVisible = false
            nav_Menu.findItem(R.id.nav_shopping).isVisible = false

            menuLogin.visibility = View.VISIBLE
            menuRegister.visibility = View.VISIBLE
            menuUserName.visibility = View.GONE
            menuUserImage.visibility = View.GONE
        }
    }

    var userLogged = false
    var userName = ""

    override fun onBasketShowEmpty() {
        replaceFragment(
            BasketProductEmptyFragment(),
            BASKET_EMPTY_FRAGMENT,
            false,
            getString(R.string.toolbar_name_basket)
        )
    }


    var basketProductFragment: BasketProductFragment? = null

    override fun onBasketDeleteClick(item: ItemProductCatalog?) {
        item?.let {
        }
    }

    override fun onBasketListClick(item: ItemProductCatalog?) {
    }

    override fun onProductReviewClick(item: ItemProductReview?) {
    }

    override fun showCustomAppBar(text: String, showBar: Boolean, showMenuButton: Boolean) {
        if (text == "") {
            toolbar_main.title = ""
            toolbar_main.setLogo(R.drawable.vmiske_logo_red)
        } else {
            toolbar_main.title = text
            toolbar_main.logo = null
        }

        if (showBar) {
            supportActionBar?.hide()

        } else {
            supportActionBar?.show()

            toolbar_main.setTitleTextColor(resources.getColor(R.color.colorMainRed))
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }

        if (showMenuButton) {
            showMenuButtons = true
            invalidateOptionsMenu()
        } else {
            showMenuButtons = false
            invalidateOptionsMenu()
        }
    }


    var productIdSubCategory = ""
    var productNameSubCategory = ""

    override fun onMainListInteraction(item: ItemCatalog?) {
        item?.let {
            if (it.subCategoryCount != null)
                productNameSubCategory = it.name
            if (it.subCategoryCount!! > 0) {

                productIdSubCategory = item.subCategoryId.toString()
                replaceFragment(
                    CatalogSubFragment(item.name, item.subCategoryId.toString()),
                    CATALOG_SUB_FRAGMENT,
                    false,
                    ""
                )
            } else {

                productNameSubCategory = it.name
                productIdSubCategory = item.subCategoryId.toString()
                if (catalogProductFragment == null) {
                    catalogProductFragment = CatalogProductFragment.newInstance(item.name)
                }

                catalogProductFragment?.setCaption(productNameSubCategory)
                replaceFragment(
                    catalogProductFragment ?: CatalogProductFragment.newInstance(
                        item.name
                    ),
                    CATALOG_PRODUCT_FRAGMENT,
                    false,
                    " "
                )
            }
        }
    }

    override fun onSubListClick(item: ItemCatalog?) {

        item?.let {
            if (it.subCategoryCount != null)
                productNameSubCategory = it.name

            if (it.subCategoryCount!! > 0) {

                productIdSubCategory = item.subCategoryId.toString()

                replaceFragment(
                    CatalogSubFragment(item.name, item.subCategoryId.toString()),
                    CATALOG_SUB_FRAGMENT,
                    false,
                    ""
                )
            } else {
                //no subcategory
                productIdSubCategory = item.subCategoryId.toString()

                var newName = item.name
                var newName1 = newName.replace("&quot;", "")
                var newName2 = newName1.replace("&amp;", "&")

                catalogProductFragment = CatalogProductFragment.newInstance(newName2)
                replaceFragment(
                    catalogProductFragment!!
                    ,
                    CATALOG_PRODUCT_FRAGMENT,
                    false,
                    " "
                )
            }
        }
    }

    override fun onProductListClick(item: ItemProductCatalog?) {

        item?.let {
            var fragment = CatalogProductInfoPagerFragment(item.SubCategory, item)
            replaceFragment(fragment, CATALOG_PRODUCT_PAGER_INFO, false, item.SubCategory)
        }
    }


    fun hamburgerShow(show: Boolean) {
        //, drawerMode: Int
        if (!show) {
            // [Undocumented?] trick to get up button icon to show
            toggle_start?.isDrawerIndicatorEnabled = false
            //show back
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            toolbar_main.setNavigationIcon(R.drawable.left_arrow_red)
            toolbar_main.setNavigationOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    onBackPressed()
                }
            }
            )
        } else {
            toggle_start?.isDrawerIndicatorEnabled = true
            toolbar_main.setNavigationOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    drawer_layout.openDrawer(GravityCompat.START)
                }
            }
            )
        }
    }


    fun fabHide() {
        fab.hide()
    }

    fun fabShow() {
        fab.show()
    }

    override fun replaceFragment(
        fragment: Fragment,
        fragmentTag: String,
        showLogo: Boolean,
        titleText: String
    ) {

        hideKeyboard()
        fab.hide()

        if (showLogo) {
            toolbar_main.title = ""
            toolbar_main.setLogo(R.drawable.vmiske_logo_red)

        } else {
            toolbar_main.title = titleText
            toolbar_main.logo = null
        }

        supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
            .replace(R.id.main_container, fragment, fragmentTag)
            .addToBackStack(fragmentTag)
            .commit()
    }

    fun replaceFragmentNobackstack(
        fragment: Fragment,
        fragmentTag: String,
        showLogo: Boolean,
        titleText: String
    ) {

        hideKeyboard()
        fab.hide()

        if (showLogo) {
            toolbar_main.title = ""
            toolbar_main.setLogo(R.drawable.vmiske_logo_red)

        } else {
            toolbar_main.title = titleText
            toolbar_main.logo = null
        }

        supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
            .replace(R.id.main_container, fragment, fragmentTag)
            .addToBackStack(null)
            .commit()
    }

    fun setMenuCounter(count: Int, visible: Boolean) {
        //drawer_counter
        var counterMenu = nav_view.menu
        var basket = counterMenu.findItem(R.id.nav_basket)
            .actionView.findViewById<TextView>(R.id.nav_menu_counter_basket)
        basket?.let {
            it.text = count.toString()
        }
    }

    override fun onBackPressed() {

        if (!(this as Activity).isFinishing) {
            hideKeyboard()
            getBasketFromServer(false, ({}))
        }
        if (drawer_layout.isDrawerOpen(GravityCompat.END)) {
            drawer_layout.closeDrawer(GravityCompat.END)
        } else {
            val count = supportFragmentManager.backStackEntryCount

            if (count == 0) {
                super.onBackPressed()
                //additional code
            } else {
                supportFragmentManager.popBackStack()

            }
        }
    }


    fun getPrevFragment(): Fragment? {
        supportFragmentManager.run {
            return when (backStackEntryCount) {
                0 -> null
                else -> {
                    var pos = backStackEntryCount - 2
                    if (pos > 0) {
                        var frag = findFragmentByTag(getBackStackEntryAt(pos).name)
                        return frag
                    }
                    return null
                }
            }
        }
    }


    fun hideKeyboard() {
        val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        currentFocus?.let {
            inputManager.hideSoftInputFromWindow(it.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }


    var menuGlobal: Menu? = null

    override fun onCreateOptionsMenu(menu: Menu): Boolean {


        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        val actionMenuFind = menu.findItem(R.id.action_search)
        actionMenuFind?.setOnMenuItemClickListener {

            toolbar_main.logo = null
            showCustomAppBar("", true, false)
            replaceFragment(SearchFragment(), SEARCH_FRAGMENT, false, "")
            true
        }

        val actionMenuBasket = menu.findItem(R.id.action_shop_basket)
        actionMenuBasket?.setOnMenuItemClickListener {
            showBasket(false)
            true
        }

        val actionMenuEditProfile = menu.findItem(R.id.action_profile_edit)
        actionMenuEditProfile?.setOnMenuItemClickListener {
            replaceFragment(ProfileEditFragment(), PROFILE_EDIT_FRAGMENT, false, "")

            true
        }

        if (showMenuButtons) {
            for (i in 0 until menu.size()) {
                menu.getItem(i).isVisible = true
                if (menu.getItem(i).itemId == R.id.action_profile_edit) menu.getItem(i).isVisible =
                    false
            }
        } else {
            for (i in 0 until menu.size())
                menu.getItem(i).isVisible = false
        }

        if (!showMenuButtons)
            if (showMenuEditProfileButton) {
                for (i in 0 until menu.size()) {
                    menu.getItem(i).isVisible = false
                    if (menu.getItem(i).itemId == R.id.action_profile_edit) menu.getItem(i)
                        .isVisible = true
                }
            }
        menuGlobal = menu

        setCount(this, globalCounterBasket.toString())

        return true
    }

    var globalCounterBasket = 0
    fun showBasketCounter(number: Int) {
        globalCounterBasket = number
        invalidateOptionsMenu()
    }

    var showMenuButtons = true
    var showMenuEditProfileButton = false

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.

        val handler = Handler()
        handler.postDelayed({
            // do something after 1000ms
            drawer_layout.closeDrawer(GravityCompat.START)
        }, 100)

        when (item.itemId) {

            R.id.nav_main -> {
                showCatalog(true)

            }
            R.id.nav_catalog -> {
                showCatalog(false)

            }
            R.id.nav_basket -> {
                showBasket(false)
            }
            R.id.nav_shopping -> {
                replaceFragment(
                    ShoppingListFragment(),
                    SHOPPING_FRAGMENT,
                    false,
                    getString(R.string.toolbar_name_shopping)
                )
            }
            R.id.nav_bonus -> {
                replaceFragment(
                    BonusFragment(),
                    BONUS_FRAGMENT,
                    false,
                    getString(R.string.toolbar_name_bonuses)
                )
            }

            R.id.nav_information -> {
                replaceFragment(
                    InfoListFragment(),
                    INFOLIST_FRAGMENT,
                    false,
                    getString(R.string.toolbar_name_information)
                )
            }
            R.id.nav_setup -> {
                replaceFragment(
                    SetupFragment(),
                    SETUP_FRAGMENT,
                    false,
                    getString(R.string.toolbar_name_promo_setup)
                )

            }
        }
        return true
    }


    override fun showCatalog(showAdver: Boolean) {

        if (showAdver) {
            var fragment = CatalogMainFragment()
            fragment.setList(listCatalog)
            fragment.setShowHideAdver(true)
            replaceFragment(fragment, CATALOG_FRAGMENT, true, "")
        } else {
            var fragment = CatalogMainFragment()
            fragment.setList(listCatalog)
            fragment.setShowHideAdver(false)
            replaceFragment(fragment, CATALOG_FRAGMENT, true, "")
        }
    }

    fun showBasket(checkOutFlag: Boolean) {

        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        basketProductFragment = BasketProductFragment()
        basketProductFragment?.setCheckoutFlag(checkOutFlag)

        replaceFragment(
            basketProductFragment as Fragment,
            BASKET_FRAGMENT,
            false,
            getString(R.string.toolbar_name_basket)
        )
    }


    fun setCount(context: Context, count: String) {
        val menuItem = menuGlobal?.findItem(R.id.action_shop_basket)
        //top right basket
        menuItem?.let {
            val icon = menuItem.icon as LayerDrawable
            val badge: CountDrawable
            // Reuse drawable if possible
            val reuse = icon.findDrawableByLayerId(R.id.ic_group_count)
            badge = CountDrawable(context)

            badge.setCount(count)
            icon.mutate()
            icon.setDrawableByLayerId(R.id.ic_group_count, badge)
        }
    }


    fun setResourcesByLocale(lang: String) {
        val configuration = Configuration(resources.configuration)
        configuration.setLocale(Locale(lang))
        resources.updateConfiguration(configuration, null)
    }


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(applySelectedAppLanguage(base))

    }

    private fun applySelectedAppLanguage(context: Context): Context {

        var lang = Paper.book().read<String>(PAPER_LANG_DEFAULT, LANG_RU)
        val newLocale = Locale(lang)
        val newConfig = Configuration(context.resources.configuration)
        Locale.setDefault(newLocale)
        newConfig.setLocale(newLocale)
        return context.createConfigurationContext(newConfig)
    }

    fun clearBasket() {
        viewModel?.basketList?.clear()
        viewModel?.basketCountLD?.value = 0

    }

    fun updateBasket(token: String, basketId: Int, quantity: Int) {
        viewModel?.apiUpdateBasket(token, basketId, quantity)?.observe(this, Observer { response ->
            if (response.isSuccessful) {
                val data = response.body()
                if (data?.error == false) {
                    if (data.answer == true) {
                        //toast(getString(R.string.pro))
                        toast("Количество изменилось")
                    }
                } else
                    toast(data?.message.toString())
            } else
                toast(response?.message().toString())
        })
    }

    fun addBasket(
        token: String,
        productId: String,
        basketItemInfo: BasketItemInfo,
        quantity: String
    ) {
        viewModel?.apiAddBasket(
            token, quantity, productId,
            basketItemInfo.povId.toString(), basketItemInfo.poId.toString(),
            basketItemInfo.optionValue, basketItemInfo.optionDescription,
            basketItemInfo.optionPrice?.toString()
        )?.observe(this, Observer { response ->
            if (response.isSuccessful) {
                val data = response.body()
                if (data?.error == false) {
                    if (data.answer == true) {
                        toast(getString(R.string.product_added_basket))

                    } else {
                        toast(getString(R.string.product_not_add_bsaket))
                    }
                } else
                    toast(data?.message.toString())
            } else
                toast(response?.message().toString())

        })
    }


}

fun countList(list: MutableList<ItemProductCatalog>): Int {

    return list.size
}


    fun MainActivity.toastMain(message: CharSequence) {


        if (!(this as Activity).isFinishing) {
            val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
            val view = toast.view
            view?.background
                ?.setColorFilter(
                    resources.getColor(R.color.colorMainRedToast),
                    PorterDuff.Mode.SRC_IN
                )
            toast.show()
        }


    }


