package noty.team.miska.bonus


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_bonus.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel


class BonusFragment : Fragment() {
    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_bonus, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_bonuses),
            false, true
        )
        getProfile()
    }

    fun getProfile() {

        var token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
            viewModel.apiGetProfile(
                token
            ).observe(this, Observer { response ->
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {

                        if (data.answer?.bonus == null)
                            bonus_value_data.text = "0"
                        else
                            bonus_value_data.text = data.answer?.bonus.toString()


                        if (data.answer?.status == null)
                            bonus_status_data.text = ""
                        else
                            bonus_status_data.text = data.answer?.status.toString()
                    } else
                        context?.toast(data?.message.toString())
                } else
                    context?.toast(response?.message().toString())
            })
        }
    }

}
