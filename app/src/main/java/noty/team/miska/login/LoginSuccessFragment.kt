package noty.team.miska.login


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_login_success.*
import noty.team.miska.CATALOG_FRAGMENT
import noty.team.miska.MainActivity
import noty.team.miska.PAPER_USER_LOGGED
import noty.team.miska.R
import noty.team.miska.catalog.CatalogMainFragment


class LoginSuccessFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_login_success, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_authorization), true,
            false
        )

        Paper.book().write(PAPER_USER_LOGGED, true)

        register_success_start_shopping_button.setOnClickListener {
            (activity as MainActivity).replaceFragment(
                CatalogMainFragment(),
                CATALOG_FRAGMENT, true, ""
            )
        }

        login_success_arrow_back.setOnClickListener {
            (activity as MainActivity).replaceFragment(
                CatalogMainFragment(),
                CATALOG_FRAGMENT, true, ""
            )
        }
    }
}
