package noty.team.miska.login


import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_login.*
import noty.team.miska.*
import noty.team.miska.catalog.CatalogMainFragment
import noty.team.miska.model.UserProfile
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import noty.team.miska.viewmodel.LANG_RU
import org.json.JSONObject
import java.util.regex.Pattern

const val LOGIN_FRAGMENT = "LOGIN_FRAGMENT"
const val LOGIN_FORGOT_SEND_FRAGMENT = "LOGIN_FORGOT_SEND_FRAGMENT"
const val LOGIN_FORGOT_CHANGE_FRAGMENT = "LOGIN_FORGOT_CHANGE_FRAGMENT"
const val LOGIN_FORGOT_NEWPASS_FRAGMENT = "LOGIN_FORGOT_NEWPASS_FRAGMENT"
const val LOGIN_FORGOT_FRAGMENT = "LOGIN_FORGOT_FRAGMENT"
const val CHECKOUT_PICKUP_FRAGMENT = "CHECKOUT_PICKUP_FRAGMENT"
const val CHECKOUT_CASE_COURIER_FRAGMENT = "CHECKOUT_CASE_COURIER_FRAGMENT"
const val CHECKOUT_CASE_NOVAPOSHTA_FRAGMENT = "CHECKOUT_CASE_NOVAPOSHTA_FRAGMENT"
const val CHECKOUT_CASE_PICKUP_FRAGMENT = "CHECKOUT_CASE_PICKUP_FRAGMENT"
const val CHECKOUT_ERROR_FRAGMENT = "CHECKOUT_ERROR_FRAGMENT"
const val SHOPPING_EMPTY_FRAGMENT = "SHOPPING_EMPTY_FRAGMENT"
const val SHOPPING_AMBAR_LIST_FRAGMENT = "SHOPPING_AMBAR_LIST_FRAGMENT"
const val CHECKOUT_THANKYOU_FRAGMENT = "CHECKOUT_THANKYOU_FRAGMENT"
const val RC_SIGN_IN = 15236
const val ID_GOOGLE = "glId"
const val ID_FACEBOOK = "fbId"

class LoginFragment(val basketCheckout: Boolean) : Fragment() {

    private var paperLang = ""

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onPause() {
        super.onPause()
        login_edit_email.text.clear()
        login_edit_password.text.clear()
    }

    var callbackManager: CallbackManager? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        paperLang = Paper.book().read(PAPER_LANG_DEFAULT, LANG_RU)
        callbackManager = CallbackManager.Factory.create()

        facebookButton.setReadPermissions("public_profile")
        facebookButton.setReadPermissions("email")
        // If using in a fragment
        facebookButton.fragment = this

        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    loginFacebookUser()
                }

                override fun onCancel() {
                    progress_login.visibility = View.GONE
                    context?.toast(getString(R.string.message_facebook_cancel))
                }

                override fun onError(error: FacebookException?) {
                    progress_login.visibility = View.GONE
                    context?.toast(getString(R.string.message_facebook_error))
                }
            }
        )

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_authorization),
            false, true
        )
        login_enter_button.setOnClickListener {
            //progress_login.visibility=View.VISIBLE
            loginUser()
            true
        }

        login_facebook_button.setOnClickListener {
            progress_login.visibility = View.VISIBLE
            //startSuccess()
            if (AccessToken.getCurrentAccessToken() != null) {
                loginFacebookUser()
            } else {
                facebookButton.performClick()
            }
            true
        }
        login_google_button.setOnClickListener {
            //  startSuccess()
            progress_login.visibility = View.VISIBLE
            loginGoogleUser()
            true
        }

        login_forgot_caption.setOnClickListener {
            (activity as MainActivity).replaceFragment(
                LoginForgotFragment(),
                LOGIN_FORGOT_SEND_FRAGMENT,
                false,
                getString(R.string.toolbar_name_restore_password)

            )
        }
    }

    fun loginFacebookUser() {

        var request: GraphRequest =
            GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                object : GraphRequest.GraphJSONObjectCallback {
                    override fun onCompleted(email: JSONObject?, response: GraphResponse?) {
                        try {
                            accountFacebookName = email?.getString("name")
                            fbId = email?.getString("id")
                            loginToMiska(fbId, email?.getString("email"), ID_FACEBOOK)
                        } catch (e: Exception) {
                            context?.toast(e.localizedMessage.toString())
                        }
                    }
                })

        val parameters = Bundle()
        parameters.putString("fields", "id,name,email,gender, birthday");
        request.parameters = parameters
        request.executeAsync()

    }

    fun loginGoogleUser() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        val mGoogleSignInClient = GoogleSignIn.getClient(context!!, gso)
        val signInIntent = mGoogleSignInClient.getSignInIntent()
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    var accountGoogle: GoogleSignInAccount? = null
    var accountFacebookName: String? = null
    var fbId: String? = ""

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        progress_login.visibility = View.GONE
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                accountGoogle = task.getResult(ApiException::class.java)
                (activity as MainActivity).setGoogleAccount(accountGoogle)
                val googleId = accountGoogle?.id
                loginToMiska(googleId, accountGoogle?.email, ID_GOOGLE)
                // Signed in successfully, show authenticated UI.
                //updateUI(accountGoogle)
            } catch (e: ApiException) {
                // The ApiException status code indicates the detailed failure reason.
                // Please refer to the GoogleSignInStatusCodes class reference for more information.
                Log.w(TAG, "signInResult:failed code=" + e.statusCode)
                //updateUI(null)
                // context?.toast(e.localizedMessage.toString())
            }
        }
    }

    fun loginToMiska(googleId: String?, email: String?, loginType: String?) {

        if (googleId != null)
            viewModel.apiLoginNetworks(
                googleId,
                loginType, email
            ).observe(this, Observer { response ->
                progress_login.visibility = View.GONE

                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {
                        //login ok
                        val token = data.answer?.token
                        if (token != null) {
                            context?.toast(getString(R.string.message_success_network))
                            Paper.book().write(PAPER_TOKEN, token)
                            Paper.book().write(PAPER_USER_LOGGED, true)
                            //show name in menu
                            accountGoogle?.let {
                                (activity as MainActivity).showUserMenu(
                                    true,
                                    it.givenName.toString()
                                )
                            }
                            accountFacebookName?.let {
                                (activity as MainActivity).showUserMenu(
                                    true,
                                    it
                                )
                            }
                            if (basketCheckout) {
                                startBasket()
                            } else {
                                (activity as MainActivity).getBasketFromServer(true,({}))
                                startCatalog()
                            }

                        } else {
                            //send to registration
                            accountGoogle?.let {
                                login_edit_email.setText(it.email.toString())
                            }
                        }
                    } else
                        context?.toast(getString(R.string.message_facebook_notregister))
                } else
                    context?.toast(response?.message().toString())
            })
        else
            context?.toast(getString(R.string.message_login_error))

    }

    fun loginUser() {

        if (login_edit_email.text.trim().toString().isEmpty()) {
            context?.toast(getString(R.string.message_input_email))
            return
        }

        val pattern1 =
            Pattern.compile("^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+")

        val matcher1 = pattern1.matcher(login_edit_email.text)

        if (!matcher1.matches()) {
            context?.toast(getString(R.string.message_input_correct_email))
            return
        }

        if (login_edit_password.text?.trim()?.isEmpty() ?: false) {
            context?.toast(getString(R.string.message_input_password))
            return
        }
        if (login_edit_password.text?.toString()?.length ?: 0 < 4) {
            context?.toast(getString(R.string.message_input_password_length))
            return
        }


        viewModel.apiLoginUser(
            login_edit_email.text.toString(), login_edit_password.text.toString()
        ).observe(this, Observer { responseLogin ->
            // if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED))
            if (responseLogin.isSuccessful) {
                val data = responseLogin.body()
                if (data?.error == false) {
                    var token = data.answer?.token ?: ""
                    Paper.book().write(PAPER_TOKEN, token)
                    Paper.book().write(PAPER_USER_LOGGED, true)

                    //get user name from profile

                    if (token != "") {
                        token = TOKEN_PREFIX + token
                        viewModel.apiGetProfile(
                            token
                        ).observe(this, Observer { response ->

                            if (response.isSuccessful) {
                                val data = response.body()
                                if (data?.error == false) {

                                    Paper.book()
                                        .write(PAPER_USER_NAME, data.answer?.firstname.toString())
                                    (activity as MainActivity).setProfileData(
                                        UserProfile(
                                            email = data.answer?.email.toString(),
                                            telephone = data.answer?.telephone.toString(),
                                            firstname = data.answer?.firstname.toString(),
                                            lastname = data.answer?.lastname.toString(),
                                            token = token
                                        )
                                    )
                                    (activity as MainActivity).showUserMenu(
                                        true,
                                        data.answer?.firstname.toString()
                                    )
                                    //startCatalog()
                                    if (basketCheckout) {
                                        startBasket()
                                    } else {
                                        loadBasket()
                                        startCatalog()
                                    }
                                } else
                                    context?.toast(getString(R.string.message_login_error))
                            } else
                                context?.toast(response?.message().toString())
                        })
                    }

                    //--------------------------------
                } else {
                    var errorMessage = data?.message.toString()
                    var translate = ""
                    when (errorMessage) {
                        "invalid user email or password" -> {
                            translate = getString(R.string.message_wrong_user_password_email)
                        }
                        else -> {
                            translate = errorMessage
                        }
                    }
                    context?.toast(translate)
                }
            } else
                context?.toast(responseLogin?.message().toString())
        })

    }

    fun startBasket() {
        (activity as MainActivity).showBasket(true)
    }

    fun loadBasket() {
        (activity as MainActivity).getBasketFromServer(true,({}))
    }

    fun startCatalog() {
        (activity as MainActivity).showCustomAppBar("", false, true)

        var fragment = CatalogMainFragment()
        fragment.setShowHideAdver(true)
        (activity as MainActivity).replaceFragment(
            fragment, CATALOG_FRAGMENT,
            false, getString(R.string.toolbar_name_authorization)
        )
    }

    fun startSuccess() {
        (activity as MainActivity).showCustomAppBar("", true, true)
        (activity as MainActivity).replaceFragment(
            LoginSuccessFragment(), LOGIN_FORGOT_FRAGMENT,
            false, getString(R.string.toolbar_name_authorization)
        )

    }
}
