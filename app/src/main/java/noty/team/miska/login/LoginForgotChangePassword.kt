package noty.team.miska.login


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_login_forgot_change_password.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.util.toast


class LoginForgotChangePassword : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login_forgot_change_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.password_restore),
            true,
            false
        )

        forgot_password_repeat_send.setOnClickListener {

            forgot_change_edit_password
            forgot_change_edit_repeat_password

            if (forgot_change_edit_password
                    .text?.trim()?.isEmpty() == true
            ) {
                context?.toast(getString(R.string.message_input_password))
                return@setOnClickListener
            }

            if (forgot_change_edit_password.length() < 4) {
                context?.toast(getString(R.string.password_length_4_20))
                return@setOnClickListener
            }


            if (forgot_change_edit_repeat_password.text?.trim()?.isEmpty() == true) {
                context?.toast(getString(R.string.message_repeat_password))
                return@setOnClickListener
            }

            if (forgot_change_edit_repeat_password.length() < 4) {
                context?.toast(getString(R.string.password_length_4_20))
                return@setOnClickListener
            }

            if ((forgot_change_edit_password.text?.toString() ?: "") !=
                forgot_change_edit_repeat_password.text?.toString() ?: ""
            ) {
                context?.toast(getString(R.string.message_passwords_not_thesame))
                return@setOnClickListener
            }

            (activity as MainActivity).replaceFragment(
                LoginForgotPasswordNewFragment(),
                LOGIN_FORGOT_NEWPASS_FRAGMENT,
                false,
                getString(R.string.password_restore)
            )
        }

        forgot_change_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
    }

}
