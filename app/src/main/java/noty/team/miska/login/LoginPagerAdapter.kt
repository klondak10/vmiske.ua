package noty.team.miska.login

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import noty.team.miska.R
import noty.team.miska.util.InfoNoInternetFragment

private val TAB_TITLES = arrayOf(
    R.string.login_tab_text_1,
    R.string.login_tab_text_2
)


class LoginPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        var fragmnet: Fragment = InfoNoInternetFragment()

        when (position) {
            0 -> fragmnet = LoginFragment(false)
            1 -> fragmnet = LoginRegisterFragment()
        }

        return fragmnet
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return 2
    }
}
