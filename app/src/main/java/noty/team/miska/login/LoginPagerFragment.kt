package noty.team.miska.login


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.fragment_pager_login.*
import noty.team.miska.MainActivity
import noty.team.miska.R


class LoginPagerFragment(loginType: Boolean) : Fragment() {

    var loginTypeFragment = true

    init {
        loginTypeFragment = loginType
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_pager_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_authorization),
            false, true
        )
        login_view_pager.adapter = LoginPagerAdapter(context!!, childFragmentManager)
        login_tabs.setupWithViewPager(login_view_pager)

        if (loginTypeFragment) {
            val tab = login_tabs.getTabAt(0)
            tab?.select()
        } else {
            val tab = login_tabs.getTabAt(1)
            tab?.select()
        }

        login_view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                if (position == 1) {

                }
            }
        })
    }
}
