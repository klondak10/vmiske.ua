package noty.team.miska.login


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_login_forgot_send.*
import noty.team.miska.LOGIN_PAGER_FRAGMENT
import noty.team.miska.MainActivity
import noty.team.miska.R


class LoginForgotSendFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_login_forgot_send, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_authorization),
            true,
            false
        )

        forgot_password_instructions_send.setOnClickListener {

            (activity as MainActivity).replaceFragment(
                LoginPagerFragment(true), LOGIN_PAGER_FRAGMENT,
                false, ""
            )

        }
        login_forgot_send_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
    }
}
