package noty.team.miska.login


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_login_forgot_password_new.*
import noty.team.miska.CATALOG_FRAGMENT
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.catalog.CatalogMainFragment


class LoginForgotPasswordNewFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_login_forgot_password_new, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_password_changed),
            true,
            false
        )
        forgot_new_password_change_send.setOnClickListener {
            (activity as MainActivity).replaceFragment(
                CatalogMainFragment(),
                CATALOG_FRAGMENT, true, ""
            )
        }
        frgot_newpass_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
    }

}
