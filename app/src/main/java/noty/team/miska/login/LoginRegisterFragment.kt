package noty.team.miska.login

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.github.vacxe.phonemask.PhoneMaskManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_login_register.*
import noty.team.miska.MainActivity
import noty.team.miska.PAPER_USER_LOGGED
import noty.team.miska.PAPER_USER_NAME
import noty.team.miska.R
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import org.json.JSONObject
import java.util.regex.Pattern

const val TERM_USE_FRAGMENT = "TERM_USE_FRAGMENT"
const val PAPER_TOKEN = "PAPER_TOKEN"

class LoginRegisterFragment : Fragment() {

    var fbId: String? = null
    var ggId: String? = null
    var userAccount: GoogleSignInAccount? = null
    var callbackManager: CallbackManager? = null
    var account: GoogleSignInAccount? = null


    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login_register, container, false)
    }

    fun loginFacebookUser() {
        val request: GraphRequest =
            GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                object : GraphRequest.GraphJSONObjectCallback {
                    override fun onCompleted(fbAccount: JSONObject?, response: GraphResponse?) {
                        try {
                            register_edit_name.setText(fbAccount?.getString("first_name"))
                            register_edit_surname.setText(fbAccount?.getString("last_name"))
                            register_edit_email.setText(fbAccount?.getString("email"))
                            fbId = fbAccount?.getString("id")
                        } catch (e: Exception) {
                            context?.toast(e.localizedMessage.toString())
                        }
                    }
                })

        val parameters = Bundle()
        parameters.putString("fields", "id,name,email,gender, birthday,first_name,last_name")
        request.parameters = parameters
        request.executeAsync()
    }

    fun registerGoogleUser() {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        val mGoogleSignInClient = GoogleSignIn.getClient(context!!, gso)
        val signInIntent = mGoogleSignInClient.getSignInIntent()
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data)
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                account = task.getResult(ApiException::class.java)
                (activity as MainActivity).setGoogleAccount(account)
                //updateUI(accountGoogle)
                //(activity as MainActivity).setGoogleAccount(account)
                userAccount = account
                fbId = null
                ggId = null
                userAccount?.let {
                    register_edit_name.setText(it.givenName)
                    register_edit_surname.setText(it.familyName)
                    register_edit_email.setText(it.email)
                    ggId = it.id
                }
            } catch (e: ApiException) {
                // The ApiException status code indicates the detailed failure reason.
                // Please refer to the GoogleSignInStatusCodes class reference for more information.
                Log.w(ContentValues.TAG, "signInResult:failed code=" + e.statusCode)
                //updateUI(null)
                context?.toast(e.localizedMessage.toString())
            }
        }
    }

    fun registerFacebookUser() {
        if (AccessToken.getCurrentAccessToken() != null) {
            loginFacebookUser()
        } else {
            facebookRegisterButton.performClick()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        register_edit_name.text.clear()
        register_edit_surname.text.clear()
        register_edit_email.text.clear()
        register_edit_phone.text.clear()
        register_edit_leader_number.text.clear()
        register_edit_password.text?.clear()
        register_edit_repeat_password.text?.clear()

        callbackManager = CallbackManager.Factory.create()

        facebookRegisterButton.setReadPermissions("public_profile")
        facebookRegisterButton.setReadPermissions("email")
        // If using in a fragment
        facebookRegisterButton.fragment = this

        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    //   context?.toast("Facebook логин успешен!")
                    loginFacebookUser()
                }

                override fun onCancel() {
                    context?.toast(getString(R.string.message_facebook_cancel))
                }

                override fun onError(error: FacebookException?) {
                    context?.toast(getString(R.string.message_facebook_cancel))
                }
            }
        )

        register_new_user_button.setOnClickListener {
            registerNewUser()
            true
        }

        register_facebook_button.setOnClickListener {
            registerFacebookUser()
            true
        }
        register_google_button.setOnClickListener {
            registerGoogleUser()
            true
        }

        register_forgot_caption.setOnClickListener {
            (activity as MainActivity).replaceFragment(
                LoginTermsUseFragment(
                    getString(R.string.toolbar_name_terms_use),
                    getString(R.string.info_terms)
                ),
                TERM_USE_FRAGMENT,
                false,
                ""
            )
        }
        register_forgot_caption_next.setOnClickListener {
            (activity as MainActivity).replaceFragment(
                LoginTermsUseFragment(
                    getString(R.string.police_title),
                    getString(R.string.policy_info)
                ),
                TERM_USE_FRAGMENT,
                false,
                ""
            )
        }

        PhoneMaskManager()
            .withMask(getString(R.string.phone_mask))
            .withRegion(getString(R.string.phone_prefix))
            .withValueListener { phone -> System.out.println(phone) }
            .bindTo((view.findViewById(R.id.register_edit_phone) as EditText))

        PhoneMaskManager()
            .withMask(getString(R.string.phone_mask))
            .withRegion(getString(R.string.phone_prefix))
            .withValueListener { phone -> System.out.println(phone) }
            .bindTo((view.findViewById(R.id.register_edit_leader_number) as EditText))

    }

    fun startSuccess() {
        (activity as MainActivity).replaceFragment(
            LoginSuccessFragment(),
            LOGIN_FRAGMENT,
            false,
            getString(R.string.toolbar_name_authorization)
        )
    }

    fun registerNewUser() {
        if (register_edit_name.text.toString().trim().isEmpty()) {
            context?.toast(getString(R.string.message_input_name))
            return
        }
        if (register_edit_surname.text.toString().trim().isEmpty()) {
            context?.toast(getString(R.string.message_input_surname))
            return
        }
        if (register_edit_email.text.toString().trim().isEmpty()) {
            context?.toast(getString(R.string.message_input_email))
            return
        }

        val pattern1 =
            Pattern.compile("^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+")

        val matcher1 = pattern1.matcher(register_edit_email.text)

        if (!matcher1.matches()) {
            context?.toast(getString(R.string.message_input_correct_email))
            return
        }

        if (register_edit_phone.text.toString().trim().isEmpty()) {
            context?.toast(getString(R.string.input_phone))
            return
        }

        if (register_edit_phone.text.toString().length < 19 &&
            register_edit_phone.text.toString().length > 5
        ) {
            context?.toast(getString(R.string.message_input_10_numbers))
            return
        }


        if (register_edit_leader_number.text.toString().isNotEmpty()) {
            if (register_edit_phone.text.toString() == register_edit_leader_number.text.toString()) {
                context?.toast(getString(R.string.message_number_vogak_phone))
                return
            }

            if (register_edit_leader_number.text.toString().length < 19
                && register_edit_leader_number.text.toString().length > 5
            ) {
                context?.toast(getString(R.string.message_input_10_numbers))
                return
            }
        }

        if (register_edit_password.text?.toString()?.trim()?.isEmpty() ?: false) {
            context?.toast(getString(R.string.message_input_password))
            return
        }
        if (register_edit_password.text?.toString()?.length ?: 0 < 4) {
            context?.toast(getString(R.string.message_input_password_length))
            return
        }

        val patternPassword =
            Pattern.compile("[A-ZА-Яа-яa-z0-9_-Ёё]{4,20}\$")

        val matcherPassword = patternPassword.matcher(register_edit_password.text.toString())

        if (!matcherPassword.matches()) {
            context?.toast(getString(R.string.message_password_only_number_letters))
            return
        }

        val matcherRepeatPassword = patternPassword.matcher(register_edit_password.text.toString())
        if (!matcherRepeatPassword.matches()) {
            context?.toast(getString(R.string.message_password_repeat_only_number_letters))
            return
        }

        if (register_edit_repeat_password.text?.toString()?.trim()?.isEmpty() ?: false) {
            context?.toast(getString(R.string.message_repeat_password))
            return
        }

        if (!matcher1.matches()) {
            //show your message if not matches with email pattern
            context?.toast(getString(R.string.message_input_correct_email))
            return
        }

        if (register_edit_repeat_password.text?.toString()?.length ?: 0 < 4) {
            context?.toast(getString(R.string.message_input_password_length))
            return
        }
        if (!(register_edit_repeat_password.text?.toString() ?: "").equals(
                register_edit_password.text?.toString() ?: ""
            )
        ) {
            context?.toast(getString(R.string.message_passwords_not_thesame))
            return
        }

        var leaderNumber: String? = ""
        if (register_edit_leader_number.text.toString().length > 5) {
            leaderNumber = register_edit_leader_number.text.toString()
        }

        viewModel.apiRegisterUser(
            register_edit_name.text.toString(),
            register_edit_surname.text.toString(),
            register_edit_email.text.toString(),
            register_edit_phone.text.toString(),
            register_edit_password.text.toString(),
            leaderNumber, fbId, ggId
        ).observe(this, Observer { response ->

            if (response.isSuccessful) {
                val data = response.body()
                if (data?.error == false) {
                    val token = data.answer?.token ?: ""
                    Paper.book().write(PAPER_TOKEN, token)
                    Paper.book().write(PAPER_USER_LOGGED, true)
                    Paper.book().write(PAPER_USER_NAME, register_edit_name.text.toString())
                    (activity as MainActivity).showUserMenu(
                        true,
                        register_edit_name.text.toString()
                    )
                    startSuccess()
                } else
                    context?.toast(data?.message.toString())
            } else
                context?.toast(response.message().toString())
        })
    }
}


