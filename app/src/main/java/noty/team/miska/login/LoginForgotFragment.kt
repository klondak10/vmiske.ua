package noty.team.miska.login


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_login_forgot.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import java.util.regex.Pattern


class LoginForgotFragment : Fragment() {

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login_forgot, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_restore_password)
            , true, false
        )

        forgot_password_send.setOnClickListener {
            if (forgot_password_email.text.toString().trim().isEmpty()) {
                context?.toast(getString(R.string.message_input_email))
                return@setOnClickListener
            }

            val pattern1 =
                Pattern.compile("^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+")

            val matcher1 = pattern1.matcher(forgot_password_email.text)

            if (!matcher1.matches()) {
                //show your message if not matches with email pattern
                context?.toast(getString(R.string.message_input_correct_email))
                return@setOnClickListener
            }

            viewModel.apiRestorePassword(
                forgot_password_email.text.toString()
            ).observe(this, Observer { response ->

                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error?.email == "") {
                        //login ok
                        val response = data?.success?.title
                        if (response != null) {
                            context?.toast(getString(R.string.message_send_to_email))
                            (activity as MainActivity).replaceFragment(
                                LoginForgotSendFragment(),
                                LOGIN_FORGOT_SEND_FRAGMENT,
                                false,
                                getString(R.string.toolbar_name_restore_password)
                            )
                        }
                    } else {
                        context?.toast(data?.error?.email ?: getString(R.string.message_error_send))
                    }
                }
            }
            )
        }

        login_forgot_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
    }

}
