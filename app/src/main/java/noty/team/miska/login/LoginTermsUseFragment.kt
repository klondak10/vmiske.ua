package noty.team.miska.login


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_login_terms_user.*
import noty.team.miska.MainActivity
import noty.team.miska.R

class LoginTermsUseFragment(val caption: String, val info: String) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_login_terms_user, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).showCustomAppBar(
            caption,
            true,
            false
        )

        term_use_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        review_edit_send_app_bar_caption.text = caption
        login_terms_text.text = info

    }

}
