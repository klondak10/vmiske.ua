package noty.team.miska.viewmodel

import android.app.Application
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import noty.team.miska.model.DeliveryAdress
import noty.team.miska.model.ItemProductCatalog
import noty.team.miska.modelRetrofit.*
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.HttpException
import retrofit2.Response
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.net.URL
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext


const val IMAGE_URL = "https://vmiske.ua/image/"
const val LIMIT_NUMBER = "1"


class DataViewModel(application: Application) : AndroidViewModel(application) {


    var errorInternet: MutableLiveData<Boolean> = MutableLiveData()

    var basketList: MutableList<ItemProductCatalog> = mutableListOf()
    var basketListLD: MutableLiveData<MutableList<ItemProductCatalog>> = MutableLiveData()
    var basketItemLD: MutableLiveData<ItemProductCatalog> = MutableLiveData()
    var basketCountLD: MutableLiveData<Int> = MutableLiveData()
    var promoCode = ""
    var promoCodeAnswer: AnswerPromo? = AnswerPromo()
    var bonusesUsed = 0
    var sumPromoBonus = 0


    val scope: CoroutineScope = object : CoroutineScope {
        override val coroutineContext: CoroutineContext
            get() = Dispatchers.IO  + CoroutineExceptionHandler { _, throwable ->
                //errorInternet.postValue(true)
                mainError.postValue(throwable.localizedMessage)
            }
    }

    var mainCatalogLD = MutableLiveData<CatalogResponse>()
    var mainSubCategoryLD = MutableLiveData<CatalogResponse>()
    var mainBannersLD = MutableLiveData<BannerResponse>()
    var mainError = MutableLiveData<String>()
    val serverApi: ApiRetrofit = ApiRetrofit.create()
    var serverApi2: ApiRetrofit? = null

    var mDisposable: CompositeDisposable = CompositeDisposable()
    var deliveryType: DeliveryAdress = DeliveryAdress()

    var listPositionCatalog = 0
    var listOffsetCatalog = 0

    var listPositionCatalogSub = 0
    var listOffsetCatalogSub = 0


    init {
        errorInternet.postValue(false)
        basketItemLD.observeForever {
            basketList.add(it)
            basketCountLD.value = basketList.size
        }
    }


    fun apiCheckPromoCode(
        token: String, promoCode: String?
    ): MutableLiveData<Response<CheckPromoCodeResponse>> {
        val oneClickResult: MutableLiveData<Response<CheckPromoCodeResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                oneClickResult.postValue(
                    serverApi.apiPromoCheck(
                        token, promoCode
                    )
                )
            }
        }
        return oneClickResult
    }

    fun apiOneClickBuyProductId(
        token: String, telephone: String?, quantity: String?, product_id: String?
    ): MutableLiveData<Response<OneClickBuyResponse>> {
        val oneClickResult: MutableLiveData<Response<OneClickBuyResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                oneClickResult.postValue(
                    serverApi.apiOneClickBuyProductId(
                        token, telephone, quantity, product_id, "", ""
                        , 0, 0, ""
                    )
                )
            }
        }
        return oneClickResult
    }

    fun apiOneClickBuy(
        token: String, telephone: String?, quantity: String?, product_id: String?,
        option_value: String?, option_desc: String?, option_price: String?, povId: String?,
        poId: String?
    ): MutableLiveData<Response<OneClickBuyResponse>> {
        val oneClickResult: MutableLiveData<Response<OneClickBuyResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                oneClickResult.postValue(
                    serverApi.apiOneClickBuy(
                        token, telephone, quantity, product_id, option_value,
                        option_desc, option_price, povId?.toInt(), poId?.toInt()
                    )
                )
            }
        }
        return oneClickResult
    }

    fun apiAddToAmbar(
        orderId: String,
        token: String, date: String, comment: String, productsStr: String
    ): MutableLiveData<Response<AddAmbarResponse>> {
        val addResult: MutableLiveData<Response<AddAmbarResponse>> = MutableLiveData()
        val contentType = "application/x-www-form-urlencoded"
        scope.launch {
            withContext(Dispatchers.Main) {
                addResult.postValue(
                    serverApi.apiAddAmbar(orderId, token, contentType, date, comment, productsStr)
                )
            }
        }
        return addResult
    }

    fun apiUpdateToAmbar(
        orderId: String,
        token: String, date: String, ambId: String, productsStr: String, comment: String
    ): MutableLiveData<Response<AddAmbarResponse>> {
        val addResult: MutableLiveData<Response<AddAmbarResponse>> = MutableLiveData()
        val contentType = "application/x-www-form-urlencoded"
        scope.launch {
            withContext(Dispatchers.Main) {
                addResult.postValue(
                    serverApi.apiUpdateAmbar(
                        orderId,
                        token,
                        contentType,
                        date,
                        ambId,
                        productsStr,
                        comment
                    )
                )
            }
        }
        return addResult
    }

    fun apiGetOrderInfo(
        token: String, orderId: String, lang: String
    ): MutableLiveData<Response<CheckoutOrderInfoResponse>> {
        val infoOrder: MutableLiveData<Response<CheckoutOrderInfoResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                infoOrder.postValue(
                    serverApi.apiGetOrderInfo(token, orderId, lang)
                )
            }
        }
        return infoOrder
    }

    fun apiGetOrders(
        token: String, lang: String
    ): MutableLiveData<Response<CheckoutOrdersResponse>> {
        val listOrders: MutableLiveData<Response<CheckoutOrdersResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                listOrders.postValue(
                    serverApi.apiGetOrders(token, lang)
                )
            }
        }
        return listOrders
    }

    fun apiOrderMake(
        token: String,
        ambar: String?,
        address: String?,
        cityId: String?,
        zoneId: String?,
        comment: String?,
        paymentCode: String?,
        shippingCode: String?,
        vaucher: String?,
        bonus: String?
    ): MutableLiveData<Response<OrderMakeResponse>> {
        var orderResponse: MutableLiveData<Response<OrderMakeResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                orderResponse.postValue(
                    serverApi.apiOrderMake(
                        token, (ambar ?: "0").toInt(), address, cityId, zoneId,
                        comment, paymentCode, shippingCode, vaucher, bonus
                    )
                )
            }
        }
        return orderResponse
    }

    fun apiConfirmPayment(
        token: String, orderId: String
    ): MutableLiveData<Response<ConfirmPaymentResponse>> {
        val confirmPayment: MutableLiveData<Response<ConfirmPaymentResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                confirmPayment.postValue(
                    serverApi.apiConfirmPaymentLinq(token, orderId)
                )
            }
        }
        return confirmPayment
    }

    fun apiCheckoutPaymentTypesList(
        token: String, lang: String
    ): MutableLiveData<Response<CheckoutListPaymentsResponse>> {
        val paymentList: MutableLiveData<Response<CheckoutListPaymentsResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                paymentList.postValue(
                    serverApi.apiGetListPayments(token, lang)
                )
            }
        }
        return paymentList
    }

    fun apiCheckoutDeliveryTypesList(
        token: String
    ): MutableLiveData<Response<CheckoutListDeliveryTypesResponse>> {
        val deliveryList: MutableLiveData<Response<CheckoutListDeliveryTypesResponse>> =
            MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                deliveryList.postValue(
                    serverApi.apiGetListDeliveryTypes(token)
                )
            }
        }
        return deliveryList
    }

    fun apiCheckoutPickupList(
        token: String
    ): MutableLiveData<Response<CheckoutListPickupResponse>> {
        val pickupList: MutableLiveData<Response<CheckoutListPickupResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                pickupList.postValue(
                    serverApi.apiGetListPickup(token)
                )
            }
        }
        return pickupList
    }

    fun apiCheckoutZoneList(
        token: String
    ): MutableLiveData<Response<CheckoutListZonesResponse>> {
        val zoneList: MutableLiveData<Response<CheckoutListZonesResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                zoneList.postValue(
                    serverApi.apiGetListZones(token)
                )
            }
        }
        return zoneList
    }

    fun apiCheckoutCityList(
        token: String, zoneId: String
    ): MutableLiveData<Response<CheckoutListCitiesResponse>> {
        val cityList: MutableLiveData<Response<CheckoutListCitiesResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                cityList.postValue(
                    serverApi.apiGetListCities(token, zoneId)
                )
            }
        }
        return cityList
    }

    fun apiCheckoutDepList(
        token: String, ciryId: String
    ): MutableLiveData<Response<CheckoutListDepNovaposhtaResponse>> {
        val depList: MutableLiveData<Response<CheckoutListDepNovaposhtaResponse>> =
            MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                depList.postValue(
                    serverApi.apiGetListDepNovaposhta(token, ciryId)
                )
            }
        }
        return depList
    }

    fun apiSearch(
        lang: String, searchWord: String
    ): MutableLiveData<Response<ListProductResponse>> {
        val searchResponse: MutableLiveData<Response<ListProductResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                searchResponse.postValue(
                    serverApi.apiSearch(lang, searchWord)
                )
            }
        }
        return searchResponse
    }


    fun apiUpdateBasket(
        token: String, basketId: Int, quantity: Int
    ): MutableLiveData<Response<BasketUpdateResponse>> {
        var basketResponse: MutableLiveData<Response<BasketUpdateResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                basketResponse.postValue(
                    serverApi.apiUpdateBasket(token, basketId, quantity)
                )
            }
        }
        return basketResponse
    }

    fun apiDeleteBasket(
        token: String, basketId: Int
    ): MutableLiveData<Response<BasketDeleteResponse>> {
        var basketResponse: MutableLiveData<Response<BasketDeleteResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                basketResponse.postValue(
                    serverApi.apiDeleteBasket(token, basketId)
                )
            }
        }
        return basketResponse
    }

    fun apiAddBasket(
        token: String, quantity: String?, productId: String?, povId: String?, poId: String?,
        optionValue: String?, optionDesc: String?, optionPrice: String?
    ): MutableLiveData<Response<BasketAddResponse>>
    {
        var basketResponse: MutableLiveData<Response<BasketAddResponse>> = MutableLiveData()

        scope.launch {
            withContext(Dispatchers.Main) {
                basketResponse.postValue(
                    serverApi.apiAddBasket(
                        token, quantity, productId, povId, poId,
                        optionValue, optionDesc, optionPrice
                    )
                )
            }
        }
        return basketResponse
    }

    fun apiGetBasket(
        token: String, lang: String
    ): MutableLiveData<Response<BasketResponse>> {
        var basketResponse: MutableLiveData<Response<BasketResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                basketResponse.postValue(
                    serverApi.apiGetBasket(token, lang)
                )
            }
        }
        return basketResponse
    }


    fun apiRestorePassword(
        email: String?
    ): MutableLiveData<Response<RestorePasswordResponse>> {

        var restorePassword: MutableLiveData<Response<RestorePasswordResponse>> = MutableLiveData()

        //var newEmail= email?.replace("@","%40")
        scope.launch {
            withContext(Dispatchers.Main) {
                val type = "text/html"
                serverApi2 = ApiRetrofit.changeApiBaseUrl()

                restorePassword.postValue(
                    serverApi2?.apiRestorePassword(
                        email
                    )
                )
            }
        }
        return restorePassword
    }

    fun apiLoginNetworks(
        idNet: String?, typeNet: String?, email: String?
    ): MutableLiveData<Response<LoginGoogleResponse>> {

        var loginResponse: MutableLiveData<Response<LoginGoogleResponse>> = MutableLiveData()

        scope.launch {
            withContext(Dispatchers.Main) {
                val type = "application/x-www-form-urlencoded"
                loginResponse.postValue(
                    serverApi.apiLoginNetworks(
                        type,
                        idNet,
                        typeNet, email
                    )
                )
            }
        }
        return loginResponse
    }


    fun apiChangePassword(
        token: String, password: String, newpassword: String
    ):
            MutableLiveData<Response<ResetPasswordResponse>> {
        var passwordData: MutableLiveData<Response<ResetPasswordResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                //String, RequestBody
                val map = HashMap<String, RequestBody>()
                val passwordBody =
                    RequestBody.create(MediaType.parse("multipart/form-data"), password)
                map["password"] = passwordBody
                val newpasswordBody =
                    RequestBody.create(MediaType.parse("multipart/form-data"), newpassword)
                map["newpassword"] = newpasswordBody
                passwordData.postValue(
                    serverApi.apiResetPassword(token, map)
                )
            }
        }
        return passwordData
    }


    fun apiUpdateUserProfile(
        token: String, email: String, telephone: String, firstname: String, lastname: String
    ): MutableLiveData<Response<UpdateProfileResponse>> {
        var updateUserProfile: MutableLiveData<Response<UpdateProfileResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                val map = HashMap<String, RequestBody>()
                val firstnameBody =
                    RequestBody.create(MediaType.parse("multipart/form-data"), firstname)
                map["firstname"] = firstnameBody
                val lastnameBody =
                    RequestBody.create(MediaType.parse("multipart/form-data"), lastname)
                map["lastname"] = lastnameBody

                updateUserProfile.postValue(
                    serverApi.apiUpdateUserProfile(token, map)
                )
            }
        }
        return updateUserProfile
    }


    fun apiLoginUser(
        email: String,
        password: String

    ): MutableLiveData<Response<LoginResponse>> {
        var loginUser: MutableLiveData<Response<LoginResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                //loginUser=MutableLiveData()
                //String, RequestBody
                val map = HashMap<String, RequestBody>()
                val emailBody = RequestBody.create(MediaType.parse("multipart/form-data"), email)
                map["email"] = emailBody
                val passwordBody =
                    RequestBody.create(MediaType.parse("multipart/form-data"), password)
                map["password"] = passwordBody



                loginUser.postValue(
                    serverApi.apiLoginUser(map)
                )
            }
        }
        return loginUser
    }


    fun apiGetProfile(token: String): MutableLiveData<Response<ProfileResponse>> {

        var getProfile: MutableLiveData<Response<ProfileResponse>> = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                getProfile.postValue(
                    serverApi.apiGetProfile(token)
                )
            }
        }
        return getProfile
    }


    var newReviewResponse: MutableLiveData<Response<NewReviewResponse>> = MutableLiveData()

    fun apiSetNewProductReview(
        productId: String, email: String, author: String, rating: String, text: String
    ): MutableLiveData<Response<NewReviewResponse>> {
        newReviewResponse = MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                val type = "application/x-www-form-urlencoded"
                /*
                 @Field("email")email:String,@Field("name")name:String,
                                       @Field("rating")rating:String,@Field("text")text:String
                 */
                newReviewResponse.postValue(

                    serverApi.apiSetNewProductReview(
                        type,
                        productId,
                        email,
                        author,
                        rating,
                        text
                    )
                )
            }
        }
        return newReviewResponse
    }


    fun apiRegisterUser(
        firstname: String, lastname: String, email: String, telephone: String, password: String,
        leaderNumber: String?, fbId: String?, ggId: String?
    ): MutableLiveData<Response<RegisterResponse>> {
        var registerUser: MutableLiveData<Response<RegisterResponse>> = MutableLiveData()
        //registerUser = MutableLiveData()
        var newNumber: String? = ""

        if (leaderNumber ?: "" == "")
            newNumber = null
        else
            newNumber = leaderNumber

        scope.launch {
            withContext(Dispatchers.Main) {
                registerUser.postValue(
                    serverApi.apiRegisterUser(
                        firstname,
                        lastname,
                        email,
                        telephone,
                        password,
                        newNumber, fbId, ggId
                    )
                )
            }
        }
        return registerUser
    }


    fun apiGetProductsListByCategory(
        lang: String,
        category: String
    ): MutableLiveData<Response<ListProductResponse>> {
        var listProductResponse: MutableLiveData<Response<ListProductResponse>> = MutableLiveData()

        scope.launch {
            withContext(Dispatchers.Main) {
                listProductResponse.postValue(
                    serverApi.apiGetProductsListByCategory(
                        lang,
                        category
                    )
                )
            }
        }
        return listProductResponse
    }


    fun apiGetProductsListByPrice(
        lang: String,
        category: String,
        sortType: String
    ): MutableLiveData<Response<ListProductResponse>> {
        var listProductResponsePrice: MutableLiveData<Response<ListProductResponse>> =
            MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                listProductResponsePrice.postValue(
                    serverApi.apiGetProductsListSortedPrice(
                        lang,
                        category,
                        sortType,
                        LIMIT_NUMBER
                    )
                )

            }
        }
        return listProductResponsePrice

    }


    fun apiGetProductsListByFilter(
        lang: String,
        category: String,
        filter: String
    ): MutableLiveData<Response<ListProductResponse>> {

        var listProductResponseFilter: MutableLiveData<Response<ListProductResponse>> = MutableLiveData()

        scope.launch {
            withContext(Dispatchers.Main) {
                listProductResponseFilter.postValue(
                    serverApi.apiGetProductsListFilter(
                        lang,
                        category,
                        filter,
                        LIMIT_NUMBER
                    )
                )
            }
        }
        return listProductResponseFilter
    }


    fun apiGetProductsListByName(
        lang: String,
        category: String,
        sortType: String
    ): MutableLiveData<Response<ListProductResponse>> {
        var listProductResponseName: MutableLiveData<Response<ListProductResponse>> =
            MutableLiveData()
        scope.launch {
            withContext(Dispatchers.Main) {
                listProductResponseName.postValue(
                    serverApi.apiGetProductsListSortedAZ(
                        lang,
                        category,
                        sortType,
                        LIMIT_NUMBER
                    )
                )
            }
        }
        return listProductResponseName
    }


    fun apiGetProductInfo(
        lang: String,
        productId: String
    ): MutableLiveData<Response<ProductInfoResponse>> {
        var productInfoResponse: MutableLiveData<Response<ProductInfoResponse>> = MutableLiveData()

        scope.launch {
            withContext(Dispatchers.Main) {


                val result = serverApi.apiGetProductInfo(lang, productId)
                var bitmapList: ArrayList<Bitmap> = arrayListOf()
                if (result.isSuccessful) {
                    var imagesSise = result.body()?.answer?.info?.image?.size ?: 0
                    var images = result.body()?.answer?.info?.image ?: arrayListOf()


                    if (imagesSise > 0) {

                        for (index in 0 until images.size) {
                            //catalog/baners/banner-1-ua.jpg"
                            var stream: InputStream? = null
                            var bitmap: Bitmap? = null
                            try {
                                val url = URL(IMAGE_URL + images[index] ?: "")
                                stream = url.openConnection().getInputStream()
                                stream?.let {
                                    val reader = BufferedInputStream(stream)
                                    bitmap = BitmapFactory.decodeStream(reader)
                                    bitmap?.let {

                                        bitmapList?.add(it)
                                    }
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }

                        result.body()?.answer?.info?.imageBitmap?.addAll(bitmapList)
                    }
                }

                productInfoResponse.postValue(result)
            }
        }
        return productInfoResponse
    }

    fun apiGetCatalogList(lang: String) {

        mDisposable.add(
            serverApi.apiGetMainCatalog(lang)
                .observeOn(Schedulers.newThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                    { response ->
                        mainCatalogLD.postValue(response)

                    }
                    , {

                        var errorMessage = CatalogResponse()
                        when (it) {
                            is UnknownHostException -> {
                                errorInternet.postValue(true)
                                errorMessage.serverError = it.localizedMessage
                            }
                            is IOException -> {
                                //handle network serverError
                                errorMessage.serverError = it.localizedMessage
                            }
                            is HttpException -> {
                                //handle HTTP serverError response code
                                errorMessage.serverError = it.code().toString() + it.message()
                                //var code = it as HttpException
                                // if (code.code() == 500) { }
                            }
                            else -> {
                                //handle other exceptions
                                it?.let {
                                    errorMessage.serverError = it.localizedMessage
                                }

                            }
                        }
                        mainCatalogLD.postValue(errorMessage)

                    })
        )
    }

    fun apiGetSubCategoryCatalog(subNumber: String, lang: String) {
        mDisposable.add(
            serverApi.apiGetSubCategory(subNumber, lang)
                .observeOn(Schedulers.newThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                    { response ->
                        mainSubCategoryLD.postValue(response)
                    }
                    , {
                        Log.d("Rf Tracking serverError", it.message.toString())
                        var errorMessage = CatalogResponse()
                        when (it) {
                            is UnknownHostException -> {
                                errorInternet.postValue(true)
                                errorMessage.serverError = it.localizedMessage
                            }
                            is IOException -> {
                                //handle network serverError
                                errorMessage.serverError = it.localizedMessage
                            }
                            is HttpException -> {
                                //handle HTTP serverError response code
                                errorMessage.serverError = it.code().toString() + it.message()
                                //var code = it as HttpException
                                // if (code.code() == 500) { }
                            }
                            else -> {
                                //handle other exceptions
                                it?.let {
                                    errorMessage.serverError = it.localizedMessage
                                }

                            }
                        }
                        mainSubCategoryLD.postValue(errorMessage)
                    })
        )
    }

    fun apiGetBannersList() {
        mDisposable.add(
            serverApi.apiGetBanners()
                .observeOn(Schedulers.newThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                    { response ->

                        response.answer?.let {

                            for (item in it) {
                                //catalog/baners/banner-1-ua.jpg"
                                var stream: InputStream? = null
                                var bitmap: Bitmap? = null
                                try {
                                    var afterDecode = IMAGE_URL + item?.image
                                    var url = URL(afterDecode)
                                    stream = url.openConnection().getInputStream()
                                    stream?.let {
                                        val reader = BufferedInputStream(stream)
                                        bitmap = BitmapFactory.decodeStream(reader)
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                bitmap?.let {
                                    //item?.bitmap = resize(it)}
                                    item?.bitmap = it
                                }
                            }
                        }
                        mainBannersLD.postValue(response)
                    }
                    , {
                        Log.d("Rf Tracking serverError", it.message.toString())
                        var errorMessage = BannerResponse()
                        when (it) {
                            is UnknownHostException -> {
                                errorInternet.postValue(true)
                                errorMessage.serverError = it.localizedMessage
                            }
                            is IOException -> {
                                //handle network serverError
                                errorMessage.serverError = it.localizedMessage
                            }
                            is HttpException -> {
                                //handle HTTP serverError response code
                                errorMessage.serverError = it.code().toString() + it.message()
                                //var code = it as HttpException
                                // if (code.code() == 500) { }
                            }
                            else -> {
                                //handle other exceptions
                                it?.let {
                                    errorMessage.serverError = it.localizedMessage
                                }

                            }
                        }
                        mainBannersLD.postValue(errorMessage)
                    })
        )
    }


    private fun resize(bitmap: Bitmap): Bitmap {
        val ws = bitmap.width * 1.0
        val hs = bitmap.height * 1.0
        val k = 1000.0 / ws
        return Bitmap.createScaledBitmap(
            bitmap,
            1000,
            (hs * k).toInt(),
            false
        )
    }
}

