package noty.team.miska.viewmodel

import io.reactivex.Single
import noty.team.miska.modelRetrofit.*
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

val API_END_POINT = "http://api.vmiske.ua/"
val API_END_POINT_WWW = "https://vmiske.ua/"
const val TIMEOUT = 10L
const val LANG_RU = "ua"
const val LANG_UA = "ru"

interface ApiRetrofit {


    @FormUrlEncoded
    @POST("order/coupon")
    suspend fun apiPromoCheck(
        @Header("Authorization") token: String,
        @Field("vaucher") vaucherPromoCode: String?

    ): Response<CheckPromoCodeResponse>

    @FormUrlEncoded
    @POST("order/fastmake")
    suspend fun apiOneClickBuyProductId(
        @Header("Authorization") token: String,
        @Field("telephone") telephone: String?,
        @Field("quantity") quantity: String?,
        @Field("product_id") productId: String?,
        @Field("option_value") optionValue: String?,
        @Field("option_desc") optionDesc: String?,
        @Field("povId") povId: Int?,
        @Field("poId") poId: Int?,
        @Field("option_price") option_price: String?
    ): Response<OneClickBuyResponse>


    @FormUrlEncoded
    @POST("order/fastmake")
    suspend fun apiOneClickBuy(
        @Header("Authorization") token: String,
        @Field("telephone") telephone: String?,
        @Field("quantity") quantity: String?,
        @Field("product_id") productId: String?,
        @Field("option_value") optionValue: String?,
        @Field("option_desc") optionDesc: String?,
        @Field("option_price") optionPrice: String?,
        @Field("povId") povId: Int?,
        @Field("poId") poId: Int?

    ): Response<OneClickBuyResponse>

    @FormUrlEncoded
    @POST("order/ambar/{orderId}")
    suspend fun apiAddAmbar(
        @Path("orderId") orderId: String,
        @Header("Authorization") token: String,
        @Header("Content-Type") contentType: String,
        @Field("date") date: String?,
        @Field("comment") comment: String?,
        @Field("products") productsStr: String
    ): Response<AddAmbarResponse>

    @FormUrlEncoded
    @POST("order/ambar/{orderId}")
    suspend fun apiUpdateAmbar(
        @Path("orderId") orderId: String,
        @Header("Authorization") token: String,
        @Header("Content-Type") contentType: String,
        @Field("date") date: String?,
        @Field("ambId") ambId: String?,
        @Field("products") productsStr: String,
        @Field("comment") comment: String?
    ): Response<AddAmbarResponse>

    @FormUrlEncoded
    @POST("order/make")
    suspend fun apiOrderMake(
        @Header("Authorization") token: String,
        @Field("ambar") ambar: Int?,
        @Field("address") address: String?,
        @Field("city_id") city_id: String?,
        @Field("zone_id") zone_id: String?,
        @Field("comment") comment: String?,
        @Field("payment_code") payment_code: String?,
        @Field("shipping_code") shipping_code: String?,
        @Field("vaucher") vaucher: String?,
        @Field("bonuse") bonus: String?
    ): Response<OrderMakeResponse>

    @GET("order/confirm/{orderId}")
    suspend fun apiConfirmPaymentLinq(@Header("Authorization") token: String, @Path("orderId") orderId: String): Response<ConfirmPaymentResponse>

    @GET("order/")
    suspend fun apiGetOrders(@Header("Authorization") token: String, @Header("X-Locale") lang: String): Response<CheckoutOrdersResponse>

    @GET("order/zone")
    suspend fun apiGetListZones(@Header("Authorization") token: String): Response<CheckoutListZonesResponse>

    @GET("order/city")
    suspend fun apiGetListCities(@Header("Authorization") token: String, @Query("id") zoneId: String): Response<CheckoutListCitiesResponse>

    @GET("order/novaposhta")
    suspend fun apiGetListDepNovaposhta(@Header("Authorization") token: String, @Query("id") idCity: String): Response<CheckoutListDepNovaposhtaResponse>

    @GET("order/pickup")
    suspend fun apiGetListPickup(@Header("Authorization") token: String): Response<CheckoutListPickupResponse>

    @GET("order/delivery")
    suspend fun apiGetListDeliveryTypes(@Header("Authorization") token: String): Response<CheckoutListDeliveryTypesResponse>

    @GET("order/paymethod/")
    suspend fun apiGetListPayments(
        @Header("Authorization") token: String, @Header(
            "X-Locale"
        ) lang: String
    ): Response<CheckoutListPaymentsResponse>

    @GET("order/info/{orderId}")
    suspend fun apiGetOrderInfo(
        @Header("Authorization") token: String, @Path("orderId") orderId: String, @Header(
            "X-Locale"
        ) lang: String
    ): Response<CheckoutOrderInfoResponse>


    @GET("products/list/?limit=1")
    suspend fun apiSearch(@Header("X-Locale") lang: String, @Query("word") findString: String): Response<ListProductResponse>

    @GET("basket/")
    suspend fun apiGetBasket(
        @Header("Authorization") token: String,
        @Header("X-Locale") lang: String
    ): Response<BasketResponse>

    @FormUrlEncoded
    @POST("basket/add")
    suspend fun apiAddBasket(
        @Header("Authorization") token: String,
        @Field("quantity") quantity: String?,
        @Field("product_id") productId: String?,
        @Field("povId") povId: String?,
        @Field("poId") poId: String?,
        @Field("option_value") optionValue: String?,
        @Field("option_desc") optionDesc: String?,
        @Field("option_price") optionPrice: String?
    ): Response<BasketAddResponse>

    @FormUrlEncoded
    @POST("basket/delete")
    suspend fun apiDeleteBasket(
        @Header("Authorization") token: String,
        @Field("baskId") baskId: Int?
    ): Response<BasketDeleteResponse>

    @FormUrlEncoded
    @POST("basket/update")
    suspend fun apiUpdateBasket(
        @Header("Authorization") token: String,
        @Field("baskId") baskId: Int?,
        @Field("quantity") quantity: Int?
    ): Response<BasketUpdateResponse>

    @FormUrlEncoded
    @POST("index.php?route=account/forgotten/forgottenProcessing")
    suspend fun apiRestorePassword(
        @Field("email") email: String?
    ): Response<RestorePasswordResponse>


    @FormUrlEncoded
    @POST("user/sigsn")
    suspend fun apiLoginNetworks(
        @Header("Content-Type") type: String, @Field("id") idNetwork: String?,
        @Field("type") typeNetwork: String?,
        @Field("email") email: String?
    ): Response<LoginGoogleResponse>

    @Multipart
    @POST("user/resetpwd")
    suspend fun apiResetPassword(@Header("Authorization") token: String, @PartMap params: HashMap<String, RequestBody>): Response<ResetPasswordResponse>

    @Multipart
    @POST("user/update")
    suspend fun apiUpdateUserProfile(@Header("Authorization") token: String, @PartMap params: HashMap<String, RequestBody>): Response<UpdateProfileResponse>

    @Multipart
    @POST("user/sigin")
    suspend fun apiLoginUser(@PartMap params: HashMap<String, RequestBody>): Response<LoginResponse>

    @GET("user/profile")
    suspend fun apiGetProfile(@Header("Authorization") token: String): Response<ProfileResponse>

    @FormUrlEncoded
    @POST("user/signup")
    suspend fun apiRegisterUser(
        @Field("firstname") firstname: String,
        @Field("lastname") lastname: String,
        @Field("email") email: String,
        @Field("telephone") telephone: String,
        @Field("password") password: String,
        @Field("vogak") vogak: String?,
        @Field("fbId") fbId: String?,
        @Field("glId") glId: String?
    ): Response<RegisterResponse>

    @GET("categories/list")
    fun apiGetMainCatalog(@Header("X-Locale") lang: String): Single<CatalogResponse>

    @GET("categories/list/{categoryNumber}")
    fun apiGetSubCategory(@Path("categoryNumber") categoryNumber: String, @Header("X-Locale") lang: String): Single<CatalogResponse>

    @GET("banners/list")
    fun apiGetBanners(): Single<BannerResponse>

    @GET("products/list/{category}?limit=-1")
    suspend fun apiGetProductsListByCategory(@Header("X-Locale") lang: String, @Path("category") category: String): Response<ListProductResponse>


    //http://api.vmiske.ua/products/list/123?limit=-1&order=price&type=asc
    @GET("products/list/{category}?order=price")
    suspend fun apiGetProductsListSortedPrice(
        @Header("X-Locale") lang: String, @Path("category") category: String,
        @Query("stype") type: String, @Query("limit") limit: String
    ): Response<ListProductResponse>

    @GET("products/list/{category}?order=name")
    suspend fun apiGetProductsListSortedAZ(
        @Header("X-Locale") lang: String, @Path("category") category: String,
        @Query("stype") type: String, @Query("limit") limit: String
    ): Response<ListProductResponse>

    @GET("products/list/{category}")
    suspend fun apiGetProductsListFilter(
        @Header("X-Locale") lang: String, @Path("category") category: String,
        @Query("filter") filter: String, @Query("limit") limit: String
    ): Response<ListProductResponse>

    @GET("products/info/{productId}")
    suspend fun apiGetProductInfo(@Header("X-Locale") lang: String, @Path("productId") productId: String): Response<ProductInfoResponse>

    @FormUrlEncoded
    @POST("products/reviews/{productId}")
    suspend fun apiSetNewProductReview(
        @Header("Content-Type") contentType: String, @Path("productId") productId: String,

        @Field("email") email: String, @Field("author") author: String,
        @Field("rating") rating: String, @Field("text") text: String

    ): Response<NewReviewResponse>

    companion object Factory {

        fun create(): ApiRetrofit {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder()
            client.readTimeout(TIMEOUT, TimeUnit.SECONDS)
            client.writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            client.connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            client.addInterceptor(interceptor)
            client.addInterceptor { chain ->
                val request = chain.request()
                val newRequest: Request

                newRequest = request.newBuilder()
                    .addHeader("Content-type", "application/json")
                    .build()
                chain.proceed(newRequest)
            }

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client.build())
                .baseUrl(apiBaseUrl)
                .build()
            return retrofit.create(ApiRetrofit::class.java)
        }

        var apiBaseUrl = API_END_POINT

        fun changeApiBaseUrl(): ApiRetrofit {
            apiBaseUrl = API_END_POINT_WWW

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder()
            client.readTimeout(TIMEOUT, TimeUnit.SECONDS)
            client.writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            client.connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            client.addInterceptor(interceptor)
            client.addInterceptor { chain ->
                val request = chain.request()
                val newRequest: Request
                newRequest = request.newBuilder()
                    .addHeader("Content-type", "application/json")
                    .build()
                chain.proceed(newRequest)
            }

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client.build())
                .baseUrl(apiBaseUrl)
                .build()
            return retrofit.create(ApiRetrofit::class.java)
        }

    }
}




