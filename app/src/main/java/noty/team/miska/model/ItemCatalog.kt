package noty.team.miska.model

class ItemCatalog() {
    var cId: Int = 0
    var imageUrl: String = ""
    var name: String = ""
    var imageId: Int = 0
    var subCategoryName: String = ""
    var subCategoryId: Int? = 0
    var subCategoryCount: Int? = 0
    var serverError: String? = null

    constructor(name: String, imageId: Int, subCategoryId: Int, subCategoryCount: Int) : this() {
        this.name = name
        this.subCategoryId = subCategoryId
        this.subCategoryCount = subCategoryCount
        this.imageId = imageId

    }

    constructor(
        name: String,
        imageUrl: String,
        subCategoryId: Int,
        subCategoryCount: Int
    ) : this() {
        this.name = name
        this.subCategoryId = subCategoryId
        this.subCategoryCount = subCategoryCount
        this.imageId = 0
        this.imageUrl = imageUrl


    }

}
