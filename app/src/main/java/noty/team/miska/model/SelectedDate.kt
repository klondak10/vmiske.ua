package noty.team.miska.model

class SelectedDate() {
    var year: String = ""
    var month: String = ""
    var day: String = ""

    constructor(d: String, m: String, y: String) : this() {
        day = d
        month = m
        year = y
    }
}