package noty.team.miska.model

class UserProfile(
    var email: String,
    var telephone: String,
    var firstname: String,
    var lastname: String,
    var token: String
)

