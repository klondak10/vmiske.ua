package noty.team.miska.model


class ItemProductReview(
    userName: String,
    userDate: String,
    userRating: Float,
    userText: String
) {
    var imageId: Int = 0

    var author: String = ""
    var date: String = ""
    var rating: Float = 0.0f
    var text: String = ""
    var email: String = ""
    var answer: Boolean? = null
    var managerName: String = ""
    var managerDate: String = ""
    var managerText: String = ""

    init {
        this.author = userName
        this.date = userDate
        this.rating = userRating
        this.text = userText
    }
}
