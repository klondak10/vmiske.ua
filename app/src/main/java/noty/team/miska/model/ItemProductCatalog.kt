package noty.team.miska.model

import android.widget.ImageView
import noty.team.miska.modelRetrofit.Discount
import noty.team.miska.modelRetrofit.Subprice


class ItemProductCatalog(name: String, imageId: Int) {

    var imageId: Int = 0
    var productId: Int = 0
    var imageUrl: String = ""
    var imageView: ImageView? = null
    var discount: Discount = Discount()
    var subprice: ArrayList<Subprice> = arrayListOf()
    var name: String = ""
    var SubCategory: String = ""
    var price: String = ""
    var newPrice: Int? = null
    var priceCurrency: String = ""
    var weight: String = ""
    var status: String = ""
    var rating: Float = 0.0f
    var imageName: String = ""
    var quantity: Int = 0
    var listPosition: Int = 0
    var salesEnabled: Boolean = false
    //for basket
    var basketItemInfo: BasketItemInfo? = null
    var basketId: Int? = 0
    var oneClickPhone: String? = ""
    var optionValueWeight = ""

    init {
        this.name = name
        this.imageId = imageId
    }


}

class BasketItemInfo() {
    var optionValue: String? = ""
    var optionDescription: String? = ""
    var optionPrice: Int? = 0
    var povId: Int? = 0
    var poId: Int? = 0
    var quantity: Int? = 0
    var product_id: Int? = 0
}
