package noty.team.miska.model

import noty.team.miska.modelRetrofit.*

class DeliveryAdress {
    //nova
    var dataZoneNova: AnswerZone = AnswerZone()
    var dataCityNova: AnswerCity = AnswerCity()
    var dataDepNova: AnswerDep = AnswerDep()
    //pickup
    var dataPickup: AnswerPickup = AnswerPickup()
    //courier
    var dataCourier: AnswerCourier = AnswerCourier()
    var dataZoneCourier: AnswerZone = AnswerZone()
    var dataCityCourier: AnswerCity = AnswerCity()


    var deliveryType: ArrayList<AnswerDeliveryType> = arrayListOf()
    var typeDeliverySelected: DELIVERYTYPE? = null


    var paymentType: ArrayList<AnswerPayment> = arrayListOf()
    var paymentTypeSelected: PAYMENTTYPE? = null


}

enum class DELIVERYTYPE {
    PICKUP, NOVA, COURIER
}

enum class PAYMENTTYPE {
    CASH, ONDELIVERY, LIQPAY
}