package noty.team.miska.model


class ShopingItem(
    number: String,
    date: String,
    sumOrder: Float,
    statusOrder: String,
    statusOrderInt: Int
) {

    var number: String = ""
    var date: String = ""
    var sumOrder: Float = 0.0f
    var statusOrderStr: String = ""
    var statusOrderInt: Int = 0

    init {
        this.number = number
        this.date = date
        this.sumOrder = sumOrder
        this.statusOrderStr = statusOrder
        this.statusOrderInt = statusOrderInt
    }
}
