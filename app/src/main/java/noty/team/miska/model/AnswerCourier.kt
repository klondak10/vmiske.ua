package noty.team.miska.model

class AnswerCourier {
    var regionName: String = ""
    var cityName: String = ""
    var streetName: String = ""
    var houseNumber: String = ""
    var apartmentNumber: String = ""
    var comment: String = ""
}
