package noty.team.miska.util

interface IRadioButton {
    fun setRadioId(newId: Int?)
    fun getRadioId(): Int?
}