package noty.team.miska.util

import com.google.android.gms.auth.api.signin.GoogleSignInAccount

interface IGoogleAccountStore {

    fun setGoogleAccount(account: GoogleSignInAccount?)
    fun getGoogleAccount(): GoogleSignInAccount?
}