package noty.team.miska.util

import noty.team.miska.modelRetrofit.ListProductResponse
import retrofit2.Response

interface SetDataFromFilter {
    fun setNewDataFromFilter(newData: Response<ListProductResponse>?)
}