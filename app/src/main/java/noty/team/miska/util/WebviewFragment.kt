package noty.team.miska.util


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_webview.*
import noty.team.miska.R


class WebviewFragment(webUrl: String?) : Fragment() {

    private var webUrl: String? = webUrl

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_webview, container, false)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        webUrl?.let {
            webView.settings.javaScriptEnabled = true
            webView.loadUrl(it)
        }
    }


}
