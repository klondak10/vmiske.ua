package noty.team.miska.util


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_info_no_internet.*
import noty.team.miska.CATALOG_FRAGMENT
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.catalog.CatalogMainFragment


class InfoNoInternetFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_info_no_internet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).fabShow()

        try_reconnect_button.setOnClickListener {

            if (isOnline()){
                context?.toast(getString(R.string.connection_success))
                val fragment = CatalogMainFragment()
                fragment.setShowHideAdver(true)
                (activity as MainActivity).replaceFragment(fragment, CATALOG_FRAGMENT,true,"")

            }else{
                context?.toast(getString(R.string.connection_fail))
            }

        }
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).fabShow()
    }

    override fun onPause() {
        super.onPause()
        (activity as MainActivity).fabHide()
    }
}
