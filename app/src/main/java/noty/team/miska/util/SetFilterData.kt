package noty.team.miska.util

import noty.team.miska.modelRetrofit.ListProductResponse
import retrofit2.Response

interface SetFilterData {
    fun setNewFilterData(
        newData: Response<ListProductResponse>,
        category: String,
        categoryName: String
    )
}