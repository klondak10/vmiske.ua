package noty.team.miska.util


import androidx.fragment.app.Fragment

interface UtilInterface {
    fun replaceFragment(
        fragment: Fragment,
        fragmentTag: String,
        showLogo: Boolean,
        titleText: String
    )

    fun showCustomAppBar(text: String, showBar: Boolean, showMenuButton: Boolean)
    fun showCatalog(showAdver: Boolean)
    fun showUserMenu(showUserMenu: Boolean, userName: String)
    fun showSortDrawer(show: Boolean)
    fun showFilterDrawer(show: Boolean)
    fun showUserEditProfileMenu(showEditProfile: Boolean)

}