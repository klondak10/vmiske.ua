package noty.team.miska.util

import android.app.Activity
import android.content.Context
import android.graphics.PorterDuff
import android.widget.Toast
import noty.team.miska.R
import noty.team.miska.model.SelectedDate
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.text.SimpleDateFormat
import java.util.*


fun findImageIdByName(imageName: String, imageId: HashMap<String, Int>): Int {
    var imageLocalId: Int? = 0

    when (imageName.toLowerCase()) {
        "Собаки".toLowerCase() -> imageLocalId = imageId.get("icon_dog")
        "Коты".toLowerCase() -> imageLocalId = imageId.get("icon_cat")
        "Аквариумистика".toLowerCase() -> imageLocalId = imageId.get("icon_fish")
        "Лошади".toLowerCase() -> imageLocalId = imageId.get("icon_horse")
        "Птицы".toLowerCase() -> imageLocalId = imageId.get("icon_bird")
        "Грызуны".toLowerCase() -> imageLocalId = imageId.get("icon_rodent")
        "Хорьки".toLowerCase() -> imageLocalId = imageId.get("icon_ferret")
        "Рептилии".toLowerCase() -> imageLocalId = imageId.get("icon_reptiles")
        "Насекомые".toLowerCase() -> imageLocalId = imageId.get("icon_insect")
        "Пчелы".toLowerCase() -> imageLocalId = imageId.get("icon_bee")
        "Пруд".toLowerCase() -> imageLocalId = imageId.get("icon_pond")
        "Домашние животные".toLowerCase() -> imageLocalId = imageId.get("icon_pets")
        "Чистый дом".toLowerCase() -> imageLocalId = imageId.get("icon_cleanhome")
        "Корм на вес".toLowerCase() -> imageLocalId = imageId.get("icon_feed")
        "Акции".toLowerCase() -> imageLocalId = imageId.get("icon_sale")
        "Товары наших друзей".toLowerCase() -> imageLocalId = imageId.get("icon_for_friends")

        else -> {
            imageLocalId = 0
        }
    }
    if (imageLocalId == null) {
        imageLocalId = 0
    }
    return imageLocalId
}

fun Context.toast(message: CharSequence) {

    if (this is Activity)
        if (!this.isFinishing) {
            //Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
            var toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
            val view = toast.view
            view?.background
                ?.setColorFilter(
                    getResources().getColor(R.color.colorMainRedToast),
                    PorterDuff.Mode.SRC_IN
                )
            toast.show()
        }
}


fun isOnline(): Boolean {

    var connectOK: Boolean = false

    var timeoutMs = 1500
    var sock = Socket()
    var sockaddr = InetSocketAddress("8.8.8.8", 53)

    try {

        var mRunnable = Runnable {
            sock.connect(sockaddr, timeoutMs)
            sock.close()
            connectOK = true
        }
        mRunnable.run()
    } catch (e: IOException) {
        connectOK = false
    }
    return connectOK
}

fun removeZeros(src: String): String {
    var resStr = ""
    var newStr = (src.split("."))
    if (newStr.isNotEmpty())
        resStr = newStr[0]
    return resStr
}

fun formatDate(srcDate: String?): String {
    var resDate = ""
    try {
        //date":"2019-12-10 22:00:00"
        var df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        df.timeZone = TimeZone.getTimeZone("GMT")
        var result: Date = df.parse(srcDate)
        //get instance
        var calendar = Calendar.getInstance()
        calendar.timeInMillis = result.time

        var day = (calendar.get(Calendar.DATE)).toString()
        if (day.length == 1)
            day = "0" + day
        var month = (calendar.get(Calendar.MONTH) + 1).toString()
        if (month.length == 1)
            month = "0" + month


        resDate = day + "." +
               month + "." +
                calendar.get(Calendar.YEAR).toString()
    } catch (ex: Exception) {

        resDate = if (srcDate != null)
            srcDate
        else
            ""
    }
    return resDate
}

fun formatDateAmbar(srcDate: String?): String {
    ////2019-12-21 18:31:00
    var resDate = ""
    try {
        //date":"2019-12-10 22:00:00"
        //var df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        var df = SimpleDateFormat("yyyy-MM-dd")
        df.timeZone = TimeZone.getTimeZone("GMT")
        var result: Date = df.parse(srcDate)
        //get instance
        var calendar = Calendar.getInstance()
        calendar.timeInMillis = result.time

        var day = (calendar.get(Calendar.DATE)).toString()
        if (day.length == 1)
            day = "0" + day
        var month = (calendar.get(Calendar.MONTH) + 1).toString()
        if (month.length == 1)
            month = "0" + month

        resDate = day + "." +
                month + "." +
                calendar.get(Calendar.YEAR).toString()
    } catch (ex: Exception) {

        if (srcDate != null)
            resDate = srcDate
        else
            resDate = ""
    }
    return resDate
}

fun addZeroToDate(srcDate: SelectedDate): String {
    ////2019-12-21 18:31:00
    var resDate = ""
    try {
        var day = srcDate.day
        if (day.length == 1)
            day = "0" + day
        var month = srcDate.month
        if (month.length == 1)
            month = "0" + month

        resDate = day + "." +
                month + "." +
                srcDate.year
    } catch (ex: Exception) {
        resDate = ""
    }
    return resDate
}

fun returnDateAmbar(srcDate: String?): SelectedDate {
    ////2019-12-21 18:31:00

    var selectedDate = SelectedDate()
    try {
        //date":"2019-12-10 22:00:00"
        var df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        df.timeZone = TimeZone.getTimeZone("GMT")
        var result: Date = df.parse(srcDate)
        //get instance
        var calendar = Calendar.getInstance()
        calendar.timeInMillis = result.time
        var day = (calendar.get(Calendar.DATE)).toString()
        if (day.length == 1)
            day = "0" + day

        selectedDate.day = day

        var month = (calendar.get(Calendar.MONTH) + 1).toString()
        if (month.length == 1)
            month = "0" + month

        selectedDate.month = month
        selectedDate.year = calendar.get(Calendar.YEAR).toString()
    } catch (ex: Exception) {


    }
    return selectedDate
}