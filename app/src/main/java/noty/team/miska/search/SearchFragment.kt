package noty.team.miska.search


import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_search.*
import noty.team.miska.MainActivity
import noty.team.miska.PAPER_LANG_DEFAULT
import noty.team.miska.R
import noty.team.miska.SEARCH_RESULT_FRAGMENT
import noty.team.miska.model.ItemProductCatalog
import noty.team.miska.model.ItemSavedSearch
import noty.team.miska.modelRetrofit.ListProductResponse
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import noty.team.miska.viewmodel.IMAGE_URL
import noty.team.miska.viewmodel.LANG_RU
import retrofit2.Response

const val PAPER_SAVED_LIST = "PAPER_SAVED_LIST"

class SearchFragment : Fragment(), ISearchFragment {
    private var listener: ISearchFragment? = null
    var paperSavedList: ArrayList<ItemSavedSearch> = arrayListOf()
    var paperLang: String = LANG_RU
    var searchAdapter: SearchSavedAdapter? = null
    var mainList: MutableList<ItemProductCatalog> = arrayListOf()
    var searchString = ""
    var notFoundFlag = false
    val limit = 9

    override fun clickSavedList(item: ItemSavedSearch) {
        searchRequest(item.stringSearch, item.stringLang)
        searchString = item.stringSearch
        search_edittext_data.setText(item.stringSearch)

    }

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    fun showSearchHistory() {

        search_start_info_layout.visibility = View.GONE
        search_recycler_saved_layout.visibility = View.VISIBLE

    }

    fun hideSearchHistory() {
        search_start_info_layout.visibility = View.VISIBLE
        search_recycler_saved_layout.visibility = View.GONE
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            "", true,
            false
        )

        paperLang = Paper.book().read<String>(PAPER_LANG_DEFAULT, LANG_RU)
        var paperSavedList: ArrayList<ItemSavedSearch> =
            Paper.book().read(PAPER_SAVED_LIST, ArrayList<ItemSavedSearch>())

        for (index in 0 until paperSavedList.size)
            if (index > limit)
                paperSavedList.removeAt(index)

        searchAdapter =
            SearchSavedAdapter(context!!, paperSavedList, listener)
        recycler_saved_search?.adapter = searchAdapter

        search_info_clear_button.setOnClickListener {
            paperSavedList.clear()
            searchAdapter?.notifyDataSetChanged()

            Paper.book().write(PAPER_SAVED_LIST, paperSavedList)
            notFoundFlag = true
            search_image.setImageResource(R.drawable.group_4)
            search_start_find.text = getString(R.string.search_message_start)
            hideSearchHistory()
        }

        if (paperSavedList.size > 0) {
            showSearchHistory()
        } else {
            notFoundFlag = true
            search_image.setImageResource(R.drawable.group_4)
            search_start_find.text = getString(R.string.search_message_start)
            hideSearchHistory()
        }

        search_arrow_back.setOnClickListener {
            if (notFoundFlag) {
                notFoundFlag = false
                search_image.setImageResource(R.drawable.group_4)
                search_start_find.text = getString(R.string.search_message_start)
                return@setOnClickListener
            }
            if (paperSavedList.size > 0 && search_recycler_saved_layout.visibility == View.GONE) {
                showSearchHistory()
            } else {
                notFoundFlag = false
                (activity as MainActivity).onBackPressed()
            }
        }
        search_cancel_button.visibility = View.VISIBLE
        search_cancel_button.setImageResource(android.R.color.transparent)

        search_edittext_data.setOnKeyListener { viewEdit: View, keyCode: Int, keyEvent: KeyEvent ->

            search_cancel_button.visibility = View.VISIBLE
            search_cancel_button.setImageResource(R.drawable.cancel_button_grey)

            if ((keyEvent.action == KeyEvent.ACTION_DOWN) &&
                (keyCode == KeyEvent.KEYCODE_ENTER)
            ) {

                if (search_edittext_data.text.toString().trim().isBlank()) {
                    search_edittext_data.text.clear()
                    showNotFound()
                    return@setOnKeyListener true
                }

                paperLang = Paper.book().read<String>(PAPER_LANG_DEFAULT, LANG_RU)
                searchString = search_edittext_data.text.toString()
                var newSearch = ItemSavedSearch(searchString, paperLang)
                paperSavedList.add(0, newSearch)

                for (index in 0 until paperSavedList.size)
                    if (index > limit)
                        paperSavedList.removeAt(index)

                searchAdapter?.notifyDataSetChanged()

                Paper.book().write(PAPER_SAVED_LIST, paperSavedList)
                searchRequest(searchString, paperLang)
                true
            }
            false
        }

        search_cancel_button.setOnClickListener {

            search_edittext_data.text.clear()
            search_cancel_button.setImageResource(android.R.color.transparent)

            if (paperSavedList.size > 0) {
                showSearchHistory()
            } else {
                hideSearchHistory()
                search_cancel_button.setImageResource(android.R.color.transparent)
                search_image.setImageResource(R.drawable.group_4)
                search_start_find.text = getString(R.string.search_message_start)
            }
        }
    }

    fun searchRequest(strFind: String, lang: String) {
        //searchString = strFind
        progress_search.visibility = View.VISIBLE
        search_cancel_button.setImageResource(R.drawable.cancel_button_grey)

        viewModel.apiSearch(lang, strFind).observe(this,
            Observer { response ->
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {
                        updateList(response)
                        // searchSuccess = true
                    } else context?.toast(data?.message.toString())
                } else
                    context?.toast(response?.message().toString())

                progress_search?.visibility = View.GONE
            }
        )
    }

    var listPosition = 0
    fun savePosition(pos: Int) {
        listPosition = pos
    }

    fun updateList(response: Response<ListProductResponse>) {
        //progress_search.visibility = View.VISIBLE
        if (response.isSuccessful) {
            val data = response.body()
            notFoundFlag = false
            if (data?.error == false) {
                val list = data?.answer
                val size = list?.products?.size ?: 0

                if (size > 0) {

                    mainList.clear()
                    val newList = list?.products
                    newList?.let { newListData ->
                        for (newItem in newListData) {
                            val item = ItemProductCatalog(newItem?.name ?: "", R.drawable.product_1)
                            item.imageUrl = IMAGE_URL + newItem?.image
                            newItem?.discount?.let {
                                item.discount.dateEnd = newItem?.discount?.dateEnd
                                item.discount.dateStart = newItem?.discount?.dateStart
                                item.discount.price = newItem?.discount?.price
                            }
                            item.quantity = newItem?.quantity ?: 0
                            item.price = newItem?.price.toString()
                            item.subprice = newItem?.subprice ?: arrayListOf()
                            item.productId = newItem?.productId ?: 0
                            item.weight = ""
                            item.rating = newItem?.rew ?: 0.0f

                            if (newItem?.discount == null) {
                                item.salesEnabled = false
                            } else {
                                item.salesEnabled = true
                            }
                            //item.SubCategory = caption
                            mainList.add(item)
                        }
                        searchAdapter?.notifyDataSetChanged()

                        progress_search?.visibility = View.GONE

                        if (mainList.size == 0) {
                            hideSearchHistory()
                            search_image.setImageResource(R.drawable.search_notfound)
                            search_start_find.text = getString(R.string.search_message_not_found)
                        } else {
                            (activity as MainActivity).replaceFragment(
                                SearchResultFragment(mainList, searchString),
                                SEARCH_RESULT_FRAGMENT, false, searchString
                            )
                        }
                        (activity as MainActivity).hideKeyboard()
                    }
                } else {
                    showNotFound()
                }
            } else {
                showNotFound()
            }
        } else {
            showNotFound()
        }
    }

    fun showNotFound() {
        hideSearchHistory()
        search_image.setImageResource(R.drawable.search_notfound)
        search_start_find.text = getString(R.string.search_message_not_found)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (this is ISearchFragment) {
            listener = this
        } else {
            throw RuntimeException(context.toString() + " must implement ISearchFragment")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

}
