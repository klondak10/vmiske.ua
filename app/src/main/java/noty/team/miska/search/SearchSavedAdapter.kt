package noty.team.miska.search


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.search_saved_item.view.*
import noty.team.miska.model.ItemSavedSearch


const val CURRENCY = " грн"

interface ISearchFragment {
    fun clickSavedList(item: ItemSavedSearch)
}

class SearchSavedAdapter(
    val context: Context,
    private val mValues: List<ItemSavedSearch>,
    private val mListener: ISearchFragment?

) : RecyclerView.Adapter<SearchSavedAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ItemSavedSearch
            mListener?.clickSavedList(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view: View? = null

        view = LayoutInflater.from(parent.context)
            .inflate(noty.team.miska.R.layout.search_saved_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var item = mValues[position]
        holder.mText.text = item.stringSearch


        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }


    override fun getItemCount(): Int {
        return mValues.size
    }

    inner class ViewHolder(val mView: View) :
        RecyclerView.ViewHolder(mView) {
        val mText: TextView = mView.search_saved_string
    }
}
