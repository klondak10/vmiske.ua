package noty.team.miska.search


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_search.progress_search
import kotlinx.android.synthetic.main.fragment_search_result.*
import noty.team.miska.MainActivity
import noty.team.miska.PAPER_LANG_DEFAULT
import noty.team.miska.R
import noty.team.miska.catalog.CatalogProductFragment
import noty.team.miska.catalog.CatalogProductItemAdapter
import noty.team.miska.model.ItemProductCatalog
import noty.team.miska.viewmodel.DataViewModel
import noty.team.miska.viewmodel.LANG_RU


class SearchResultFragment(val mainList: MutableList<ItemProductCatalog>, val caption: String) :
    Fragment() {

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    private var listener: CatalogProductFragment.OnProductCatalogInteraction? = null

    var paperLang: String = LANG_RU
    var productAdapter: CatalogProductItemAdapter? = null

    var foundStrTitle = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_search_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progress_search.visibility = View.VISIBLE
        (activity as MainActivity).showCustomAppBar(caption, false, true)

        foundStrTitle = getString(R.string.found_products) + " " + mainList.size.toString() +" " + getString(R.string.found_producs_2)
        search_info_result.text = foundStrTitle

        paperLang = Paper.book().read<String>(PAPER_LANG_DEFAULT, LANG_RU)
        recycler_search?.layoutManager = GridLayoutManager(context, 2)

        productAdapter =
            CatalogProductItemAdapter(context!!, mainList, listener, false, paperLang)
            { savePosition(it) }
        recycler_search?.adapter = productAdapter
        progress_search.visibility = View.GONE

    }

    var listPosition = 0
    fun savePosition(pos: Int) {
        listPosition = pos
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is CatalogProductFragment.OnProductCatalogInteraction) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnMainCatalogListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

}
