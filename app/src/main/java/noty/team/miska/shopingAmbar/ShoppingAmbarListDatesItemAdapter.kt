package noty.team.miska.shopingAmbar

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_shopping_ambar_item.view.*
import noty.team.miska.R
import noty.team.miska.model.SelectedDate
import noty.team.miska.modelRetrofit.Ambar
import noty.team.miska.modelRetrofit.ProductAmbar
import noty.team.miska.util.addZeroToDate


interface OnShoppingAmbarListInteraction {
    fun onAmbarListClick(item: Ambar?)

}

class ShoppingAmbarListDatesItemAdapter(
    val context: Context,
    private val mValues: List<Ambar>,
    private val mListener: OnShoppingAmbarListInteraction?,
    var callback: (itemPos: Int, itemInfo: Ambar) -> Unit
) : RecyclerView.Adapter<ShoppingAmbarListDatesItemAdapter.ViewHolder>(),
    OnShoppingAmbarProductInteraction {
    var flagArrow = false

    override fun onAmbarProductClick(item: ProductAmbar?) {

    }

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Ambar
            mListener?.onAmbarListClick(item)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view: View? = null
        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_shopping_ambar_item, parent, false)


        return ViewHolder(view!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        holder.mNumber.text = "№ " + item.info?.orderId
//        //yyyy-mm-dd
        var date = item.info?.date?.split("-")
        var dateStr = ""
        date?.let { list ->
            if (list.size == 3) {
                dateStr = addZeroToDate(SelectedDate(list[2], list[1], list[0]))
            }
        }
        holder.mDate.text = dateStr
        //holder.mDate.text = item.info?.date


        holder.mComment.text = item.info?.txtForCustomer
        val status = item.info?.status ?: ""

        holder.mOrderStatus.text = status
        holder.mOrderStatus.setTextColor(Color.parseColor("#e24b17"))

        if (item.products?.size ?: 0 > 0) {

            var ambarList = item.products ?: ArrayList<ProductAmbar>()



            holder.mRecyclerSub.layoutManager = LinearLayoutManager(context)
            holder.mRecyclerSub.adapter = ShoppingAmbarProductItemAdapter(context, ambarList, this)
        }

        holder.mEditButton.setOnClickListener {
            callback(position, item)
        }



        holder.mDetailButton.setOnClickListener {
            if (flagArrow) {
                holder.mRecyclerLayout.visibility = View.GONE
                holder.mArrow.setBackgroundResource(R.drawable.arrow_down_red)
                flagArrow = false
            } else {
                holder.mRecyclerLayout.visibility = View.VISIBLE
                holder.mArrow.setBackgroundResource(R.drawable.arrow_up_red)

                flagArrow = true
            }
        }

        holder.mDetailArrowButton.setOnClickListener {
            holder.mDetailButton.performClick()
        }

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {

        val mNumber: TextView = mView.ambar_order_number
        val mDate: TextView = mView.ambar_delivery_date
        val mOrderStatus: TextView = mView.ambar_status
        val mRecyclerSub = mView.recycler_ambar_list_product_sub
        val mEditButton = mView.ambar_edit_new_date_button
        val mDetailButton = mView.shopping_date_detail_button
        val mDetailArrowButton = mView.ambar_detail_arrow
        val mRecyclerLayout = mView.recycler_ambar_add_date_layout
        val mArrow = mView.ambar_detail_arrow
        val mComment = mView.ambar_comment

    }
}
