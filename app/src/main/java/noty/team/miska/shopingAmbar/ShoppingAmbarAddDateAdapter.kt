package noty.team.miska.shopingAmbar

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_shopping_add_date_item.view.*
import kotlinx.android.synthetic.main.fragment_shopping_item_info_detail.view.ambar_date_product_price
import kotlinx.android.synthetic.main.fragment_shopping_item_info_detail.view.ambar_info_date_item_chip_weight
import kotlinx.android.synthetic.main.fragment_shopping_item_info_detail.view.ambar_info_date_item_count
import kotlinx.android.synthetic.main.fragment_shopping_item_info_detail.view.ambar_product_date_info_image
import kotlinx.android.synthetic.main.fragment_shopping_item_info_detail.view.ambar_product_date_info_name
import noty.team.miska.R
import noty.team.miska.basket.BasketOperations
import noty.team.miska.modelRetrofit.ProductAmbar
import noty.team.miska.search.CURRENCY
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.IMAGE_URL


class ShoppingAmbarAddDateAdapter(
    val context: Context,val typeEdit:Boolean,
    private val mValues: List<ProductAmbar>,
    private val mListener: OnAmbarInfoInteraction?,
    var callback: (ops: BasketOperations, itemPos: Int, item: ProductAmbar) -> Unit

) : RecyclerView.Adapter<ShoppingAmbarAddDateAdapter.ViewHolder>() {


    private val mOnClickListener: View.OnClickListener
    var listType: Boolean? = null

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ProductAmbar
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            //mListener?.onAmbarInfoClick(item)

        }
        this.listType = listType
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view: View? = null
        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_shopping_add_date_item, parent, false)
        return ViewHolder(view!!)
    }


    override fun onBindViewHolder(holder: ShoppingAmbarAddDateAdapter.ViewHolder, position: Int) {
        val item = mValues[position]

        var maxQuantity=0
        if (typeEdit){
            maxQuantity = (item.inOrder?:0)
        }
        else{
            maxQuantity = (item.inOrder?:0)-(item.inAmbar?:0)
        }

        item.quantity=maxQuantity

        if( (item.inOrder?:0)==(item.inAmbar?:0)){
            holder.mSelected.isEnabled=false
            item.enabled=false

            holder.mPlusButton.isEnabled=false

            holder.mMinusButton.isEnabled=false
            holder.mButtonsLayout.visibility=View.INVISIBLE
        }else{
            holder.mSelected.isEnabled=true
            item.enabled=true
        }


        holder.mQuantity.setOnClickListener {
            if (!holder.mPlusButton.isEnabled || !holder.mMinusButton.isEnabled) {
                context.toast(context.getString(R.string.you_cant_change_quantity))
                return@setOnClickListener
            }
        }
        val edIzm = context.getString(R.string.count_ed)

        holder.mSelected.setOnCheckedChangeListener { buttonView, isChecked ->
            item.selected = isChecked
        }
        holder.mSelected.isChecked = item.selected


        var imageUrl = IMAGE_URL + item.image
        Picasso.get()
            .load(imageUrl)
            .placeholder(noty.team.miska.R.drawable.placeholder_small)
            .error(noty.team.miska.R.drawable.placeholder_small)
            .into(holder.mImage)


        val newName = item.name?.replace("&quot;", "")
        val newName2 = newName?.replace("amp;", "")

        holder.mText.text = newName2
        item.price = item.price

        if (item.quantity == null) item.quantity = 0



        if (item.quantity >= 0) {
            val sum = (item.price ?: 0) * item.quantity
            holder.mPrice.text = sum.toString() + CURRENCY
            holder.mQuantity.text = item.quantity.toString() +" "+ edIzm
            //holder.mAmbarQuantity.text = item.quantity.toString()
            holder.mAmbarQuantity.text = item.quantity.toString()

        } else
            holder.mPrice.text = item.price.toString() + CURRENCY

        holder.mPlusButton.setOnClickListener {

            //if (item.quantity <= 1000) {
            if (item.quantity < maxQuantity) {
                item.quantity++
                callback(BasketOperations.PLUS, position, item)

                holder.mQuantity.text = item.quantity.toString()+" "+  edIzm
                holder.mAmbarQuantity.text = item.quantity.toString()

                holder.mMinusButton.setImageResource(R.drawable.stepper_minus_red)

                if (item.quantity > 0) {
                    var sum = (item.price ?: 0) * item.quantity
                    holder.mPrice.text = sum.toString() + CURRENCY
                } else
                    holder.mPrice.text =
                        item.price.toString() + CURRENCY
            }
        }

        holder.mMinusButton.setOnClickListener {


            if (item.quantity > 1) {
                item.quantity--

                holder.mQuantity.text = item.quantity.toString()+" "+ edIzm
                holder.mAmbarQuantity.text = item.quantity.toString()
                callback(BasketOperations.MINUS, position, item)

                if (item.quantity == 1) {
                    holder.mMinusButton.setImageResource(R.drawable.stepper_minus_grey)
                }
                if (item.quantity > 0) {
                    var sum = (item.price ?: 0) * item.quantity
                    holder.mPrice.text = sum.toString() + CURRENCY
                } else
                    holder.mPrice.text =
                        item.price.toString() + CURRENCY

            }
        }

        if (item.quantity == 1||item.quantity == 0) {
            holder.mMinusButton.setImageResource(R.drawable.stepper_minus_grey)
        } else {
            holder.mMinusButton.setImageResource(R.drawable.stepper_minus_red)
        }

        if (item.quantity > maxQuantity) {
            holder.mPlusButton.isEnabled=false
        }


        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mImage: ImageView = mView.ambar_product_date_info_image
        val mText: TextView = mView.ambar_product_date_info_name
        val mPrice: TextView = mView.ambar_date_product_price
        val mWeight = mView.ambar_info_date_item_chip_weight
        val mQuantity = mView.ambar_info_date_item_count
        val mSelected = mView.selected_new_date
        val mMinusButton: ImageButton = mView.date_minus_button
        val mPlusButton: ImageButton = mView.date_plus_button
        val mAmbarQuantity: TextView = mView.ambar_product_quantity
        val mButtonsLayout = mView.button_plus_minus


        override fun toString(): String {
            return super.toString() + " '" + mText.text + "'"
        }
    }


}