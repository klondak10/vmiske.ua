package noty.team.miska.shopingAmbar

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_shopping_ambar_item_sub.view.*
import noty.team.miska.R
import noty.team.miska.modelRetrofit.ProductAmbar
import noty.team.miska.search.CURRENCY
import noty.team.miska.viewmodel.IMAGE_URL


interface OnShoppingAmbarProductInteraction {
    fun onAmbarProductClick(item: ProductAmbar?)
}

class ShoppingAmbarProductItemAdapter(val context: Context,
    private val mValues: List<ProductAmbar>,
    private val mListener: OnShoppingAmbarProductInteraction?
) : RecyclerView.Adapter<ShoppingAmbarProductItemAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ProductAmbar
            mListener?.onAmbarProductClick(item)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view: View? = null
        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_shopping_ambar_item_sub, parent, false)
        return ViewHolder(view!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        var imageUrl = IMAGE_URL + item.image
        Picasso.get()
            .load(imageUrl)
            .placeholder(noty.team.miska.R.drawable.placeholder_small)
            .error(noty.team.miska.R.drawable.placeholder_small)
            .into(holder.mImage)


        var newName = item.name?.replace("&quot;", "")
        var newName2 = newName?.replace("amp;", "")

        holder.mName.text = newName2
        //holder.mAmbarChip.text = ""
        holder.mAmbarChip.visibility = View.INVISIBLE
        val odIzm=context.resources.getString(R.string.count_ed)

        holder.mQuantity.text = item.quantity.toString() + " " +odIzm
        var sum = item.price ?: 0 * (item.quantity ?: 0)
        holder.mSum.text = sum.toString() + CURRENCY

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {

        val mImage = mView.ambar_product_date_info_image
        val mName = mView.ambar_product_date_info_name
        val mAmbarChip = mView.ambar_info_date_item_chip_weight
        val mQuantity: TextView = mView.ambar_info_date_item_count
        val mSum: TextView = mView.ambar_sum_price
    }
}
