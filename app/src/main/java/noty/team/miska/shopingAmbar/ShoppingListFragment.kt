package noty.team.miska.shopingAmbar


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_shopping_list.*
import noty.team.miska.MainActivity
import noty.team.miska.PAPER_LANG_DEFAULT
import noty.team.miska.R

import noty.team.miska.SHOPPING_ITEM_FRAGMENT
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.modelRetrofit.AnswerOrders
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import noty.team.miska.viewmodel.LANG_RU


interface OnShoppingListInteraction {
    fun onShoppingListClick(item: AnswerOrders?)
    fun onShoppingListShowEmpty()
}

class ShoppingListFragment : Fragment(),
    OnShoppingListInteraction {
    override fun onShoppingListClick(item: AnswerOrders?) {
        (activity as MainActivity).replaceFragment(
            ShoppingItemInfoFragment(item?.ordId.toString()),
            SHOPPING_ITEM_FRAGMENT,
            false,
            ""
        )
    }

    override fun onShoppingListShowEmpty() {

    }

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }


    private var listener: OnShoppingListInteraction? = null


    var mainList: MutableList<AnswerOrders> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shopping_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener = this
        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_shopping),
            false,
            true
        )

        mainList.clear()
        recycler_shopping_list?.layoutManager = LinearLayoutManager(context)
        recycler_shopping_list?.adapter = ShoppingListItemAdapter(
            mainList, listener,
            ({ i: Int, answerOrders: AnswerOrders ->

                (activity as MainActivity).replaceFragment(
                    ShoppingItemInfoFragment(answerOrders.ordId.toString()),
                    SHOPPING_ITEM_FRAGMENT,
                    false,
                    ""
                )
            })
        )

        var paperLang = Paper.book().read(PAPER_LANG_DEFAULT, LANG_RU)

        var token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token

            viewModel.apiGetOrders(token, paperLang).observe(this, Observer { response ->
                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {

                        var list = (data.answer ?: arrayListOf())
                        //remover some  orders
                        val it = list.iterator()
                        while (it.hasNext()) {
                            val foo = it.next()
                            if ((foo.orderStatus == 1 && foo.payment_code == "liqpay")) it.remove()
                        }


                        var newList = list.sortedByDescending { item -> item.date ?: "" }

                        mainList.clear()
                        mainList.addAll(newList)
                        recycler_shopping_list.adapter?.notifyDataSetChanged()
                        if (mainList.size == 0)
                            (activity as MainActivity).replaceFragment(
                                ShoppingEmptyFragment(),
                                SHOPPING_ITEM_FRAGMENT,
                                false,
                                ""
                            )
                    } else
                        context?.toast(data?.message.toString())
                } else
                    context?.toast(response?.message().toString())
            })
        }
    }
}
