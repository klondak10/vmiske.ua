package noty.team.miska.shopingAmbar

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_shopping_item.view.*
import noty.team.miska.R
import noty.team.miska.modelRetrofit.AnswerOrders
import noty.team.miska.util.formatDate


class ShoppingListItemAdapter(
    private val mValues: List<AnswerOrders>,
    private val mListener: OnShoppingListInteraction?,
    var callback: (itemPos: Int, orderInfo: AnswerOrders) -> Unit
) : RecyclerView.Adapter<ShoppingListItemAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as AnswerOrders
            mListener?.onShoppingListClick(item)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view: View? = null
        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_shopping_item, parent, false)


        return ViewHolder(view!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        holder.mNumber.text = "№ " + item.ordId
        holder.mDate.text = formatDate(item.date)

        var newTotal = (item.total?.split("."))
        if (newTotal?.size ?: 0 > 0)
            holder.mOrderSum.text = newTotal!![0] + " грн"
        else
            holder.mOrderSum.text = item.total + " грн"

        if (item.ambar == 1) {
            holder.mAmbarChip.visibility = View.VISIBLE
            holder.mAmbarChip.setOnClickListener {
                callback(position, item)
            }


        } else {
            holder.mAmbarChip.visibility = View.GONE
        }

        holder.mOrderStatus.text = item.status
        holder.mOrderStatus.setTextColor(Color.parseColor("#e24b17"))
        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {

        val mNumber: TextView = mView.shopping_number
        val mDate: TextView = mView.shopping_date
        val mOrderSum: TextView = mView.shopping_info_date_send_data
        val mOrderStatus: TextView = mView.shopping_order_status_data
        val mAmbarChip = mView.ambar_chip
    }
}
