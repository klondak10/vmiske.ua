package noty.team.miska.shopingAmbar


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_shopping_ambar_list_dates.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.login.SHOPPING_EMPTY_FRAGMENT
import noty.team.miska.modelRetrofit.*
import noty.team.miska.util.removeZeros
import noty.team.miska.viewmodel.DataViewModel

class ShoppingAmbarListDatesFragment(
    var newList: ArrayList<ProductOrderInfo>,
    var orderInfo: CheckoutOrderInfoResponse
) : Fragment(), OnShoppingAmbarListInteraction {

    override fun onAmbarListClick(item: Ambar?) {
    }

    var mainList = arrayListOf<Ambar>()

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }
    var listener: OnShoppingAmbarListInteraction? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_shopping_ambar_list_dates, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            "",
            true,
            false
        )
        listener = this
        ambar_add_date_list_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        ambar_save_new_date_button.setOnClickListener {
            var products = orderInfo.answer?.get(0)?.products ?: arrayListOf()

            var newAmbarItem = Ambar()

            var newInfo = InfoAmbar()
            newInfo.orderId = orderInfo.answer?.get(0)?.ordId
            newAmbarItem.info = newInfo

            var newProducts: ArrayList<ProductAmbar> = arrayListOf()
            for (item in products) {
                var newProductAmbar = ProductAmbar()
                var price = item.price ?: "0"
                newProductAmbar.price = (removeZeros(price)).toInt()

                newProductAmbar.productId = item.productId.toString()
                newProductAmbar.name = item.name
                newProductAmbar.image = item.image
                newProductAmbar.quantity = item.quantity
                //newProductAmbar. = item

                newProducts.add(newProductAmbar)
            }
            newAmbarItem.products = newProducts
            var caption = getString(R.string.add_date_ambar)
            (activity as MainActivity).replaceFragment(
                ShoppingAmbarAddDateFragment(caption, false, newAmbarItem, orderInfo),
                SHOPPING_EMPTY_FRAGMENT,
                false,
                ""
            )
        }


        var ambarList = orderInfo.answer?.get(0)?.ambars ?: ArrayList<Ambar>()


        if (ambarList.size == 0) {
            (activity as MainActivity).replaceFragment(
                ShoppingEmptyFragment(),
                SHOPPING_EMPTY_FRAGMENT,
                false,
                getString(R.string.toolbar_name_basket)
            )
        }

        recycler_ambar_list_dates?.layoutManager = LinearLayoutManager(context)

        var caption = getString(R.string.edit_date)

        var newAmbarList = ambarList.sortedByDescending { item -> item.info?.date ?: "" }

        recycler_ambar_list_dates?.adapter =
            ShoppingAmbarListDatesItemAdapter(
                context!!, newAmbarList, listener,
                ({ i: Int, ambar: Ambar ->
                    //edit button -ambar
                    (activity as MainActivity).replaceFragment(
                        ShoppingAmbarAddDateFragment(caption, true, ambar, orderInfo),
                        SHOPPING_EMPTY_FRAGMENT,
                        false,
                        ""
                    )
                })
            )
    }
}
