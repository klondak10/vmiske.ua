package noty.team.miska.shopingAmbar


import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.text.InputType.TYPE_NULL
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_shopping_ambar_add_date.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.basket.BasketOperations
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.model.SelectedDate
import noty.team.miska.modelRetrofit.Ambar
import noty.team.miska.modelRetrofit.CheckoutOrderInfoResponse
import noty.team.miska.modelRetrofit.ProductAmbar
import noty.team.miska.modelRetrofit.ProductOrderInfo
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.util.*
import noty.team.miska.viewmodel.DataViewModel
import java.util.*
import java.util.regex.Pattern


class ShoppingAmbarAddDateFragment(
    var caption: String, var typeEditDate: Boolean,
    var newAmbarItem: Ambar, var orderInfo: CheckoutOrderInfoResponse
) : Fragment(),
    OnAmbarInfoInteraction {

    var mainList = arrayListOf<ProductAmbar>()
    var items: ArrayList<Int> = arrayListOf()
    var selectedDate = SelectedDate()
    var selectedAmbardDate: SelectedDate? = SelectedDate()

    override fun onAmbarInfoClick(item: ProductOrderInfo?) {

    }

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }
    var listener: OnAmbarInfoInteraction? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_shopping_ambar_add_date, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            caption,
            true,
            false
        )
        listener = this

        shopping_info_caption.text = caption

        //get all products from order info
        mainList.clear()
//
        var products = orderInfo.answer?.get(0)?.products ?: arrayListOf()
        for (item in products) {
            var newProductAmbar = ProductAmbar()
            var price = item.price ?: "0"
            newProductAmbar.price = (removeZeros(price)).toInt()
            newProductAmbar.productId = item.productId.toString()
            newProductAmbar.name = item.name
            newProductAmbar.image = item.image
            newProductAmbar.quantity = item.quantity
            newProductAmbar.orderProductId = item.orderProductId.toString()
            newProductAmbar.inAmbar = item.inAmbar
            newProductAmbar.inOrder = item.quantity
            mainList.add(newProductAmbar)
        }

        //fill data
        var comment = newAmbarItem.info?.txtForCustomer ?: ""
        //var date = orderInfo.answer?.get(0)?.date ?: ""
        var date = newAmbarItem.info?.date ?: ""
        //2019-12-21 18:31:00
        //"2019-11-30" -> 30.11.2019

        if (typeEditDate)
            ambar_new_date_edit.setText(formatDateAmbar(date))
        else {
            val today = Calendar.getInstance()
            val todayStr = SelectedDate()
            todayStr.day = today.get(Calendar.DATE).toString()
            todayStr.month = (today.get(Calendar.MONTH) + 1).toString()
            todayStr.year = (today.get(Calendar.YEAR)).toString()

            ambar_new_date_edit.setText(addZeroToDate(todayStr))

        }
        selectedAmbardDate = returnDateAmbar(date)

        ambar_new_date_edit.isEnabled = true
        ambar_new_date_edit.inputType = TYPE_NULL
        ambar_new_date_edit.isFocusableInTouchMode = false

        ambar_new_comment.setText(comment)

        //select only products from ambar
        var selectedAmbar = newAmbarItem.products ?: arrayListOf()

        for (itemProduct in mainList) {

            for (itemAmbar in selectedAmbar) {
                if (itemProduct.orderProductId == itemAmbar.orderProductId) {
                    itemProduct.selected = true
                    itemProduct.quantity = itemAmbar.quantity
                }
            }
        }
        ambar_add_date_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        var token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
        }

        ambar_cancel_new_date_button.setOnClickListener {
            (activity as MainActivity).replaceFragment(ShoppingListFragment(), "", false, "")
        }

        //if (!typeEditDate)
        if (Build.VERSION.SDK_INT > 23) {
            var datePickerDialog: DatePickerDialog? = null
            ambar_new_date_edit.isEnabled = true
            ambar_new_date_edit.inputType = TYPE_NULL
            ambar_new_date_edit.isFocusableInTouchMode = false
            ambar_new_date_edit.setOnClickListener {
                (activity as MainActivity).hideKeyboard()
                datePickerDialog = DatePickerDialog(context!!)
                datePickerDialog?.setOnDateSetListener { view, year, month, dayOfMonth ->

                    var day = dayOfMonth.toString()
                    if (day.length == 1) day = "0" + day

                    selectedDate.day = day
                    var month = (month + 1).toString()
                    if (month.length == 1) month = "0" + month

                    selectedDate.month = month
                    selectedDate.year = year.toString()

                    val datePick =
                        day + "." + month + "." + year.toString()
                    ambar_new_date_edit.setText(datePick)
                }
                datePickerDialog?.datePicker?.minDate = System.currentTimeMillis()
                datePickerDialog?.setTitle(getString(R.string.select_date_delivery))
                datePickerDialog?.show()
            }
        }
        recycler_ambar_add_date_list?.layoutManager = LinearLayoutManager(context)
        recycler_ambar_add_date_list?.adapter =
            ShoppingAmbarAddDateAdapter(
                context!!, typeEditDate, mainList, listener,
                ({ basketOperations: BasketOperations, pos: Int, productOrderInfo: ProductAmbar ->

                    if (basketOperations == BasketOperations.PLUS || basketOperations == BasketOperations.MINUS)
                        context?.toast(getString(R.string.products_count_changed))
                })
            )
//apiAddToAmbar
//new
        ambar_save_new_date_button.setOnClickListener {
            var flagSelected = false

            for (item in mainList)
                if (item.selected) {
                    flagSelected = true
                    break
                }
            if (!flagSelected) {
                context?.toast(getString(R.string.select_product))
                return@setOnClickListener
            }

            val newDate = ambar_new_date_edit.text.toString()
            if (newDate.trim().isEmpty()) {
                context?.toast(getString(R.string.input_date))
                return@setOnClickListener
            }
            val comment = ambar_new_comment.text.toString()
            if (comment.trim().isEmpty()) {
                context?.toast(getString(R.string.input_comment))
                return@setOnClickListener
            }

            var regexDate = "[0-9]{1,2}(.)[0-9]{1,2}(.)[0-9]{4}"
            val pattern1 =
                Pattern.compile(regexDate)

            val matcher1 = pattern1.matcher(newDate)

            if (!matcher1.matches()) {
                context?.toast(getString(R.string.input_correct_date))
                return@setOnClickListener
            }


            var token = Paper.book().read(PAPER_TOKEN, "")
            if (token == "") return@setOnClickListener

            token = TOKEN_PREFIX + token
            items.clear()

            val orderId = (orderInfo.answer?.get(0)?.ordId ?: 0).toString()
            var newDateStr =
                selectedAmbardDate?.year + "-" + selectedAmbardDate?.month + "-" + selectedAmbardDate?.day
            var newComment = ambar_new_comment.text.toString()
            //    //{"1016":1}
            //find orderProductId
            var prStr = "{"

            //count selected item
            var selected: ArrayList<String> = arrayListOf()

            for (itemIndex in 0 until mainList.size) {
                if (mainList[itemIndex].selected && mainList[itemIndex].enabled) {
                    selected.add(mainList[itemIndex].orderProductId)
                }
            }

            var count = 0
            for (itemIndex in 0 until mainList.size) {
                if (mainList[itemIndex].selected && mainList[itemIndex].enabled) {
                    count++
                    //{"1016":1,"1017":2}
                    var productId = mainList[itemIndex].orderProductId ?: 0
                    prStr =
                        prStr + "\"" + productId + "\":" + mainList[itemIndex].quantity.toString()

                    if (selected.size > 1 && count < selected.size)
                        prStr += ","
                }
            }
            prStr += "}"


            if (!typeEditDate) {
                if (selected.size > 0) {
                    viewModel.apiAddToAmbar(orderId, token, newDateStr, newComment, prStr)
                        .observe(this, androidx.lifecycle.Observer { response ->
                            if (response.isSuccessful) {
                                val data = response.body()
                                if (data?.error == false) {
                                    // {"answer":true,"error":false,"message":null}
                                    if (data.answer == true) {
                                        context?.toast(getString(R.string.product_added_ambar))
                                        (activity as MainActivity).replaceFragment(
                                            ShoppingListFragment(),
                                            "",
                                            false,
                                            ""
                                        )

                                    }
                                } else {
                                    context?.toast(data?.message.toString())
                                }
                            } else
                                context?.toast(response?.message().toString())
                        })
                } else {
                    context?.toast(getString(R.string.you_cant_add_product))
                }
            } else {
                //edit mode
                var ambarId = (newAmbarItem.info?.ambarId ?: "").toString()
                //get list product
                prStr = "{"
                var count = 0
                for (itemIndex in 0 until mainList.size) {
                    if (mainList[itemIndex].selected) {

                        if (count > 0) {
                            prStr += ","
                        }
                        count++
                        //{"1016":1,"1017":2}
                        var productId = mainList[itemIndex].orderProductId ?: 0
                        prStr =
                            prStr + "\"" + productId + "\":" + mainList[itemIndex].quantity.toString()
                    }
                }
                prStr += "}"
                //selectedDate
                if (selectedDate.day != "") {
                    newDateStr =
                        selectedDate.year + "-" + selectedDate.month + "-" + selectedDate.day
                } else {
                    var date1 = date
                    newDateStr = date1

                }
                //comment
                //newComment=


                viewModel.apiUpdateToAmbar(orderId, token, newDateStr, ambarId, prStr, newComment)
                    .observe(this, androidx.lifecycle.Observer { response ->
                        if (response.isSuccessful) {
                            val data = response.body()
                            if (data?.error == false) {
                                // {"answer":true,"error":false,"message":null}
                                if (data.answer == true) {
                                    context?.toast(getString(R.string.ambar_changed))
                                    (activity as MainActivity).replaceFragment(
                                        ShoppingListFragment(),
                                        "",
                                        false,
                                        ""
                                    )

                                }
                            } else {
                                context?.toast(data?.message.toString())
                            }
                        } else
                            context?.toast(response?.message().toString())
                    })
            }
        }
    }

}
