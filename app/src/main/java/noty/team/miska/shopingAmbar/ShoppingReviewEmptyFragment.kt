package noty.team.miska.shopingAmbar


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_shopping_ambar_review_empty.*
import noty.team.miska.MainActivity
import noty.team.miska.R
import noty.team.miska.login.SHOPPING_EMPTY_FRAGMENT
import noty.team.miska.modelRetrofit.*
import noty.team.miska.util.removeZeros

class ShoppingReviewEmptyFragment(
    var newList: ArrayList<ProductOrderInfo>,
    var orderInfo: CheckoutOrderInfoResponse
) : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_shopping_ambar_review_empty, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            "",
            true,
            false
        )

        ambar_review_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        ambar_add_date_empty_button.setOnClickListener {

            var products = orderInfo.answer?.get(0)?.products ?: arrayListOf()

            var newAmbarItem = Ambar()

            var newInfo = InfoAmbar()
            newInfo.orderId = orderInfo.answer?.get(0)?.ordId
            newAmbarItem.info = newInfo

            var newProducts: ArrayList<ProductAmbar> = arrayListOf()
            for (item in products) {
                var newProductAmbar = ProductAmbar()
                var price = item.price ?: "0"
                newProductAmbar.price = (removeZeros(price)).toInt()

                newProductAmbar.productId = item.productId.toString()
                newProductAmbar.name = item.name
                newProductAmbar.image = item.image
                newProductAmbar.quantity = item.quantity
                newProducts.add(newProductAmbar)
            }
            newAmbarItem.products = newProducts

            (activity as MainActivity).replaceFragment(
                ShoppingAmbarAddDateFragment(getString(R.string.add_date_ambar),false,newAmbarItem, orderInfo),
                SHOPPING_EMPTY_FRAGMENT,
                false,
                ""
            )
        }

    }
}
