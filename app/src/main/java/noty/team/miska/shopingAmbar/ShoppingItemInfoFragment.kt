package noty.team.miska.shopingAmbar


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_catalog_product_info_pager.*
import kotlinx.android.synthetic.main.fragment_shopping_item_info.*
import noty.team.miska.*
import noty.team.miska.basket.BasketProductFragment
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.login.SHOPPING_AMBAR_LIST_FRAGMENT
import noty.team.miska.login.SHOPPING_EMPTY_FRAGMENT
import noty.team.miska.model.BasketItemInfo
import noty.team.miska.model.ItemProductCatalog
import noty.team.miska.modelRetrofit.CheckoutOrderInfoResponse
import noty.team.miska.modelRetrofit.ProductInfoResponse
import noty.team.miska.modelRetrofit.ProductOrderInfo
import noty.team.miska.product.CatalogProductInfoPagerFragment
import noty.team.miska.product.ProductPagerAdapter
import noty.team.miska.profile.TOKEN_PREFIX
import noty.team.miska.search.CURRENCY
import noty.team.miska.util.formatDate
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import noty.team.miska.viewmodel.LANG_RU


interface OnAmbarInfoInteraction {
    fun onAmbarInfoClick(item: ProductOrderInfo?)
}


class ShoppingItemInfoFragment(var orderId: String) : Fragment(), OnAmbarInfoInteraction {

    var mainList = arrayListOf<ProductOrderInfo>()
    var orderInfo: CheckoutOrderInfoResponse = CheckoutOrderInfoResponse()

    override fun onAmbarInfoClick(item: ProductOrderInfo?) {

    }

    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }
    var listener: OnAmbarInfoInteraction? = null
    var ambarInfo = 0
    var ambarsSize = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_shopping_item_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).showCustomAppBar(
            "",
            true,
            false
        )
        //fragment_shopping_ambar_review_empty
        listener = this
        shopping_info_buy_repeat.setOnClickListener {

            mainList.forEachIndexed { i, element ->

                viewModel?.apiGetProductInfo("", element.productId.toString())
                    ?.observe(this, Observer {
                            response ->
                            if (response.isSuccessful) {
                                val data = response.body()
                                if (data?.error == false) {
                                    val list = data?.answer

                                    if (list!!.info!!.subprice.isNullOrEmpty()){

                                        viewModel.apiAddBasket(
                                            "token "+Paper.book().read(PAPER_TOKEN, ""), element.quantity.toString(), (element.productId ?: 0).toString(),
                                            null,
                                            null,
                                            null,
                                            null,
                                            null

                                        ).observe(this, Observer { response ->
                                            if (response.isSuccessful) {
                                                val data = response.body()
                                                if (data?.error == false) {
                                                    if (data.answer == true) {
                                                        context?.toast(getString(R.string.product_added_basket))
                                                        (activity as MainActivity).getBasketFromServer(
                                                            false,
                                                            ({
                                                                (activity as MainActivity).replaceFragment(
                                                                    BasketProductFragment(),
                                                                    BASKET_FRAGMENT,
                                                                    false,
                                                                    getString(R.string.toolbar_name_basket)

                                                                )
                                                            })
                                                        )


                                                    } else {
                                                        context?.toast(getString(R.string.product_not_add_bsaket))
                                                    }
                                                } else
                                                    context?.toast(data?.message.toString())
                                            } else
                                                context?.toast(response?.message().toString())


                                        })
                                    }else {
                                        list!!.info!!.subprice!!.forEachIndexed { j, subprice ->

                                            if (element.model.toString().contains(subprice!!.artikul.toString())) {

                                                viewModel.apiAddBasket(
                                                    "token " + Paper.book().read(PAPER_TOKEN, ""),
                                                    element.quantity.toString(),
                                                    (element.productId ?: 0).toString(),
                                                    subprice.povId.toString(),
                                                    subprice.poId.toString(),
                                                    subprice.name,
                                                    subprice.value,
                                                    subprice.price.toString()

                                                ).observe(this, Observer { response ->
                                                    if (response.isSuccessful) {
                                                        val data = response.body()
                                                        if (data?.error == false) {
                                                            if (data.answer == true) {
                                                                context?.toast(getString(R.string.product_added_basket))
                                                                (activity as MainActivity).getBasketFromServer(
                                                                    false,
                                                                    ({
                                                                        (activity as MainActivity).replaceFragment(
                                                                            BasketProductFragment(),
                                                                            BASKET_FRAGMENT,
                                                                            false,
                                                                            getString(R.string.toolbar_name_basket)

                                                                        )
                                                                    })
                                                                )


                                                            } else {
                                                                context?.toast(getString(R.string.product_not_add_bsaket))
                                                            }
                                                        } else
                                                            context?.toast(data?.message.toString())
                                                    } else
                                                        context?.toast(response?.message().toString())


                                                })


                                            }
                                        }
                                    }

                                }
                            }
                    })

            }

        }

        shopping_info_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        if (ambarInfo == 0)
            bottom_ambar_sheet?.visibility = View.GONE
        else
            bottom_ambar_sheet?.visibility = View.VISIBLE
        ambar_review_arrow_button.setOnClickListener {
            ambar_review_button.performClick()
        }

        ambar_review_button.setOnClickListener {

            if (ambarsSize) {

                (activity as MainActivity).replaceFragment(
                    ShoppingAmbarListDatesFragment(mainList, orderInfo),
                    SHOPPING_AMBAR_LIST_FRAGMENT,
                    false,
                    ""
                )
            } else {

                (activity as MainActivity).replaceFragment(
                    ShoppingReviewEmptyFragment(mainList, orderInfo),
                    SHOPPING_AMBAR_LIST_FRAGMENT,
                    false,
                    ""
                )
                //
            }
        }

        shopping_info_caption.text =
            getString(R.string.order_number) + orderId

        var paperLang = Paper.book().read(PAPER_LANG_DEFAULT, LANG_RU)
        var token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
            viewModel.apiGetOrderInfo(
                token, orderId, paperLang
            ).observe(this, Observer { response ->
                if (response.isSuccessful) {
                    val data = response.body()

                    if (data?.error == false) {
                        orderInfo = response.body() ?: CheckoutOrderInfoResponse()

                        shopping_info_status_data.text = data.answer?.get(0)?.status ?: ""


                        shopping_info_date_send_data.text =
                            formatDate(data.answer?.get(0)?.date ?: "")



                        ambarInfo = data.answer?.get(0)?.ambar ?: 0

                        if ((data.answer?.get(0)?.ambars ?: arrayListOf()).size > 0)
                            ambarsSize = true else
                            false

                        if (ambarInfo == 0)
                            bottom_ambar_sheet?.visibility = View.GONE
                        else
                            bottom_ambar_sheet?.visibility = View.VISIBLE

                        if (data.answer?.get(0)?.ambar ?: 0 == 1)

                            ambar_info_chip.visibility = View.VISIBLE
                        else
                            ambar_info_chip.visibility = View.GONE

                        shopping_status_delivery_caption_bottom.text = data.answer?.get(0)?.delivery ?: ""
                        shopping_info_address_bottom.text = data.answer?.get(0)?.address ?: ""
                        shopping_info_payment_bottom.text = data.answer?.get(0)?.paytype ?: ""

                        val newTotal =
                            noty.team.miska.util.removeZeros(data.answer?.get(0)?.total ?: "")
                        shopping_info_total_payment_bottom_data.text = newTotal + CURRENCY
                        for(i in data.answer?.get(0)?.subinfo!!){
                            if(i.title!!.contains("Бонус")){
                                summ.visibility = View.VISIBLE
                                bonuses.visibility = View.VISIBLE
                                price_bonuse.text = i.value.toString()
                                name_bonuse.text = i.title.toString().replace("ы","")

                                price_summ.text = data.answer!!.get(0)!!.subinfo!!.get(0).value.toString()
                                name_summ.text = data.answer!!.get(0)!!.subinfo!!.get(0).title.toString()
                                 break
                            }else{
                                summ.visibility = View.GONE
                                bonuses.visibility = View.GONE
                            }
                        }
                        for(i in data.answer?.get(0)?.subinfo!!){
                            if(i.title!!.contains("Промокод")){
                                summ.visibility = View.VISIBLE
                                llPromoCode.visibility = View.VISIBLE
                                tvPromoCodePrice.text = i.value.toString()
                                tvPromoCodeTitle.text = i.title.toString().replace("ы","")

                                price_summ.text = data.answer!!.get(0)!!.subinfo!!.get(0).value.toString()
                                name_summ.text = data.answer!!.get(0)!!.subinfo!!.get(0).title.toString()
                                break
                            }else{
                                summ.visibility = View.GONE
                                llPromoCode.visibility = View.GONE
                            }
                        }
                        for(i in data.answer?.get(0)?.subinfo!!) {
                            if (i.title!!.contains("Бонус") || i.title!!.contains("Промокод")) {
                                summ.visibility = View.VISIBLE
                                break
                            } else {
                                summ.visibility = View.GONE
                            }
                        }

                        recycler_ambar_list?.layoutManager = LinearLayoutManager(context!!)
                        mainList = data.answer?.get(0)?.products ?: arrayListOf()
                        recycler_ambar_list?.adapter = ShoppingItemInfoAdapter(context!!,mainList, listener) {

                            Paper.book().write("roductID",it.toString())
                            val fragment = CatalogProductInfoPagerFragment("", null)
                            (activity as MainActivity).replaceFragment(fragment, CATALOG_PRODUCT_PAGER_INFO, false, "")
                        }
                    } else
                        context?.toast(data?.message.toString())
                } else
                    context?.toast(response?.message().toString())
            })
        }
    }

}
