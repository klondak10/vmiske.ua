package noty.team.miska.shopingAmbar

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_shopping_item_info_detail.view.*
import noty.team.miska.R
import noty.team.miska.catalog.CatalogProductItemAdapter
import noty.team.miska.modelRetrofit.ProductOrderInfo
import noty.team.miska.search.CURRENCY
import noty.team.miska.viewmodel.IMAGE_URL


class ShoppingItemInfoAdapter(
    val context: Context,
    private val mValues: List<ProductOrderInfo>,
    private val mListener: OnAmbarInfoInteraction?,
    var callback: (position: Int) -> Unit
) : RecyclerView.Adapter<ShoppingItemInfoAdapter.ViewHolder>() {


    private val mOnClickListener: View.OnClickListener
    var listType: Boolean? = null

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ProductOrderInfo
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onAmbarInfoClick(item)
            callback(item.productId!!)



        }
        this.listType = listType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view: View? = null
        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_shopping_item_info_detail, parent, false)
        return ViewHolder(view!!,
            {
                callback(it)
            }
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        var imageUrl = IMAGE_URL + item.image
        Picasso.get()
            .load(imageUrl)
            .placeholder(noty.team.miska.R.drawable.placeholder_small)
            .error(noty.team.miska.R.drawable.placeholder_small)
            .into(holder.mImage)


        val newName = item.name?.replace("&quot;", "")
        val newName2 = newName?.replace("amp;", "")
        holder.mText.text = newName2
        var price = noty.team.miska.util.removeZeros(item.price ?: "")
        holder.mPrice.text = (price.toInt() * item.quantity).toString() + CURRENCY
        val odIzm=context.resources.getString(R.string.count_ed)
        holder.mCount.text = item.quantity.toString() + " "+ odIzm

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View, var callback: (position: Int) -> Unit) :
        RecyclerView.ViewHolder(mView) {

        val mImage: ImageView = mView.ambar_product_date_info_image
        val mText: TextView = mView.ambar_product_date_info_name

        val mPrice: TextView = mView.ambar_date_product_price
        val mWeight = mView.ambar_info_date_item_chip_weight
        val mCount = mView.ambar_info_date_item_count


        override fun toString(): String {
            return super.toString() + " '" + mText.text + "'"
        }
    }


}