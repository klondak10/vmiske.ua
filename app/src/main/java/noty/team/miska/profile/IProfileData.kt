package noty.team.miska.profile

import noty.team.miska.model.UserProfile

interface IProfileData {
    fun setProfileData(userProfile: UserProfile?)
    fun getProfileData():UserProfile?
}