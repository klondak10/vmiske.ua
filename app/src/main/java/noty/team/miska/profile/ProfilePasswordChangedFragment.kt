package noty.team.miska.profile


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_profile_password_changed.*
import noty.team.miska.MainActivity
import noty.team.miska.PROFILE_FRAGMENT
import noty.team.miska.R


class ProfilePasswordChangedFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_profile_password_changed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar("", true, false)

        profile_password_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        profile_password_changed_back.setOnClickListener {
            (activity as MainActivity)
                .replaceFragment(
                    ProfileFragment(),
                    PROFILE_FRAGMENT,
                    false,
                    ""
                )
        }
    }

}
