package noty.team.miska.profile


import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_profile.*
import noty.team.miska.*
import noty.team.miska.catalog.CatalogMainFragment
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.model.UserProfile
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel


const val TOKEN_PREFIX = "token "

class ProfileFragment : Fragment() {


    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }
    var token = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar(
            getString(R.string.toolbar_name_personal_data),
            false,
            false
        )
        (activity as MainActivity).showUserEditProfileMenu(true)

        profile_exit_account_button.setOnClickListener {

            var dialog = AlertDialog.Builder(activity)
            dialog.setNegativeButton(getString(R.string.cancel)) { dialog, which ->
                dialog.dismiss()
            }
            dialog.setPositiveButton(getString(R.string.message_yes)) { alert, which ->

                run {
                    alert.dismiss()
                    Paper.book().write(PAPER_USER_LOGGED, false)
                    Paper.book().write(PAPER_TOKEN, "")

                    //facebook signout
                    LoginManager.getInstance().logOut()
                    //google signout
                    var accountGoogle = GoogleSignIn.getLastSignedInAccount(context)
                    if (accountGoogle != null) {

                        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .requestEmail()
                            .build()
                        val mGoogleSignInClient = GoogleSignIn.getClient(context!!, gso)

                        mGoogleSignInClient.revokeAccess()
                            .addOnCompleteListener(activity!!, object : OnCompleteListener<Void> {
                                override fun onComplete(p0: Task<Void>) {
                                }
                            })
                    }

                    (activity as MainActivity).clearBasket()
                    (activity as MainActivity).showUserMenu(false, "")
                    (activity as MainActivity).replaceFragment(
                        CatalogMainFragment(),
                        CATALOG_FRAGMENT,
                        true,
                        ""
                    )
                }
            }
            dialog
                .setTitle(getString(R.string.message_oy))
                .setMessage(getString(R.string.do_you_really_quit))
                .show()
        }

        profile_password_change_text_layout.setOnClickListener {

            (activity as MainActivity).showCustomAppBar("", true, false)
            (activity as MainActivity).replaceFragment(
                ProfileChangePasswordFragment(),
                PROFILE_CHANGE_PASSWORD_FRAGMENT,
                false,
                ""
            )
        }

        token = Paper.book().read(PAPER_TOKEN, "")
        if (token != "") {
            token = TOKEN_PREFIX + token
            viewModel?.apiGetProfile(
                token
            )?.observe(this, Observer { response ->

                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {
                        profile_name.setText(data.answer?.firstname.toString())
                        profile_surname.setText(data.answer?.lastname.toString())
                        profile_email.setText(data.answer?.email.toString())
                        profile_edit_phone.setText(data.answer?.telephone.toString())

                        (activity as MainActivity).setProfileData(
                            UserProfile(
                                email = data.answer?.email.toString(),
                                telephone = data.answer?.telephone.toString(),
                                firstname = data.answer?.firstname.toString(),
                                lastname = data.answer?.lastname.toString(), token = token
                            )
                        )

                    } else
                        context?.toast(data?.message.toString())
                } else
                    context?.toast(response?.message().toString())
            })
        }
    }

    override fun onPause() {
        super.onPause()
        (activity as MainActivity).showUserEditProfileMenu(false)
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).showUserEditProfileMenu(true)
    }
}
