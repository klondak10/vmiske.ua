package noty.team.miska.profile


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_profile_edit.*
import noty.team.miska.MainActivity
import noty.team.miska.PAPER_USER_NAME
import noty.team.miska.R
import noty.team.miska.model.UserProfile
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel


class ProfileEditFragment : Fragment() {

    var userProfile: UserProfile? = null
    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    var token = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_profile_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).showCustomAppBar("", true, false)

        profile_edit_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        userProfile = (activity as MainActivity).getProfileData()
        userProfile?.let {

            profile_edit_name.setText(it.firstname)
            profile_edit_surname.setText(it.lastname)
            token = it.token

        }

        profile_edit_save_button.setOnClickListener {

            if (profile_edit_name.text.trim().toString().isEmpty()) {
                context?.toast(getString(R.string.message_input_name))
                return@setOnClickListener
            }
            if (profile_edit_surname.text.trim().toString().isEmpty()) {
                context?.toast(getString(R.string.message_input_surname))
                return@setOnClickListener
            }
            viewModel.apiUpdateUserProfile(
                token,
                "", "",
                profile_edit_name.text.toString(),
                profile_edit_surname.text.toString()

            ).observe(this, Observer { response ->

                if (response.isSuccessful) {
                    val data = response.body()
                    if (data?.error == false) {

                        Paper.book().write(PAPER_USER_NAME, profile_edit_name.text.toString())
                        (activity as MainActivity).showUserMenu(
                            true,
                            profile_edit_name.text.toString()
                        )

                        if (data.answer ?: false) {
                            context?.toast(getString(R.string.successfull_update))
                            (activity as MainActivity).onBackPressed()
                        }
                    } else {
                        if (data?.message != null)
                            context?.toast(data.message ?: getString(R.string.change_error))
                    }
                } else
                    context?.toast(response.message().toString())
            })
        }
    }
}
