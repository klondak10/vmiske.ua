package noty.team.miska.profile


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_profile_change_password.*
import noty.team.miska.MainActivity
import noty.team.miska.PROFILE_PASSWORD_CHANGED_FRAGMENT
import noty.team.miska.R
import noty.team.miska.login.PAPER_TOKEN
import noty.team.miska.util.toast
import noty.team.miska.viewmodel.DataViewModel
import java.util.regex.Pattern


class ProfileChangePasswordFragment : Fragment() {
    private val viewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(DataViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_profile_change_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).showCustomAppBar("", true, false)

        profile_change_password_arrow_back.setOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        profile_change_save_password_button.setOnClickListener {

            if (reset_old_password.text?.trim()?.isEmpty() ?: false) {
                context?.toast(getString(R.string.input_old_password))
                return@setOnClickListener
            }

            if (reset_old_password.text?.length ?: 0 < 4) {
                context?.toast(getString(R.string.message_input_password_length))
                return@setOnClickListener
            }
            if (reset_new_password.text?.trim()?.isEmpty() ?: false) {
                context?.toast(getString(R.string.input_new_password))
                return@setOnClickListener
            }
            if (reset_new_password.text?.length ?: 0 < 4) {
                context?.toast(getString(R.string.message_input_password_length))
                return@setOnClickListener
            }

            val patternPassword =
                Pattern.compile("[A-ZА-Яа-яa-z0-9_-Ёё]{4,20}\$")
            val matcherPassword = patternPassword.matcher(reset_new_password.text.toString())

            if (!matcherPassword.matches()) {
                context?.toast(getString(R.string.message_password_only_number_letters))
                return@setOnClickListener
            }

            if (reset_repeat_password.text?.trim()?.isEmpty() ?: false) {
                context?.toast(getString(R.string.message_repeat_password))
                return@setOnClickListener
            }
            if (reset_repeat_password.text?.length ?: 0 < 4) {
                context?.toast(getString(R.string.password_repeat_length))
                return@setOnClickListener
            }

            val matcherRepeatPassword =
                patternPassword.matcher(reset_repeat_password.text.toString())

            if (!matcherRepeatPassword.matches()) {
                context?.toast(getString(R.string.password_repeat_only_numbers_letters))
                return@setOnClickListener
            }
            if (!(reset_new_password.text?.toString() ?: "").equals(
                    reset_repeat_password.text?.toString() ?: ""
                )
            ) {
                context?.toast(getString(R.string.message_passwords_not_thesame))
                return@setOnClickListener
            }

            var token = Paper.book().read(PAPER_TOKEN, "")
            if (token != "") {
                token = TOKEN_PREFIX + token
                viewModel?.apiChangePassword(
                    token, reset_old_password.text.toString(),
                    reset_new_password.text.toString()

                ).observe(this, Observer { response ->

                    if (response.isSuccessful) {
                        val data = response.body()
                        if (data?.error == false) {

                            Paper.book().write(PAPER_TOKEN, data.answer?.token ?: "")

                            (activity as MainActivity).replaceFragment(
                                ProfilePasswordChangedFragment(),
                                PROFILE_PASSWORD_CHANGED_FRAGMENT,
                                false,
                                ""
                            )

                        } else {
                            //context?.toast(data?.message.toString())
                            if (data?.message.toString() == "incorrect old password")
                                context?.toast(getString(R.string.old_password_wrong)) else {
                                context?.toast(getString(R.string.password_wrong))
                            }
                        }
                    } else
                        context?.toast(response?.message().toString())
                })
            }
        }
    }
}


